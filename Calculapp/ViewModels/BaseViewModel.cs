﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Calculapp.ViewModels
{
	/// <summary>
	/// Clase BaseViewModel: Clase Base para los view models
	/// </summary>
	public class BaseViewModel : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase MainWindow: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}

		#endregion
	}
}
