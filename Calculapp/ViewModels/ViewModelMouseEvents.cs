﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.UserControls;
using MahApps.Metro.Controls;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.ViewModels
{
	/// <summary>
	/// Clase ViewModelMouseEvents: Manejador de eventos del ratón.
	/// </summary>
	public class ViewModelMouseEvents : BaseViewModel
	{
		/// <summary>
		/// Constructor vacío de ViewModelMouseEvents();
		/// </summary>
		public ViewModelMouseEvents() { InitializeComponent(); }

		#region SETTERS AND GETTERS

		#region MainMenu

		/// <summary>
		/// SET & GET de CMainMenuMouseEnter: Comando para MouseLeave.
		/// </summary>
		public ICommand CMainMenuMouseEnter { get; private set; }

		/// <summary>
		/// SET & GET de CMainMenuMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CMainMenuMouseLeave { get; private set; }

		#endregion

		#region Options

		/// <summary>
		/// SET & GET de CommandImageMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CLateralMenuMouseEnter { get; private set; }

		/// <summary>
		/// SET & GET de CommandLateralMenuMouseLeave: Comando para LateralMenuMouseLeave.
		/// </summary>
		public ICommand CLateralMenuMouseLeave { get; private set; }

		#endregion

		#region ModuleSeleccion

		/// <summary>
		/// SET & GET de CModuleMenueMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CModuleMenuMouseEnter { get; private set; }

		/// <summary>
		/// SET & GET de CModuleMenueMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CModuleMenuMouseLeave { get; private set; }

		#endregion

		#region Mapa

		/// <summary>
		/// SET & GET de CLevelMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CLevelMouseEnter { get; private set; }

		/// <summary>
		/// SET & GET de CLevelMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CLevelMouseLeave { get; private set; }

		#endregion

		#region GamePlay

		/// <summary>
		/// SET & GET de CNumberMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CNumberMouseEnter { get; private set; }

		/// <summary>
		/// SET & GET de CNumberMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CNumberMouseLeave { get; private set; }

		#endregion

		#region General

		/// <summary>
		/// SET & GET de CommandMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CMouseEnter { get; private set; }
		
		/// <summary>
		/// SET & GET de CommandMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CMouseLeave { get; private set; }

		/// <summary>
		/// SET & GET de CommandMouseEnter: Comando para MouseEnter.
		/// </summary>
		public ICommand CHeaderMouseEnter { get; private set; }
		
		/// <summary>
		/// SET & GET de CommandMouseLeave: Comando para MouseLeave.
		/// </summary>
		public ICommand CHeaderMouseLeave { get; private set; }

		/// <summary>
		/// SET & GET de CommandMouseLeftButtonDown: Comando para LeftButtonDown.
		/// </summary>
		public ICommand CMouseLeftButtonDown { get; private set; }
		
		/// <summary>
		/// SET & GET de CommandMouseLeftButtonUp: Comando para LeftButtonUp.
		/// </summary>
		public ICommand CMouseLeftButtonUp { get; private set; }

		/// <summary>
		/// SET & GET de SelectedSectionName: Indica el menu seleccionado.
		/// </summary>
		public string SelectedSectionName { get; internal set; }

		/// <summary>
		/// SET & GET de BConverter: Convierte Hexadesimal string a Brush.
		/// </summary>
		public BrushConverter BConverter { get; internal set; }  = new BrushConverter();

		#endregion

		#endregion

		#region Eventos del Mouse

		#region MainMenu

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void MainMenuMouseEnter(object sender)
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador
			var control = sender as Border;
			var image = control.Child as Image;

			if (control != null) control.BorderThickness = new Thickness(10d);
			if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void MainMenuMouseLeave(object sender)
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");

			//Anima el controlador
			var control = sender as Border;
			var image = control.Child as Image;

			if (control != null) control.BorderThickness = new Thickness(0);
			if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 0.6, Color = Colors.Black };
		}

		#endregion

		#region Opciones

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void LateralMenuMouseEnter(object sender)
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador
			var control = sender as DockPanel;
			if (control != null)
				if (control.Name != SelectedSectionName) control.Background = (Brush)BConverter.ConvertFrom("#FF686868");
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void LateralMenuMouseLeave(object sender)
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");

			//Anima el controlador
			var control = sender as DockPanel;
			if (control != null)
				if (control.Name != SelectedSectionName) control.Background = (Brush)BConverter.ConvertFrom("#FF1E1E1E");
		}

		#endregion

		#region ModuleSelection

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void ModuleMenuMouseEnter(object sender)
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador
			var control = sender as Tile;
			//var image = control.Content as Image;

			if (control != null)
			{
				control.BorderThickness = new Thickness(5d);
				control.BorderBrush = Brushes.Gold;
			}
			//if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void ModuleMenuMouseLeave(object sender)
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");

			//Anima el controlador
			var control = sender as Tile;
			//var image = control.Content as Image;

			if (control != null)
			{
				control.BorderThickness = new Thickness(2d);
				control.BorderBrush = Brushes.White;
			}
			//if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 0.6, Color = Colors.Black };
		}

		#endregion

		#region Mapa

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void LevelMouseEnter(object sender)
		{
			//Anima el controlador
			var control = sender as UserControlLevel;

			if (!control.Locked)
			{
				PlaySound("mouse_hover.mp3");
				var svgReader = new SVGReader();

				//Cambiar icono del ratón
				DataHandler.ChangeCursor("Drop.cur");

				if (control.Passed) control.ImageLevel.Source = svgReader.Read(@"Images\Passed-Level-Selected.svg");
				else control.ImageLevel.Source = svgReader.Read(@"Images\Level-Selected.svg");
			}
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void LevelMouseLeave(object sender)
		{
			//Anima el controlador
			var control = sender as UserControlLevel;

			if (!control.Locked)
			{
				var svgReader = new SVGReader();

				//Cambiar icono del ratón
				DataHandler.ChangeCursor("Clicker.cur");

				if (control.Passed) control.ImageLevel.Source = svgReader.Read(@"Images\Passed-Level-Unselected.svg");
				else control.ImageLevel.Source = svgReader.Read(@"Images\Level-Unselected.svg");
			}
		}

		#endregion

		#region GamePlay

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void NumberMouseEnter(object sender)
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador
			var control = sender as Border;
			//var image = control.Content as Image;

			if (control != null) control.BorderBrush = Brushes.Blue;
			//if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void NumberMouseLeave(object sender)
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");

			//Anima el controlador
			var control = sender as Border;
			//var image = control.Content as Image;

			if (control != null) control.BorderBrush = Brushes.White;
			//if (image != null) image.OpacityMask = new SolidColorBrush { Opacity = 0.6, Color = Colors.Black };
		}

		#endregion

		#region General

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		private void MouseEnter()
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		private void MouseLeave()
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al poner el ratón encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void HeaderMouseEnter(object sender)
		{
			//Reproduce un sonido
			PlaySound("mouse_hover.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador
			var control = sender as Image;
			if (control != null) control.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al sacar el ratón de encima del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void HeaderMouseLeave(object sender)
		{
			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Clicker.cur");

			//Anima el controlador
			var control = sender as Image;
			if (control != null) control.OpacityMask = new SolidColorBrush { Opacity = 0.6, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al hacer clic izquierdo.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void MouseLeftButtonDown()
		{
			//Reproduce un sonido
			PlaySound("in.ogg");

			//Cambiar icono del ratón
			if (DataHandler.CurrentCursor != new Cursor(GetFullPath("Waiting.cur"))) DataHandler.ChangeCursor("Clicked.cur");
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al soltar el clic izquierdo.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private async void MouseLeftButtonUp()
		{
			//espera
			await Task.Delay(100);

			//Cambiar icono del ratón
			if (DataHandler.CurrentCursor != new Cursor(GetFullPath("Waiting.cur"))) DataHandler.ChangeCursor("Drop.cur");
		}

		/// <summary>
		/// Evento de la clase ViewModelMouseEvents: Se ejecuta al mover el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		private void MouseMove(object e)
		{
			//Reproduce un sonido
			//PlaySound("mouse_leave.mp3");

			//Cambiar icono del ratón
			DataHandler.ChangeCursor("Drop.cur");

			//Anima el controlador

		}

		#endregion

		#endregion

		#region Métodos auxiliares

		/// <summary>
		/// Método de la clase ViewModelMouseEvents: Inizialisa los componentes.
		/// </summary>
		protected void InitializeComponent()
		{
			//main menue
			CMainMenuMouseEnter = new ParamCommand(new Action<object>(MainMenuMouseEnter));
			CMainMenuMouseLeave = new ParamCommand(new Action<object>(MainMenuMouseLeave));
			//options
			CLateralMenuMouseEnter = new ParamCommand(new Action<object>(LateralMenuMouseEnter));
			CLateralMenuMouseLeave = new ParamCommand(new Action<object>(LateralMenuMouseLeave));
			//module seleccion
			CModuleMenuMouseEnter = new ParamCommand(new Action<object>(ModuleMenuMouseEnter));
			CModuleMenuMouseLeave = new ParamCommand(new Action<object>(ModuleMenuMouseLeave));
			//mapa
			CLevelMouseEnter = new ParamCommand(new Action<object>(LevelMouseEnter));
			CLevelMouseLeave = new ParamCommand(new Action<object>(LevelMouseLeave));
			//gameplay
			CNumberMouseEnter = new ParamCommand(new Action<object>(NumberMouseEnter));
			CNumberMouseLeave = new ParamCommand(new Action<object>(NumberMouseLeave));
			//general
			CMouseEnter = new DelegateCommand(new Action(MouseEnter));
			CMouseLeave = new DelegateCommand(new Action(MouseLeave));
			CHeaderMouseLeave = new ParamCommand(new Action<object>(HeaderMouseLeave));
			CHeaderMouseEnter = new ParamCommand(new Action<object>(HeaderMouseEnter));
			CMouseLeftButtonDown = new DelegateCommand(new Action(MouseLeftButtonDown));
			CMouseLeftButtonUp = new DelegateCommand(new Action(MouseLeftButtonUp));
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase ViewModelMouseEvents: Retorna el path completo del archivo.
		/// </summary>
		private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Resources\Icons\Cursors\" + fileName;
		}

		#endregion
	}
}
