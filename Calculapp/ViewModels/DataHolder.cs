﻿using Calculapp.Data;
using System.Windows;

namespace Calculapp.ViewModels
{
	/// <summary>
	/// Clase DataHolder: Almacena las propiedades de los controladores
	/// </summary>
	public class DataHolder : ViewModelMouseEvents
	{
		#region Propiedades

		private bool _soundmute;
        private bool _musicmute;
        private double _musicvolume;
        private double _soundvolume;
		private bool _isAnimating;
		private string _selectedGrade;
		private string _selectedModule;
		private double _globalFontSize;
		private Level _selectedLevel;
		private Point _mouseRelativePosition;

		#endregion

		/// <summary>
		/// Constructor vacío de DataHolder();
		/// </summary>
		public DataHolder() : base() { Inizialise(); }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De SoundMute. Indica si el sonido esta en mute.
		/// </summary>
        public bool SoundMute
		{
			get =>  ConfingHandler.Settings.SoundMute;
			set
			{
				_soundmute = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MusicMute. Indica si la musica esta en mute.
		/// </summary>
        public bool MusicMute
		{
			get => ConfingHandler.Settings.MusicMute;
			set
			{
				_musicmute = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MusicVolume. Indica el vloumen de la musica.
		/// </summary>
        public double MusicVolume
		{
			get => ConfingHandler.Settings.MusicVolume;
			set
			{
				_musicvolume = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De SoundVolume. Indica el volumen del sonido.
		/// </summary>
        public double SoundVolume
		{
			get => ConfingHandler.Settings.SoundVolume;
			set
			{
				_soundvolume = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De SelectedGrade. Indica el grado seleccionado.
		/// </summary>
        public string SelectedGrade
		{
			get => DataHandler.SelectedGrade;
			set
			{
				_selectedGrade = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De SelectedModule. Indica el módulo seleccionado.
		/// </summary>
        public string SelectedModule
		{
			get => DataHandler.SelectedModule;
			set
			{
				_selectedModule = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De SelectedLevel. Indica el nivel seleccionado.
		/// </summary>
		public Level SelectedLevel
		{
			get => DataHandler.SelectedLevel;
			set
			{
				_selectedLevel = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => DataHandler.GlobalFontSize;
			set
			{
				_globalFontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => DataHandler.IsAnimating;
			set
			{
				_isAnimating = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MousePosition. Indica la posicion del ratón segun el control seleccionado.
		/// </summary>
		public Point MouseRelativePosition
		{
			get => DataHandler.MouseRelativePosition;
			set
			{
				_mouseRelativePosition = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método de la clase DataHolder: Inisializa los componentes.
		/// </summary>
		private void Inizialise()
		{
			SoundMute = ConfingHandler.Settings.SoundMute;
			MusicMute = ConfingHandler.Settings.MusicMute;
			MusicVolume = ConfingHandler.Settings.MusicVolume;
			SoundVolume = ConfingHandler.Settings.SoundVolume;
			SelectedGrade = DataHandler.SelectedGrade;
			SelectedModule = DataHandler.SelectedModule;
			SelectedLevel = DataHandler.SelectedLevel;
			IsAnimating = DataHandler.IsAnimating;
			GlobalFontSize = DataHandler.GlobalFontSize;
		}

		#endregion
	}
}
