﻿using Calculapp.Data;
using Calculapp.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculapp.Views.Screens
{
    /// <summary>
    /// Interaction logic for CreditsScreen.xaml
    /// </summary>
    public partial class CreditsScreen : Page
    {
        public bool Aniating { get; private set; } = true;

        public CreditsScreen()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Funcion llamada cuando se hace click en la pantalla. (Regresa a la pagina del titulo)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Page_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Aniating = false;
            var myWindow = Application.Current.MainWindow as MainWindow;
            DataHandler.IsAnimating = true;
            myWindow.InverseAnimationFlag = true;

            await Task.Delay(1000);
            myWindow.FrameScreenNavigator.Content = new MainMenuSreen();
        }
        
        /// <summary>
        /// Funcion llamada tras la carga de la pagina esta completada. (Anima el controlador scroller)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            DataHandler.IsAnimating = false;

            //setea los atributos
            var aniation = new AnimationHandler();
            var duration = TimeSpan.FromSeconds(30);
            var translateTransform = new TranslateTransform(0, this.ActualHeight);

            //inica la aniación
            scroller.RenderTransform = translateTransform;
            aniation.Traslation(scroller, new Point(0, -(scroller.ActualHeight+this.ActualHeight)), new Duration(duration));

            //cambia de pantalla al finalizar
            await Task.Delay(duration);
            if (Aniating) Page_MouseLeftButtonDown(null, null);
        }
    }
}
