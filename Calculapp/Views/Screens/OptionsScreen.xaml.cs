﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.ViewModels;
using Calculapp.Views.UserControls;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de OptionsScreen.xaml.
	/// </summary>
	public partial class OptionsScreen : Page, INotifyPropertyChanged
	{
		//atributos
		private bool _lateralMenuVisible = true;
		private BrushConverter brushConverter = new BrushConverter();
		private bool _isAnimating = true;
		private string _selectedSectionName;
		private Dictionary<int, int> _comboBoxCountIntelvals = new Dictionary<int, int>();

		/// <summary>
		/// Constructor vacío de OptionsScreen().
		/// </summary>
		public OptionsScreen()
		{
			IsAnimating = true;
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al cargar la página.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
		{
			DataHandler.IsAnimating = false;

			var svgReader = new SVGReader();
			var json = DataHandler.Read("Config.bin");
			FontSize = DataHandler.GlobalFontSize;
			SelectedSectionName = "DockPanelGeneral";

			if (DataHandler.MusicMute) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else SliderMusicVolume.Value = (DataHandler.MusicVolume * 100);

			if (DataHandler.SoundMute) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else SliderSoundVolume.Value = (DataHandler.SoundVolume * 100);

			if (DataHandler.VoiceMute) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else SliderVoiceVolume.Value = (DataHandler.SoundVolume * 100);

			MyDataGridMessages.ContentList = json["Messages"];
			MyDataGridMessages.RowType = new UserControlGridRow();
			MyDataGridMessages.Reload();

			MyDataGridMixOperations.RowType = new UserControlGridRowOperation();

			ToggleSwitchScreenMode.IsChecked = DataHandler.FullScreen;

			//Correcion bug ImageBasket.Width = ???.
			if (!DataHandler.FullScreen) MyDataGridMessages.MaxHeightRows = 26;

			await Task.Delay(1000);
			IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		public Dictionary<int, int> ComboBoxCountIntenvals
		{
			get => _comboBoxCountIntelvals;
			set
			{
				_comboBoxCountIntelvals = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De LateralMenu1: Indica si los labels estan visibles o no.
		/// </summary>
		public bool LateralMenuVisible
		{
			get => _lateralMenuVisible;
			set
			{
				_lateralMenuVisible = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET: y GET: De SelectedSectionName. Indica el nombre de la sección seleccionada.
		/// </summary>
		public string SelectedSectionName
		{
			get => _selectedSectionName;
			set
			{
				((ViewModelMouseEvents)this.DataContext).SelectedSectionName = _selectedSectionName = value;
			}
		}

		/// <summary>
		/// SET: y GET: De PreviousSectionName. Indica el nombre de la sección anterior.
		/// </summary>
		public string PreviousSectionName { get; private set; } = string.Empty;

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = _isAnimating = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si esta bloqueado o no.
		/// </summary>
		public bool IsLocked { get; internal set; } = false;

		public UserControlDataGrid UserControlDataGrid
		{
			get => default(UserControlDataGrid);
			set
			{
			}
		}

		#endregion

		#region Menu Lateral

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al cargar hace clic en DockPanelBack.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImagePrevious_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (IsLocked) e.Handled = true;
			else
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				PlaySound("transition.ogg");
				DataHandler.IsAnimating = true;

				myWindow.InverseAnimationFlag = true;
				if (SelectedSectionName != "DockPanelGeneral") SaveConfig(SelectedSectionName);

				await Task.Delay(1000);
				myWindow.FrameScreenNavigator.Content = new MainMenuSreen();
			}
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al cargar hace clic en un DockPanel.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void DockPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (IsLocked) e.Handled = true;
			else
			{
				var dockPanel = sender as DockPanel;

				//LateralMenuVisible = true;
				if (SelectedSectionName != dockPanel.Name)
				{
					SelectedSectionName = dockPanel.Name;

					StackPanelGeneralOptions.Visibility = (SelectedSectionName == "DockPanelGeneral") ? Visibility.Visible : Visibility.Collapsed;
					MetroAnimatedTabControlGradeOptions.Visibility = (SelectedSectionName != "DockPanelGeneral") ? Visibility.Visible : Visibility.Collapsed;

					if (!string.IsNullOrEmpty(PreviousSectionName)) SaveConfig(PreviousSectionName);

					if (SelectedSectionName != "DockPanelGeneral") LoadConfig();
					MarkSelectedSetion();
				}
			}
		}

		#endregion

		#region Opciones Generales

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al deslizar la barra SliderMusicVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void SliderMusicVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			var svgReader = new SVGReader();
			if (!MusicPlayer.IsPlaying) MusicPlayer.Play();
			DataHandler.MusicVolume = (e.NewValue / 100);
			DataHandler.MusicMute = (e.NewValue == 0) ? true : false;
			//PlaySound("mouse_hover.mp3");

			if (e.NewValue == 0) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else if (e.NewValue < 30) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
			else if(e.NewValue > 70) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
			else ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al hacer clic en ImageMusicVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageMusicVolume_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var svgReader = new SVGReader();
			if (!MusicPlayer.IsPlaying) MusicPlayer.Play();
			DataHandler.MusicMute = !DataHandler.MusicMute;
			PlaySound("in.ogg");

			if(DataHandler.MusicMute) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else
			{
				SliderMusicVolume.Value = (DataHandler.MusicVolume * 100);
				if (DataHandler.MusicVolume == 0) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
				else if (DataHandler.MusicVolume < 0.30) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
				else if (DataHandler.MusicVolume > 0.70) ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
				else ImageMusicVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
			}
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al deslizar la barra SliderSoundsVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void SliderSoundVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			var svgReader = new SVGReader();
			DataHandler.SoundVolume = (e.NewValue / 100);
			DataHandler.SoundMute = (e.NewValue == 0) ? true : false;
			//PlaySound("mouse_hover.mp3");

			if (e.NewValue == 0) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else if (e.NewValue < 30) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
			else if (e.NewValue > 70) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
			else ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al hacer clic en ImageSoundsVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageSoundVolume_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var svgReader = new SVGReader();
			DataHandler.SoundMute = !DataHandler.SoundMute;
			PlaySound("in.ogg");

			if (DataHandler.SoundMute) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else
			{
				SliderSoundVolume.Value = (DataHandler.SoundVolume * 100);
				if (DataHandler.SoundVolume == 0) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
				else if (DataHandler.SoundVolume < 0.30) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
				else if (DataHandler.SoundVolume > 0.70) ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
				else ImageSoundVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
			}
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al deslizar la barra SliderSoundsVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void SliderVoiceVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			var svgReader = new SVGReader();
			DataHandler.VoiceVolume = (e.NewValue / 100);
			DataHandler.VoiceMute = (e.NewValue == 0) ? true : false;
			//PlaySound("mouse_hover.mp3");

			if (e.NewValue == 0) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else if (e.NewValue < 30) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
			else if (e.NewValue > 70) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
			else ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al hacer clic en ImageSoundsVolume.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageVoiceVolume_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var svgReader = new SVGReader();
			DataHandler.VoiceMute = !DataHandler.VoiceMute;
			PlaySound("in.ogg");

			if (DataHandler.VoiceMute) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
			else
			{
				SliderVoiceVolume.Value = (DataHandler.SoundVolume * 100);
				if (DataHandler.VoiceVolume == 0) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Mute-03.svg");
				else if (DataHandler.VoiceVolume < 0.30) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-low-01.svg");
				else if (DataHandler.VoiceVolume > 0.70) ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Max-Volume-02.svg");
				else ImageVoiceVolume.Source = svgReader.Read(@"Icons\Speaker-Audible-01.svg");
			}
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Se ejecuta al hacer clic en ToggleSwitchScreenMode.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ToggleSwitchScreenMode_IsCheckedChanged(object sender, EventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			var previousHeight = myWindow.Height;
			PlaySound("in.ogg");

			DataHandler.FullScreen = (bool)ToggleSwitchScreenMode.IsChecked;
			if (DataHandler.FullScreen)
			{
				myWindow.WindowState = WindowState.Maximized;
				myWindow.UseNoneWindowStyle = true;
				var newHeight = myWindow.Height;
				var r = (14 * (previousHeight / newHeight));
				FontSize = (DataHandler.GlobalFontSize += r);
			}
			else if (DataHandler.GlobalFontSize > 14)
			{
				myWindow.WindowState = WindowState.Normal;
				myWindow.WindowStyle = WindowStyle.SingleBorderWindow;
				var newHeight = myWindow.Height;
				var r = (14 * (newHeight / previousHeight));
				FontSize = (DataHandler.GlobalFontSize -= r);
			}

			MyDataGridMessages.Reload();
		}

		#endregion

		#region Métodos

		/// <summary>
        /// Evento de la clase OptionsScreen: Cambia las limitaciones de las opciones.
        /// </summary>
		private void ChangeLimitations()
		{
			MyDataGridMixOperations.SelectedSectionName = SelectedSectionName;

			if (SelectedSectionName == "DockPanelFirst")
			{
				SliderOrderCount.Maximum = 10;
				NumericUpDownOrderStartValue.Maximum = 90;
				NumericUpDownCountTime.Maximum = 2;
				SliderCountFuitsNumber.Maximum = 2;
				ComboBoxCountIntenvals = new Dictionary<int, int> { { 1, 1 }, { 2, 2 }, { 5, 5 } };
				NumericUpDownCountStartValue.Maximum = 90;
			}
			else if (SelectedSectionName == "DockPanelSecond")
			{
				SliderOrderCount.Maximum = 15;
				NumericUpDownOrderStartValue.Maximum = 990;
				NumericUpDownCountTime.Maximum = 5;
				SliderCountFuitsNumber.Maximum = 3;
				ComboBoxCountIntenvals = new Dictionary<int, int> { { 1, 1 }, { 2, 2 }, { 5, 5 }, { 10, 10 }, { 20, 20 } };
				NumericUpDownCountStartValue.Maximum = 990;
			}
			else if (SelectedSectionName == "DockPanelThird")
			{
				SliderOrderCount.Maximum = 20;
				NumericUpDownOrderStartValue.Maximum = 999990;
				NumericUpDownCountTime.Maximum = 10;
				SliderCountFuitsNumber.Maximum = 4;
				ComboBoxCountIntenvals = new Dictionary<int, int> { { 1, 1 }, { 2, 2 }, { 5, 5 }, { 10, 10 }, { 15, 15 }, { 20, 20 }, { 50, 50 }, { 100, 100 } };
				NumericUpDownCountStartValue.Maximum = 999990;
			}
		}

		/// <summary>
        /// Evento de la clase OptionsScreen: Carga la configuración del grado seleccionado.
        /// </summary>
		private void LoadConfig()
		{
			JToken json = null;
			if (SelectedSectionName == "DockPanelFirst") json = DataHandler.JSON["1ro"];
			else if (SelectedSectionName == "DockPanelSecond") json = DataHandler.JSON["2do"];
			else if (SelectedSectionName == "DockPanelThird") json = DataHandler.JSON["3ro"];

			//cambia las limitaciones segun el grado.
			ChangeLimitations();

			//carga la configuracion.
			if(json != null)
			{
				SliderOrderCount.Value = (int)json["OrderCount"];
				NumericUpDownOrderStartValue.Value = (int)json["OrderStartValue"];
				ToggleSwitchOrderOrder.IsChecked = (bool)json["OrderOrder"];
				NumericUpDownCountTime.Value = (int)json["CountTime"];
				SliderCountFuitsNumber.Value = (int)json["CountFuitsNumber"];
				ComboBoxCountInterval.SelectedValue = (int)json["CountInterval"];
				NumericUpDownCountStartValue.Value = (int)json["CountStartValue"];
				ToggleSwitchCountOrder.IsChecked = (bool)json["CountOrder"];
				MyDataGridMixOperations.ContentList = json["MixedOperations"];
				MyDataGridMixOperations.Reload();
			}

			PreviousSectionName =  SelectedSectionName;
		}

		/// <summary>
        /// Evento de la clase OptionsScreen: Guarda las opciones.
        /// </summary>
		private void SaveConfig(string name)
		{
			dynamic json = new JObject();
			dynamic dictionary = ComboBoxCountInterval.SelectedItem;

			json.OrderCount = SliderOrderCount.Value;
			json.OrderStartValue = (NumericUpDownOrderStartValue.Value != null) ? NumericUpDownOrderStartValue.Value : 0;
			json.OrderOrder = (ToggleSwitchOrderOrder.IsChecked != null) ? ToggleSwitchOrderOrder.IsChecked : true;
			json.CountTime = (NumericUpDownCountTime.Value != null) ? NumericUpDownCountTime.Value : 0;
			json.CountFuitsNumber = SliderCountFuitsNumber.Value;
			json.CountInterval = (ComboBoxCountInterval.SelectedValue != null) ? ComboBoxCountInterval.SelectedValue : 1;
			json.CountStartValue = (NumericUpDownCountStartValue.Value != null) ? NumericUpDownCountStartValue.Value : 0;
			json.CountOrder = (ToggleSwitchCountOrder.IsChecked != null) ? ToggleSwitchCountOrder.IsChecked : true;
			json.MixedOperations = MyDataGridMixOperations.ContentList;
			//No Manejados
			json.HasFractions = false;
			json.HasNegative = false;

			if (name == "DockPanelFirst") DataHandler.JSON["1ro"] = json;
			else if (name == "DockPanelSecond") DataHandler.JSON["2do"] = json;
			else if (name == "DockPanelThird") DataHandler.JSON["3ro"] = json;

			DataHandler.Write("Config.bin");
		}

		/// <summary>
		/// Evento de la clase OptionsScreen: Resalta el control seleccionado y deselecciona todos los demas del LateralMenu.
		/// </summary>
		private void MarkSelectedSetion()
        {
            foreach(DockPanel child in LateralMenu.Children)
            {
                if (child.Name == SelectedSectionName)
                {
                    BrushConverter brushConverter = new BrushConverter();
                    child.Background = (Brush)brushConverter.ConvertFrom("#FF007ACC");
                }
                else
                {
                    BrushConverter brushConverter = new BrushConverter();
                    child.Background = (Brush)brushConverter.ConvertFrom("#FF1E1E1E");
                }
            }
        }

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase Options: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}

		#endregion
	}
}
