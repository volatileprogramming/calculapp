﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.UserControls;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de MappScreen.xaml.
	/// </summary>
	public partial class MappScreen : Page, INotifyPropertyChanged
	{
		private double _fontSize;
		private bool _isAnimating = true;

		/// <summary>
		/// Constructor vacío de MappScreen().
		/// </summary>
		public MappScreen()
		{
			InitializeComponent();
        }

		/// <summary>
		/// Evento de la clase MappScreen: Se ejecuta al cargar la pantalla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
		{
			DataHandler.IsAnimating = false;
			IsAnimating = true;

			//setea el tamaño de las fuentes
			GlobalFontSize = DataHandler.GlobalFontSize;

			//configura el mapa
			Builder.Buil(CanvasMapp);

            //instancia y añade la ventana de ayuda.
            var help = new UserControlHelp("/Resources/Gifs/map_help.gif");
            Grid.SetRow(help, 1);
            Grid.SetRowSpan(help, 3);
            MainGrid.Children.Add(help);

            //Cambia el mensaje de la cabezera
            LabelTitle.Content += $" {GetModule()}";

			await Task.Delay(300);
			IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => _fontSize;
			set
			{
				FontSize = _fontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = _isAnimating = !value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Eventos del Header

		/// <summary>
		/// Evento de la clase MappScreen: Se ejecuta al cargar hace clic en ImagePrevious.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImagePrevious_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			PlaySound("transition.ogg");
			DataHandler.IsAnimating = true;
			myWindow.InverseAnimationFlag = true;

			await Task.Delay(1000);
			myWindow.FrameScreenNavigator.Content = new ModuleSelectionScreen();
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase MappScreen: Retorna el modulo seleccionado en español.
		/// </summary>
		/// <returns>Retorna un string con el modulo seleccionado</returns>
		private string GetModule()
		{
			if (DataHandler.SelectedModule == "Addition") return "suma";
			else if (DataHandler.SelectedModule == "Subtraction") return "resta";
			else if (DataHandler.SelectedModule == "Multiplication") return "multiplicación";
			else return "división";
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
