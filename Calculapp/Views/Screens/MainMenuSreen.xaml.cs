﻿using Calculapp.Data;
using Calculapp.Modules;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de MainMenuSreen.xaml.
	/// </summary>
	public partial class MainMenuSreen : Page, INotifyPropertyChanged
	{
		private double _fontSize;
		private bool _isAnimating = true;

		/// <summary>
		/// Constructor vacío de MainMenuSreen().
		/// </summary>
		public MainMenuSreen()
		{
			InitializeComponent();
			//Esperando por respuesta en
			//https://github.com/reactiveui/ReactiveUI/issues/1829
			//ViewModel = new AnimationHandler();
			//this.WhenActivated(disposable =>
			//{
			//	this.BindCommand(ViewModel, vm => vm.Image_MouseEnter, v => v.ImageGrade1, ImageGrade1.Events().MouseEnter)
			//	.DisposeWith(disposable);
			//	this.BindCommand(ViewModel, vm => vm.Control_MouseLeave, v => v.ImageGrade1, ImageGrade1.Events().MouseLeave)
			//	.DisposeWith(disposable);
			//});
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al cargar la página.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
			DataHandler.IsAnimating = false;
			IsAnimating = true;

			GlobalFontSize = DataHandler.GlobalFontSize;

			await Task.Delay(2000);
			if (!MusicPlayer.IsPlaying && !DataHandler.MusicMute) MusicPlayer.Play();
			IsAnimating = false;
        }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => _fontSize;
			set
			{
				FontSize = _fontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = _isAnimating = !value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Header Events

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al hacer clic en ImageOption.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImageOption_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			PlaySound("transition.ogg");
			DataHandler.IsAnimating = true;
			
			await Task.Delay(1000);
			myWindow.FrameScreenNavigator.Content = new OptionsScreen();
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al hacer clic en ImageClose.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			PlaySound("in.ogg");

			var myWindow = Application.Current.MainWindow as MainWindow;
			myWindow.Close();
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion

		#region Eventos Selección de grado

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al hacer clic en ImageGrade1.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void BorderGrade_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			var control = sender as Border; 

			PlaySound("transition.ogg");
			DataHandler.IsAnimating = true;

			if (control.Name == "BorderGrade1") DataHandler.SelectedGrade = "1ro";
			else if (control.Name == "BorderGrade2") DataHandler.SelectedGrade = "2do";
			else DataHandler.SelectedGrade = "3ro";

			await Task.Delay(1000);
			myWindow.FrameScreenNavigator.Content = new ModuleSelectionScreen();
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase MainMenuSreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


        #endregion

        private async void creditsButton_Click(object sender, MouseButtonEventArgs e)
        {
            var myWindow = Application.Current.MainWindow as MainWindow;
            PlaySound("transition.ogg");
            DataHandler.IsAnimating = true;

            await Task.Delay(1000);
            myWindow.FrameScreenNavigator.Content = new CreditsScreen();
        }
    }
}
