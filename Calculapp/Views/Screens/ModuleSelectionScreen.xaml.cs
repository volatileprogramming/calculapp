﻿using Calculapp.Data;
using Calculapp.Modules;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de ModuleSelectionScreen.xaml.
	/// </summary>
	public partial class ModuleSelectionScreen : Page, INotifyPropertyChanged
	{
		private double _fontSize;
		private bool _isAnimating = true;

		/// <summary>
		/// Constructor vacío de ModuleSelectionScreen().
		/// </summary>
		public ModuleSelectionScreen()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Se ejecuta al cargar la página.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
		{
			DataHandler.IsAnimating = false;
			IsAnimating = true;

			//cambian el tamaño de las fuentes
			GlobalFontSize = DataHandler.GlobalFontSize;

			//muestra u oculta algunos modulos
			if (DataHandler.SelectedGrade == "1ro")
			{
				var svgReader = new SVGReader();

				TileMultiplication.Visibility = TileDivision.Visibility = Visibility.Collapsed;
				ImageMixedOperations.Source = svgReader.Read(@"Icons\Calculation-WF-2.svg");
			}
			else TileMultiplication.Visibility = TileDivision.Visibility = Visibility.Visible;

			//Cambia el mensaje de la cabezera
			LabelTitle.Content += $" {DataHandler.SelectedGrade}";

			await Task.Delay(300);
			IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => _fontSize;
			set
			{
				FontSize = _fontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = _isAnimating = !value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Eventos del Header

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Se ejecuta al cargar hace clic en ImagePrevious.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImagePrevious_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			PlaySound("transition.ogg");
			DataHandler.IsAnimating = true;
			myWindow.InverseAnimationFlag = true;

			await Task.Delay(1000);
			myWindow.FrameScreenNavigator.Content = new MainMenuSreen();
			//myWindow.FrameScreenNavigator.GoBack();
		}

		#endregion

		#region Eventos del selector de módulos

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Se ejecuta al hacer clic en uno de los Tiles.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Tile_Click(object sender, RoutedEventArgs e)
		{
			var tile = sender as Tile;
			var myWindow = Application.Current.MainWindow as MainWindow;
			var module = DataHandler.SelectedModule = tile.Name.Replace("Tile","");

			
			//va a la pantalla correcta
			if (module != "Ordering" && module != "Count" && module != "MixedOperations")
			{
				PlaySound("transition.ogg");
				DataHandler.IsAnimating = true;
				await Task.Delay(1000);
				myWindow.FrameScreenNavigator.Content = new MappScreen();
			}
			else if (module == "MixedOperations")
			{
				var roomData = DataHandler.Read("Config.bin");
				var operations = roomData[DataHandler.SelectedGrade]["MixedOperations"];

				//comprueba si tiene level activos o no
				if (GetMixedOperations(operations).Count > 0)
				{
					PlaySound("transition.ogg");
					DataHandler.IsAnimating = true;
					await Task.Delay(1000);
					myWindow.FrameScreenNavigator.Content = new GamePlayScreen();
				}
				else
				{
					PlaySound("in.ogg");
					var resoult = await myWindow.ShowMessage("Información", "Módulo no disponible.", MessageDialogStyle.Affirmative);
				}
			}
			else
			{
				PlaySound("transition.ogg");
				DataHandler.IsAnimating = true;
				await Task.Delay(1000);
				myWindow.FrameScreenNavigator.Content = new GamePlayScreen();
			}
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase ModuleSelectionScreen: Obtiene una lista de operaciones.
		/// </summary>
		/// <param name="mixedOperations">Lista de operaciones del grado seleccionado.</param>
		/// <returns>Retorna un List<string> con la lista de operaciones activas.</returns>
		private List<string> GetMixedOperations(JToken mixedOperations)
		{
			var operations = new List<string>();

			foreach (var operation in mixedOperations) if((bool)operation["Active"]) operations.Add(operation["Operation"].ToString());

			return operations;
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
