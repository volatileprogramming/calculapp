﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.UserControls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de GamePlayScreen.xaml.
	/// </summary>
	public partial class GamePlayScreen : Page, INotifyPropertyChanged
	{
		//atributos
		private double _fontSize;
		private int _wrongAnswers = 0;
		private bool _isAnimating = true;
		private int _score;
		private TimeSpan _currentTime;

		/// <summary>
		/// Constructor vacío de GamePlayScreen().
		/// </summary>
		public GamePlayScreen()
		{
			IsAnimating = true;
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase GamePlayScreen: Se ejecuta al cargar la pantalla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
		{
			DataHandler.IsAnimating = false;

			//setea el tamaño de las fuentes
			GlobalFontSize = DataHandler.GlobalFontSize;

			//crea el nivel
			dynamic UserControlContainer = GetContainerGameplay() as UIElement;
			Builder.Buil(UserControlContainer);

			//instancia el UserControl en el GridGameplayContainer
			GridGameplayContainer.Children.Clear();
			GridGameplayContainer.Children.Add(UserControlContainer);

			//setea background del gameplay
			if (UserControlContainer is UserControlCountContainer)
			{
				var brush = this.FindResource("BrushCount") as RadialGradientBrush;
				MainGrid.Background = brush;
			}
			else if (UserControlContainer is UserControlOrderingContainer)
			{
				var brush = this.FindResource("BrushOrdening") as LinearGradientBrush;
				MainGrid.Background = brush;
			}
			else
			{
				var brush = this.FindResource("BrushOperations") as RadialGradientBrush;
				MainGrid.Background = brush;
			}

			//Inicializa el game play
			if (UserControlContainer.Gameplay != null)
			{
				UserControlContainer.Gameplay.Screen = this;
				UserControlContainer.Gameplay.Start();
			}

			await Task.Delay(300);
			IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => _fontSize;
			set
			{
				FontSize = _fontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = _isAnimating = !value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET: y GET: De WrongAswers. Almecena la cantidad de respuestas equivocadas.
		/// </summary>
		public int WrongAnswers
		{
			get => _wrongAnswers;
			set
			{
				var svgReader = new SVGReader();
				_wrongAnswers = value;
				if (value == 1) HeaderGameplay.ImageLive3.Source = svgReader.Read(@"Icons\Sad-WF.svg");
				if (value == 2) HeaderGameplay.ImageLive2.Source = svgReader.Read(@"Icons\Sad-WF.svg");
				if (value == 3) HeaderGameplay.ImageLive1.Source = svgReader.Read(@"Icons\Sad-WF.svg");
			}
		}

		/// <summary>
		/// SET: y GET: De Score. Almacena la puntiación acumulada.
		/// </summary>
		public int Score {
			get => _score;
			set
			{
				HeaderGameplay.LabelScore.Content = _score = value;
			}
		}

		public TimeSpan CurrentTime
		{
			get => _currentTime;
			set
			{
				_currentTime = value;
				var timeformat = value.ToString(@"mm\:ss");
				HeaderGameplay.LabelTime.Content = timeformat;
			}
		}

		#endregion

		#region Eventos del Header

		/// <summary>
		/// Evento de la clase GamePlayScreen: Se ejecuta al cargar hace clic en ImagePrevious.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImagePrevious_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			var module = DataHandler.SelectedModule;

			PlaySound("in.ogg");

			var resoult = await myWindow.ShowMessage("Pregunta", "¿Desea finalizar la partida?", MessageDialogStyle.AffirmativeAndNegative);

			if (resoult)
			{
				DataHandler.SelectedLevel = null;
				myWindow.InverseAnimationFlag = true;

				//reproduce una animacion
				PlaySound("transition.ogg");
				DataHandler.IsAnimating = true;

				//detiene el juego
				GetContainerGameplay().Gameplay.End();

				//limpia la memoria
				GridGameplayContainer.Children.Clear();

				//espera animacion
				await Task.Delay(1000);

				//retorna a la pantalla adecuada
				if (module != "Ordering" && module != "Count" && module != "MixedOperations")
					myWindow.FrameScreenNavigator.Content = new MappScreen();
				else myWindow.FrameScreenNavigator.Content = new ModuleSelectionScreen();
			}
		}

		#endregion

		#region Métodos
			
		/// <summary>
		/// Método de la clase GamePlayScreen: Optiene el UserControl actual.
		/// </summary>
		/// <returns>Retorna el UserControl actual.</returns>
		private IGameplayContainer GetContainerGameplay()
		{
			if (DataHandler.SelectedModule == "Count")
				return UserControlCountContainer.Instance;
			else if (DataHandler.SelectedModule == "Ordering")
				return UserControlOrderingContainer.Instance;
			else if (DataHandler.SelectedModule == "Addition")
				return UserControlOperationsContainer.Instance;
			else if (DataHandler.SelectedModule == "Subtraction")
				return UserControlOperationsContainer.Instance;
			else if (DataHandler.SelectedModule == "Multiplication")
				return UserControlOperationsContainer.Instance;
			else if (DataHandler.SelectedModule == "Division")
				return UserControlOperationsContainer.Instance;
			else return UserControlOperationsContainer.Instance;
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
