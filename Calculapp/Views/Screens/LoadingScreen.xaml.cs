﻿using Calculapp.Data;
using Calculapp.Modules;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Calculapp.Views.Screens
{
	/// <summary>
	/// Interación lógica de LoadingScreen.xaml.
	/// </summary>
	public partial class LoadingScreen : Page, INotifyPropertyChanged
	{
		private bool _logoVisibility = false;
		private bool _isLoading = true;
        private double _percentage = 0;

        /// <summary>
        /// Constructor vacío de LoadingScreen().
        /// </summary>
        public LoadingScreen()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase LoadingScreen: Se ejecuta al cargar la página.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Page_Loaded(object sender, RoutedEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			DataHandler.IsAnimating = false;
			DataHandler.ChangeCursor("Waiting.cur");

			await Task.Run(() =>
			{
				this.Dispatcher.Invoke(() => 
				{
					DataHandler.Load(myWindow);
					ImageAnimation();
				});
			});
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De IsLoading: Indica si esta cargando.
		/// </summary>
		public bool IsLoading
		{
			get => _isLoading;
			set
			{
				_isLoading = value;

				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.FrameScreenNavigator.Content = new MainMenuSreen();
				PlaySound("intro.mp3");
			}
		}

		/// <summary>
		/// SET & GET: De LogoVisibility: Indica si el logo es visible o no.
		/// </summary>
		public bool LogoVisibility
		{
			get => _logoVisibility;
			set
			{
				_logoVisibility = value;
				OnPropertyChanged();
			}
		}

        /// <summary>
        /// SET & GET: De Percentage: Indica el porcentaje de carga del ProgressBar
        /// </summary>
        public double Percentage
        {
            get => _percentage;
            set
            {
				_percentage = value;
				OnPropertyChanged();
            }
        }

		private Storyboard MyStoryboard { get; set; } = new Storyboard();

        #endregion

        #region Métodos de LoadingScreen

        /// <summary>
        /// Método de clase LoadingScreen: Cambia la imagen de ImageLogo mientras cargan los datos.
        /// </summary>
        public async void ImageAnimation()
		{
			var svgReader = new SVGReader();

			await Task.Delay(1000);
			if (!IsLoading) return;
			ImageLogo.Source = svgReader.Read(@"Logos\Logo-ITSC.svg");
			LogoVisibility = true;
			await Task.Delay(3000);
			if (!IsLoading) return;

			LogoVisibility = false;
			await Task.Delay(1000);
            SetProgress(1.0d);
			if (!IsLoading) return;
			ImageLogo.Source = svgReader.Read(@"Logos\Logo-Colegio.svg");
			LogoVisibility = true;
			await Task.Delay(3000);
			if (!IsLoading) return;

			LogoVisibility = false;
			await Task.Delay(1000);
			if (!IsLoading) return;
			ImageLogo.Source = svgReader.Read(@"Logos\Logo-Texoider.svg");
			LogoVisibility = true;

			await Task.Delay(3000);
			if (!IsLoading) return;
			LogoVisibility = false;
			await Task.Delay(1000);
			if (!IsLoading) return;
			IsLoading = false;
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase LoadingScreen: Setea el progreso de forma animada.
		/// </summary>
		/// <param name="value">Valor de la barra de progeso.</param>
		public async void SetProgress(double value)
		{
			await Task.Run(async () => 
			{
				//setea propiedades
				while (Percentage < value)
				{
					Percentage += 0.01;
					await Task.Delay(TimeSpan.FromMilliseconds(50));
				}
			});
		}

		#endregion

		#region Eventos del LoadingScreen

		/// <summary>
        /// Evento de la clase LoadingScreen: Se ejecuta al cambiar la actualizar la barra de progreso.
        /// </summary>
        /// <param name="sender">Objeto que dispara el evento.</param>
        /// <param name="e">Manejador del evento.</param>
        private void ProgressBarLoadin_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			if (e.NewValue == 1)
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				var svgReader = new SVGReader();

				DataHandler.ChangeCursor("Clicker.cur");
				BorderLoader.Visibility = Visibility.Hidden;
			}
		}

		/// <summary>
		/// Evento de la clase LoadingScreen: Se ejecuta al hacer clic izquierdo.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void Page_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (ProgressBarLoadin.Value == 1 && IsLoading) IsLoading = false;
		}

		/// <summary>
		/// Evento de la clase LoadingScreen: Se ejecuta al precionar una tecla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void Page_KeyDown(object sender, KeyEventArgs e)
		{
			if (ProgressBarLoadin.Value == 1 && IsLoading && ( e.Key == Key.Enter || e.Key == Key.Escape)) IsLoading = false;
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase LoadingScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
