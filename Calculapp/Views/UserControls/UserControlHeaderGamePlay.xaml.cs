﻿using Calculapp.Data;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlHeaderGamePlay.xaml.
	/// </summary>
	public partial class UserControlHeaderGamePlay : UserControl, INotifyPropertyChanged
	{
		//atributos
		private bool _isLevel = false;
		private bool _hasLose = false;
		private bool _hasTime = false;
		private TimeSpan _time;

		/// <summary>
		/// Constructor vacío de UserControlHeaderGamePlay().
		/// </summary>
		public UserControlHeaderGamePlay()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al cargar el control de usuario.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//carga de datos
			var module = DataHandler.SelectedModule;

			//personalización de la cabezera
			LabelModule.Content = GetModule();
			if (module != "Ordering" && module != "Count" && module != "MixedOperations")
			{
				IsLevel = true;
				HasLose = true;
				LabelLevel.Content = DataHandler.SelectedLevel.Number;
			}
			if (module == "Count") HasTime = true;
			else HasLose = true;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De IsLevel. Indica si el Gameplay es un nivel o no.
		/// </summary>
		public bool IsLevel
		{
			get => _isLevel;
			set
			{
				_isLevel = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De HasLose. Indica si en el Gameplay se puede perder o no.
		/// </summary>
		public bool HasLose
		{
			get => _hasLose;
			set
			{
				_hasLose = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsLevel. Indica si tiene tiempo.
		/// </summary>
		public bool HasTime
		{
			get => _hasTime;
			set
			{
				_hasTime = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsLevel. Indica el tiempo tanscurrido.
		/// </summary>
		private TimeSpan Time
		{
			get => _time;
			set
			{
				_time = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método de la clase UserControlHeaderGamePlay: Retorna el modulo seleccionado en español.
		/// </summary>
		/// <returns>Retorna un string con el modulo seleccionado</returns>
		private string GetModule()
		{
			if (DataHandler.SelectedModule == "Count") return "Conteo";
			else if (DataHandler.SelectedModule == "Ordering") return "Ordenamiento";
			else if (DataHandler.SelectedModule == "Addition") return "Suma";
			else if (DataHandler.SelectedModule == "Subtraction") return "Resta";
			else if (DataHandler.SelectedModule == "Multiplication") return "Multiplicación";
			else if (DataHandler.SelectedModule == "Division") return "División";
			else return "Operaciones Mixtas";
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase MainMenuSreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
