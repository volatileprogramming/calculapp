﻿using Calculapp.Data;
using Calculapp.Modules;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlGridRow.xaml.
	/// </summary>
	public partial class UserControlGridRow : UserControl
	{
		/// <summary>
		/// Constructor vacío de UserControlGridRow().
		/// </summary>
		public UserControlGridRow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase UserControlGridRowOperation: Se ejecuta al cargar el UserControl.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//setea propiedades
			var text = TextBoxMessage.Text;

			//setea el focus al TextBoxOperation
			if (string.IsNullOrEmpty(text))
			{
				while (!TextBoxMessage.IsFocused)
				{
					await Task.Delay(100);
					Keyboard.Focus(TextBoxMessage);
				}
			}
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De Index: Indica si el index del mensaje.
		/// </summary>
		public int Index { set; get; }

		/// <summary>
		/// SET & GET: De ParentControl: El UserControl padre.
		/// </summary>
		public UserControlDataGrid ParentControl { get; internal set; }

		#endregion

		#region Eventos del UserControlGridRow 

		/// <summary>
		/// Evento del UserControl UserControlGridRow: Se ejecuta al terminar la edición del TextBoxMessage.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void TextBoxMessage_LostFocus(object sender, RoutedEventArgs e)
		{
			var value = TextBoxMessage.Text;
			var messages = ParentControl.ContentList.ToObject<List<string>>();

			PlaySound("out.ogg");

			//Actualiza el dataHandler y el config con los datos del TexBox.
			if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
			{
				if (Index < messages.Count) messages.RemoveAt(Index);
				if ((StackPanel)this.Parent != null) ((StackPanel)this.Parent).Children.Remove(this);
			}
			else
			{
				if (Index < messages.Count) messages[Index] = value;
				else messages.Add(value);
			}

			ParentControl.ContentList = DataHandler.JSON["Messages"] = JArray.FromObject(messages);
			if (ParentControl.ChildrenCount < 9 && !ParentControl.HasPlussButton) ParentControl.HasPlussButton = true;
			DataHandler.Write("Config.bin");
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRow: Se ejecuta al hacer clic en ImageBasket.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImageBasket_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var text = TextBoxMessage.Text;

			if (!string.IsNullOrEmpty(text) | !string.IsNullOrWhiteSpace(text))
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				var messages = ParentControl.ContentList.ToObject<List<string>>();

				PlaySound("in.ogg");

				bool resoult = await myWindow.ShowMessage("Pregunta", "¿Desea eliminar este mensaje?", MessageDialogStyle.AffirmativeAndNegative);

				//Remueve el controlador del padre y el mensaje de DataHandler y el config.
				if (resoult)
				{
					if (!(Index >= messages.Count))
					{
						messages.RemoveAt(Index);
						ParentControl.ContentList = DataHandler.JSON["Messages"] = JArray.FromObject(messages);
					}
					if ((StackPanel)this.Parent != null) ((StackPanel)this.Parent).Children.Remove(this);
					DataHandler.Write("Config.bin");
				}
			}
		}

		#endregion

		#region Eventos del Mouse

		/// <summary>
		/// Evento del UserControl UserControlGridRow: Se ejecuta al entrar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageBasket_MouseEnter(object sender, MouseEventArgs e)
		{
			PlaySound("mouse_hover.mp3");

			var image = sender as Image;
			var svgReader = new SVGReader();

			image.Source = svgReader.Read(@"Icons\Garbage-Closed.svg");
			image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRow: Se ejecuta al sacar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageBasket_MouseLeave(object sender, MouseEventArgs e)
		{
			//PlaySound("mouse_leave.mp3");

			var image = sender as Image;
			var svgReader = new SVGReader();

			image.Source = svgReader.Read(@"Icons\Garbage-Closed-01.svg");
			image.OpacityMask = new SolidColorBrush { Opacity = 0.65, Color = Colors.Black };
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
	}
}
