﻿using Calculapp.Data;
using Calculapp.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlOrderingContainer.xaml
	/// </summary>
	public partial class UserControlOrderingContainer : UserControl, IGameplayContainer, INotifyPropertyChanged
	{
		//atributos
		private static UserControlOrderingContainer _instace;
		private bool _isAnimating = true;
		private double[] _answers;

		/// <summary>
        /// Constructor vacío de UserControlOrderingContainer().
        /// </summary>
		public UserControlOrderingContainer()
		{
			InitializeComponent();
        }

		/// <summary>
		/// Evento de la clase UserControlOrderingContainer: Se ejecuta al cargar el controlador.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//espera a que las animaciones finalizen
			IsAnimating = true;

            //muesta el orden
            GridImagesContainer.Children.Clear(); //limpia
			if (Order) GridImagesContainer.Children.Add(ImageArrowRight); //asencente
			else GridImagesContainer.Children.Add(ImageArrowLeft); //desendente

            //instancia y añade la ventana de ayuda.
            var help = new UserControlHelp("/Resources/Gifs/arrange_help.gif");
            Grid.SetRow(help, 1);
            Grid.SetRowSpan(help, 3);
            Grid.SetColumn(help, 1);

            await Task.Delay(350);
            MainGrid.Children.Add(help);
            //setea posicion de la respuesta
            //AnswerPosition = BorderAnswer.TransformToAncestor(MainGrid).Transform(new Point(0, 0));
            IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De AnswerPoints. Almacena la lista de posiciones.
		/// </summary>
		public List<Point> AnswerPoints { get; set; } = new List<Point>();

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = !(_isAnimating = value);
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MouseLastPosition. Almacena la posicion anterior del mouse.
		/// </summary>
		private MousePosition MouseLastPosition { set; get; } = new MousePosition();

		/// <summary>
		/// SET & GET: De Instance. Retorna la inatancia del controlador.
		/// </summary>
		public static UserControlOrderingContainer Instance
		{
			get
			{
				if (_instace == null) return _instace = new UserControlOrderingContainer();
				else  return _instace;
			}
			set {  _instace = value; }
		}

		private InputHandler Handler { get; set; } = new InputHandler();

		/// <summary>
		/// SET & GET: De Answers. Almacena e instancia la lista de respuestas actuales.
		/// </summary>
		public double[] Answers
		{
			get => _answers;
			set
			{
				var length = value.Length;
				_answers = value;

				//limpia el contenedor
				Resect();

				//setea las filas y collumnas
				SetGrid(length);

				//setea los controladores
				for (var i = 0; i < length; i++)
				{
					//crea controles
					var control = new UserControlSquare();
					var answer = new UserControlAnswerTarget();

					//setea los atributos
					if (!DataHandler.FullScreen)
					{
						control.Margin = new Thickness { Right = 5, Top = 5, Left = 5, Bottom = 5 };
						answer.Margin = new Thickness { Right = 5, Top = 5, Left = 5, Bottom = 5 };
					}

					control.BorderAnswer.Background = GetRandomBrush();
					control.Number = _answers[i];
					control.PreviewMouseDown += Handler.PreviewMouseDown;
					control.MouseLeftButtonDown += UserControlSquare_MouseLeftButtonDown;

					//setea columnas y filas
					//answersTarget
					if (i < 10) Grid.SetRow(answer, 0);
					else Grid.SetRow(answer, 1);
					if (i < 10) Grid.SetColumn(answer, i);
					else Grid.SetColumn(answer, i-10);
					//controls
					if (length > 10 && i < 10) Grid.SetRow(control, 3);
					else if (length > 10 && i >= 10) Grid.SetRow(control, 4);
					else Grid.SetRow(control, 2);
					if (i < 10) Grid.SetColumn(control, i);
					else Grid.SetColumn(control, i-10);

					//instancia en el padre
					TargetList.Add(answer);
					GridControlsContainer.Children.Add(answer);
					GridControlsContainer.Children.Add(control);
				}
				
			}
		}

		/// <summary>
		/// SET & GET: De gameplay. Almacena la instancia del gameplay.
		/// </summary>
		public IGameplay Gameplay { set; get; }

		/// <summary>
		/// SET & GET: De SelectecControl. Almacena la instancia del gameplay.
		/// </summary>
		private UserControlSquare SelectecControl { set; get; }

		/// <summary>
		/// SET & GET: De TextReader. Lee los numeros en voz alta.
		/// </summary>
		private TextSynthesizer TextReader { get; set; } = new TextSynthesizer();

		/// <summary>
		/// SET & GET: De MyRandom: Generador de numeros aleatorios.
		/// </summary>
		private readonly Random MyRandom = new Random();

		/// <summary>
		/// SET & GET: De Brushes: Almacena una lista Brush.
		/// </summary>
		private List<Brush> MyBrushes { set; get; } = GetBrushes();

		public List<UserControlAnswerTarget> TargetList { get; set; } = new List<UserControlAnswerTarget>();
		public bool Order { get; internal set; }
		public int Range { get; internal set; }
		public int StartValue { get; internal set; }

		#endregion

		#region Eventos

		/// <summary>
		/// Evento de la clase UserControlOrderingContainer: Se ejecuta al hacer clic en UserControlSquare.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControlSquare_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var control = sender as UserControlSquare;

			if(!IsAnimating && SelectecControl == null && !control.IsLooked)
			{
				var gameplay = Gameplay as GameplayOrdering;
				var number = control.Number;
				var index = number - StartValue;
				//var index = (Order) ?  number - StartValue : (StartValue + Range) - (number + 1);
				var answeTarget = TargetList[(int)index];

				//almacena el control seleccionado
				SelectecControl = control;

				//Efectos de sonido
				TextReader.ReadAloudAsync($"{SelectecControl.Number}");

				//pone el control por ensima de los demas
				GridControlsContainer.Children.Remove(control);
				GridControlsContainer.Children.Add(control);

				//envia la respuesta al gameplay
				await Task.Delay(400);
				gameplay.SetAnswer(SelectecControl, answeTarget.Position);
				await Task.Delay(400);
				
				//libera el espacio en memoria
				SelectecControl = null;
			}
		}

		#endregion

		#region Métodos

		public void SetGrid(int count)
		{
			var length = (count > 10) ? 2 : 1;
			var max = (count > 10) ? 10 : count;
			var index = 0;

			for(var i = 0; i < 2; i++)
			{
				for (var a = 0; a < length; a++)
				{
					GridControlsContainer.RowDefinitions.Insert(index++, new RowDefinition());
				}
				if (i == 1) break;
				GridControlsContainer.RowDefinitions.Insert(index++, 
					new RowDefinition { MinHeight=20, MaxHeight=40 });
			}

			for(var b = 0; b < max; b++)
			{
				GridControlsContainer.ColumnDefinitions.Insert(b, new ColumnDefinition());
			}
		}

		public void Resect()
		{
			//elimina todos los hijos
			TargetList.Clear();
			GridControlsContainer.Children.Clear();

			//elimina las filas y columnas
			GridControlsContainer.RowDefinitions.Clear();
			GridControlsContainer.ColumnDefinitions.Clear();
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Optiene la posicion relativa del ratón.
		/// </summary>
		/// <param name="pointA">Indica la posicion actual del objeto.</param>
		/// <param name="pointB">Indica la posicion de referencia.</param>
		/// <param name="size">Indica las dimenciones del control.</param>
		/// <returns>Retorna un double con la posicion relativa del ratón.</returns>
		private double GetDiferences(double pointA, double pointB, double size = 0)
		{
			if (size != 0)
				return pointA - (pointB + (size / 2));
			else
				return pointA - pointB + 10;
		}

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Optiene el contenedor padre.
		/// </summary>
		/// <returns>Retorna un Grid con el contenedor padre.</returns>
		public Grid GetParent() => ((Grid)this.Parent);

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Optiene la lista de colores apropiados.
		/// </summary>
		/// <returns>Retorna un List<Brush> con los colores para el Gameplay.</returns>
		//private static List<Brush> GetBrushes()
		//{
		//	var brushesNames = typeof(Brushes).GetProperties(BindingFlags.Public | BindingFlags.Static);
		//	var brushes = new List<Brush>();

		//	foreach(var propInfo in brushesNames)
		//	{
		//		var brush = (Brush)propInfo.GetValue(null, null);
		//		if(!Contains(brush)) brushes.Add(brush);
		//	}

		//	return brushes;
		//}

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Genera un Color aleatoriamente.
		/// </summary>
		/// <returns>Retorna un Color generado aleatoriamente.</returns>
		private Brush GetRandomBrush() { return MyBrushes[MyRandom.Next(MyBrushes.Count)]; }

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Optiene la lista de colores apropiados.
		/// </summary>
		/// <returns>Retorna un List<Brush> con los colores para el Gameplay.</returns>
		private static List<Brush> GetBrushes()
		{
			return new List<Brush>
			{
				Brushes.Blue,
				Brushes.BlueViolet,
				Brushes.BurlyWood,
				Brushes.CadetBlue,
				Brushes.Chartreuse,
				Brushes.Chocolate,
				Brushes.Coral,
				Brushes.CornflowerBlue,
				Brushes.DarkBlue,
				Brushes.DarkGoldenrod,
				Brushes.DarkGreen,
				Brushes.DarkMagenta,
				Brushes.DarkSalmon,
				Brushes.DarkSeaGreen,
				Brushes.DarkSlateBlue,
				Brushes.DarkSlateGray,
				Brushes.DarkTurquoise,
				Brushes.DarkViolet,
				Brushes.DeepPink,
				Brushes.DodgerBlue,
				Brushes.Firebrick,
				Brushes.ForestGreen,
				Brushes.Red,
				Brushes.Orange,
				Brushes.Indigo,
				Brushes.Green,
				Brushes.Fuchsia,
				Brushes.Navy,
				Brushes.Olive,
				Brushes.OrangeRed,
			};
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}

		#endregion
	}
}
