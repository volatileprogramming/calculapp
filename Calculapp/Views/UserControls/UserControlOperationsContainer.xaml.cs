﻿using Calculapp.Data;
using Calculapp.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlOperationsContainer.xaml
	/// </summary>
	public partial class UserControlOperationsContainer : UserControl, IGameplayContainer, INotifyPropertyChanged
	{
		//atributos
		private static UserControlOperationsContainer _instace = null;
		private string _operation;
		private double[] _answers = new double[4];
		private bool _isAnimating = true;

		/// <summary>
        /// Constructor vacío de UserControlOperationsContainer().
        /// </summary>
		public UserControlOperationsContainer()
		{
			InitializeComponent();
        }

		/// <summary>
		/// Evento de la clase UserControlOperationsContainer: Se ejecuta al cargar el controlador.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//espera a que las animaciones finalizen
			IsAnimating = true;

            //cambia el color del fondo cada 5 segundos
            //GetRandomBrush(); //pendiente

            //instancia y añade la ventana de ayuda.
            var help = new UserControlHelp("/Resources/Gifs/calculation_help.gif");
            Grid.SetRow(help, 0);
            Grid.SetRowSpan(help, 4);
            Grid.SetColumn(help, 1);
            Grid.SetColumnSpan(help, 4);

            await Task.Delay(350);
            //setea posicion de la respuesta
            AnswerPosition = BorderAnswer.TransformToAncestor(MainGrid).Transform(new Point(0, 0));
            MainGrid.Children.Add(help);
			IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = !(_isAnimating = value);
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MouseLastPosition. Almacena la posicion anterior del mouse.
		/// </summary>
		private MousePosition MouseLastPosition { set; get; } = new MousePosition();

		/// <summary>
		/// SET & GET: De AnswerPosition. Almacena la posicion de la respuestas.
		/// </summary>
		public Point AnswerPosition { get; private set; }

		/// <summary>
		/// SET & GET: De Instance. Retorna la inatancia del controlador.
		/// </summary>
		public static UserControlOperationsContainer Instance
		{
			get
			{
				if (_instace == null) return _instace = new UserControlOperationsContainer();
				else  return _instace;
			}
			set {  _instace = value; }
		}

		/// <summary>
		/// SET & GET: De Operation. Almacena la operacion actial.
		/// </summary>
		public string Operation
		{
			get => _operation;
			set
			{
				OperationControl.Operation = _operation = value;
			}
		}

		private InputHandler Handler { get; set; } = new InputHandler();

		/// <summary>
		/// SET & GET: De Answers. Almacena e instancia la lista de respuestas actuales.
		/// </summary>
		public double[] Answers
		{
			get => _answers;
			set
			{
				_answers = value;

				//limpia el contenedor
				for(int i = 0; i < MainGrid.Children.Count; i++)
				{
					var control = MainGrid.Children[i] as UserControlNumber;
					if (control != null)
					{
						MainGrid.Children.Remove(control);
						i--;
					}
				}

				//instancia los controladores
				for(var i = 0; i < _answers.Length; i++)
				{
					//setea atributos
					var control = new UserControlNumber();
					//var translateTransform = new TranslateTransform();

					//setea los atributos
					control.BorderAnswer.Background = GetRandomBrush();
					control.Number = _answers[i];
					control.PreviewMouseDown += Handler.PreviewMouseDown;
					control.MouseMove += Grid_MouseMove;
					control.MouseLeftButtonDown += UserControlNumber_MouseLeftButtonDown;
					control.PreviewMouseUp += Handler.PreviewMouseUp;
					control.MouseLeftButtonUp += UserControlNumber_MouseLeftButtonUp;

					//setea la columnas y filas
					Grid.SetRow(control, 3);
					Grid.SetColumn(control, i+1);

					//instancia en el padre
					MainGrid.Children.Add(control);
					//control.Position = control.TransformToAncestor(MainGrid).Transform(new Point(0, 0));
				}
			}
		}

		/// <summary>
		/// SET & GET: De gameplay. Almacena la instancia del gameplay.
		/// </summary>
		public IGameplay Gameplay { set; get; }

		/// <summary>
		/// SET & GET: De SelectecControl. Almacena la instancia del gameplay.
		/// </summary>
		private UserControlNumber SelectecControl { set; get; }

		/// <summary>
		/// SET & GET: De TextReader. Lee los numeros en voz alta.
		/// </summary>
		private TextSynthesizer TextReader { get; set; } = new TextSynthesizer();

		/// <summary>
		/// SET & GET: De MyRandom: Generador de numeros aleatorios.
		/// </summary>
		private readonly Random MyRandom = new Random();
		
		/// <summary>
		/// SET & GET: De Brushes: Almacena una lista Brush.
		/// </summary>
		private List<Brush> MyBrushes { set; get; } = GetBrushes();

		#endregion

		#region Eventos

		/// <summary>
		/// Evento de la clase UserControlOperationsContainer: Se ejecuta al mover el ratón.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Grid_MouseMove(object sender, MouseEventArgs e)
		{
			await Task.Delay(DataHandler.FrameRate);
			var control = sender as UserControlNumber;

			if (!IsAnimating && SelectecControl == control && e.LeftButton == MouseButtonState.Pressed)
			{
				DragControl(control, e);
			}
		}

		/// <summary>
		/// Evento de la clase UserControlOperationsContainer: Se ejecuta al hacer clic en UserControlNumber.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void UserControlNumber_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if(!IsAnimating && SelectecControl == null)
			{
				var control = sender as UserControlNumber;
				var translateTransform = new TranslateTransform();
				
				//almacena el control seleccionado
				SelectecControl = control;
				SelectecControl.RenderTransform = translateTransform;

				//Efectos de sonido
				TextReader.ReadAloudAsync($"{SelectecControl.Number}");

				//pone el control por ensima de los demas
				MainGrid.Children.Remove(control);
				MainGrid.Children.Add(control);
			}
		}

		/// <summary>
		/// Evento de la clase UserControlOperationsContainer: Se ejecuta al sortar clic en UserControlNumber.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControlNumber_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if(!IsAnimating && SelectecControl != null)
			{
				IsAnimating = true;

				//setea los atributos
				var relativePoint = SelectecControl.TransformToAncestor(MainGrid).Transform(new Point(0, 0));
				var X = GetDiferences(AnswerPosition.X, SelectecControl.Position.X);
				var Y = GetDiferences(AnswerPosition.Y, SelectecControl.Position.Y);

				//setea la duracion
				var duration = new Duration(TimeSpan.FromMilliseconds(400));

				//setea la animacion
				var animationX = new DoubleAnimation(X, duration);
				var animationY = new DoubleAnimation(Y, duration);

				//realiza la animacion
				SelectecControl.RenderTransform.BeginAnimation(TranslateTransform.XProperty, animationX);
				SelectecControl.RenderTransform.BeginAnimation(TranslateTransform.YProperty, animationY);

				//envia la respuesta al gameplay
				await Task.Delay(1400);
				Gameplay.SelectedAnswer = SelectecControl;
				await Task.Delay(400);
				
				//libera el espacio en memoria
				SelectecControl = null;
				IsAnimating = false;
			}
		}

		#endregion

		#region Métodos
		
		/// <summary>
		/// Método de la clase SetIndicator: Inicializa el indicador del nivel.
		/// </summary>
		/// <param name="list">Lista de operaciones del nivel.</param>
		public void SetIndicator(List<string> list)
		{
			var count = 1;

			//limpia el contenedor
			for(int i = 0; i < DockPanelLevelIndicator.Children.Count; i++)
			{
				var control = DockPanelLevelIndicator.Children[i] as UserControlLevelIndicator;
				if (control != null)
				{
					DockPanelLevelIndicator.Children.Remove(control);
					i--;
				}
			}

			//instancia los controles
			foreach (var item in list)
			{
				var indicator = new UserControlLevelIndicator();

				if (count == 1)
				{
					indicator.BorderIndicator.Margin = new Thickness {  Left = 0, Bottom = 3, Right = 0, Top = 3 };
					indicator.BorderIndicator.Background = Brushes.White;
					indicator.BorderIndicator.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
				}

				DockPanelLevelIndicator.Children.Add(indicator);
				count++;
			}
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase UserControlOperationsContainer: Realiza el drag de los controles.
		/// </summary>
		/// <param name="number">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void DragControl(UserControlNumber number, MouseEventArgs e)
		{
			//propiedades
			var position = e.GetPosition(MainGrid);

			if (MouseLastPosition.X != position.X || MouseLastPosition.Y != position.Y)
			{
				var translateTransform = new TranslateTransform();

				//Setea la posicion del usercontrol
				number.RenderTransform = translateTransform;
				translateTransform.X = GetDiferences(position.X, number.Position.X, number.ActualWidth);
				translateTransform.Y = GetDiferences(position.Y, number.Position.Y, number.ActualHeight);

				//recuerda la posisicion
				MouseLastPosition.Y = position.Y;
				MouseLastPosition.X = position.X;
			}
		}

		/// <summary>
		/// Método de la clase UserControlOperationsContainer: Optiene la posicion relativa del ratón.
		/// </summary>
		/// <param name="pointA">Indica la posicion actual del objeto.</param>
		/// <param name="pointB">Indica la posicion de referencia.</param>
		/// <param name="size">Indica las dimenciones del control.</param>
		/// <returns>Retorna un double con la posicion relativa del ratón.</returns>
		private double GetDiferences(double pointA, double pointB, double size = 0)
		{
			if (size != 0)
				return pointA - (pointB + (size / 2));
			else
				return pointA - pointB;
		}

		/// <summary>
		/// Método de la clase UserControlOperationsContainer: Optiene el contenedor padre.
		/// </summary>
		/// <returns>Retorna un Grid con el contenedor padre.</returns>
		public Grid GetParent() => ((Grid)this.Parent);

		/// <summary>
		/// Método de la clase UserControlOperationsContainer: Optiene la lista de colores apropiados.
		/// </summary>
		/// <returns>Retorna un List<Brush> con los colores para el Gameplay.</returns>
		//private static List<Brush> GetBrushes()
		//{
		//	var brushesNames = typeof(Brushes).GetProperties(BindingFlags.Public | BindingFlags.Static);
		//	var brushes = new List<Brush>();

		//	foreach(var propInfo in brushesNames)
		//	{
		//		var brush = (Brush)propInfo.GetValue(null, null);
		//		if(!Contains(brush)) brushes.Add(brush);
		//	}

		//	return brushes;
		//}

		/// <summary>
		/// Método de la clase UserControlOperationsContainer: Genera un Color aleatoriamente.
		/// </summary>
		/// <returns>Retorna un Color generado aleatoriamente.</returns>
		private Brush GetRandomBrush() { return MyBrushes[MyRandom.Next(MyBrushes.Count)]; }

		/// <summary>
		/// Método de la clase UserControlOrderingContainer: Optiene la lista de colores apropiados.
		/// </summary>
		/// <returns>Retorna un List<Brush> con los colores para el Gameplay.</returns>
		private static List<Brush> GetBrushes()
		{
			return new List<Brush>
			{
				Brushes.BlueViolet,
				Brushes.Brown,
				Brushes.BurlyWood,
				Brushes.CadetBlue,
				Brushes.Chartreuse,
				Brushes.Chocolate,
				Brushes.Coral,
				Brushes.CornflowerBlue,
				Brushes.Crimson,
				Brushes.DarkGoldenrod,
				Brushes.DarkGreen,
				Brushes.DarkMagenta,
				Brushes.DarkRed,
				Brushes.DarkSalmon,
				Brushes.DarkSeaGreen,
				Brushes.DarkSlateGray,
				Brushes.DarkTurquoise,
				Brushes.DarkViolet,
				Brushes.DeepPink,
				Brushes.DeepSkyBlue,
				Brushes.DodgerBlue,
				Brushes.Firebrick,
				Brushes.ForestGreen,
				Brushes.Red,
				Brushes.Orange,
				Brushes.Maroon,
				Brushes.Indigo,
				Brushes.Green,
				Brushes.Fuchsia,
				Brushes.Navy,
				Brushes.Olive,
				Brushes.OrangeRed,
			};
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
