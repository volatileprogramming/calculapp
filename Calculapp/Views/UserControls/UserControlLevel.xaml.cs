﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.Screens;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlLevel.xaml.
	/// </summary>
	public partial class UserControlLevel : UserControl
	{
		//atributos
		private bool _passed = false;

		/// <summary>
		/// Constructor vacío de UserControlLevel().
		/// </summary>
		public UserControlLevel()
		{
			InitializeComponent();
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De Passed: Indica si el nivel esta pasado o no.
		/// </summary>
		public bool Passed
		{
			get => _passed;
			set
			{
				var svgReader = new SVGReader();
				Locked = false;
				_passed = value;

				if (!value) ImageLevel.Source = svgReader.Read(@"Images\Level-Unselected.svg");
				else ImageLevel.Source = svgReader.Read(@"Images\Passed-Level-Unselected.svg");
			}
		}

		/// <summary>
		/// SET & GET: De Looked: Indica si el nivel esta bloqueado.
		/// </summary>
		public bool Locked { get; set; } = true;

		/// <summary>
		/// SET & GET: De Level: Indica las propiedades del nivel.
		/// </summary>
		public Level Level { get; internal set; }

		/// <summary>
		/// SET & GET: De MyProvince: Almacena los datos de la provincia.
		/// </summary>
        public Province MyProvince { get; internal set; }

        #endregion

        #region Eventos del Mouse

		/// <summary>
		/// Evento del UserControl UserControlLevel: Se ejecuta al hacer clic en ImageLevel.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageLevel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (!Locked)
			{
				PlaySound("in.ogg");
				//DataHandler.IsAnimating = true;

				var myWindow = Application.Current.MainWindow as MainWindow;
				var page = myWindow.GetCurrentPage() as MappScreen;
                var infoCard = new UserControlInfoCard(MyProvince);

				DataHandler.SelectedLevel = Level;
				page.MainGridContainer.Children.Add(infoCard);
                //((Canvas)this.Parent).Children.Add(infoCard);
			}
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
	}
}
