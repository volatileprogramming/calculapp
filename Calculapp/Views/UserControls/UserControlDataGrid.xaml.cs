﻿using Calculapp.Data;
using Calculapp.Views.Screens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlDataGrid.xaml.
	/// </summary>
	public partial class UserControlDataGrid : UserControl
	{
		//atributos
		private bool _hasPlussButton = true;
		private int _maxHeightRows = 26;
		private bool _hasWrongOperation;

		/// <summary>
		/// Constructor vacío de UserControlDataGrid().
		/// </summary>
		public UserControlDataGrid()
		{
			InitializeComponent();
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De UserControlDataGrid: Indica si UserControlNewButton esta o no instanciado dentro de StackPanelTable.
		/// </summary>
		public bool HasPlussButton
		{
			get => _hasPlussButton;
			set
			{
				_hasPlussButton = value;
				if (!value) StackPanelTable.Children.Remove(UserControlNewButton);
				else if (!(GetRow() is UserControlGridRow & ChildrenCount >= 10)) AddChild(UserControlNewButton);
			}
		}

		/// <summary>
		/// SET & GET: De UserControlDataGrid: Indica la cantidad de hijos de StackPanelTable.
		/// </summary>
		public int ChildrenCount { get; private set; }

		/// <summary>
		/// SET & GET: De IsLoading: Indica la si se estan cargando los datos de la tabla.
		/// </summary>
		public bool IsLoading { get; private set; } = false;

		/// <summary>
		/// SET & GET: De ContentList: Almacena la lista de contenidos de la tabla.
		/// </summary>
		public JToken ContentList { get; set; }

		/// <summary>
		/// SET & GET: De RowType: Indica el tipo de filas de la tabla.
		/// </summary>
		public UserControl RowType { set; get; }

		/// <summary>
		/// SET & GET: De RowType: Indica el alto maximo de las filas.
		/// </summary>
		public int MaxHeightRows
		{
			set
			{
				_maxHeightRows = value;
				foreach (dynamic child in StackPanelTable.Children)
				{
					if (child is UserControlGridRow) child.TextBoxMessage.Height = value;
				}
			}
			get => _maxHeightRows;
		}

		/// <summary>
		/// SET & GET: De HasWrongOperation: Indica si tiene una operación incorrecta o no.
		/// </summary>
		public bool HasWrongOperation
		{
			get => _hasWrongOperation;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				var page = myWindow.GetCurrentPage() as OptionsScreen;
				
				page.IsLocked = _hasWrongOperation = value;
			}
		}

		/// <summary>
		/// SET & GET: De SelectedSectionName: Indica la sección seleccionada.
		/// </summary>
		public string SelectedSectionName { get; internal set; }

		public UserControlGridRowOperation UserControlGridRowOperation
		{
			get => default(UserControlGridRowOperation);
			set
			{
			}
		}

		public UserControlGridRow UserControlGridRow
		{
			get => default(UserControlGridRow);
			set
			{
			}
		}

		public UserControlButtonNew UserControlButtonNew
		{
			get => default(UserControlButtonNew);
			set
			{
			}
		}

		public MixedOperation MixedOperation
		{
			get => default(MixedOperation);
			set
			{
			}
		}

		#endregion

		#region Eventos

		/// <summary>
		/// Evento del UserControlDataGrid: Se ejecuta al hacer clic en UserControlNewButton.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void UserControlNewButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (HasWrongOperation) e.Handled = true;
			else
			{
				HasPlussButton = false;

				var row = GetRow();
				var list = GetList();

				row.Index = (list.Count >= 0) ? list.Count : 0;
				row.ParentControl = this;
				if (row is UserControlGridRowOperation)
				{
					row.TextBoxOperationText = "";
					row.SelectedSectionName = SelectedSectionName;
				}

				StackPanelTable.Children.Add(row);
			}
		}

		/// <summary>
		/// Evento del UserControlDataGrid: Se ejecuta al actualizar la tabla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void StackPanelTable_LayoutUpdated(object sender, EventArgs e)
		{
			if (IsLoading) return;
			if (StackPanelTable.Children.Count < ChildrenCount) Reload();
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método del UserControlDataGrid: Deshavilita los demas controles.
		/// </summary>
		/// <param name="control">Control a ser editado.</param>
		public void EditRow(object control)
		{
			HasWrongOperation = true;

			foreach (dynamic iten in StackPanelTable.Children)
			{
				if(iten != control) iten.IsEnabled = false;
			}
		}

		/// <summary>
		/// Método del UserControlDataGrid: Habilita todos los controles.
		/// </summary>
		public void FinishEdition()
		{
			HasWrongOperation = false;

			foreach (dynamic iten in StackPanelTable.Children) iten.IsEnabled = true;
		}

		/// <summary>
		/// Método del UserControlDataGrid: Recarga los datos de la tabla.
		/// </summary>
		public void Reload()
		{
			var list = GetList();

			if (list != null)
			{
				HasPlussButton = false;
				IsLoading = true;
				StackPanelTable.Children.Clear();
			
				for (int i=0; i < list.Count; i++)
				{
					dynamic row = GetRow();

					if (row is UserControlGridRow)
					{
						row.TextBoxMessage.Text = (string)list[i];
					}
					else
					{
						var temp = (MixedOperation)list[i];
						row.TextBoxOperationText = temp.Operation;
						row.CheckBoxPlayList.IsChecked = temp.Active;
						row.SelectedSectionName = SelectedSectionName;
					}

					row.Index = i;
					row.ParentControl = this;
					StackPanelTable.Children.Add(row);
				}

				ChildrenCount = StackPanelTable.Children.Count;
				HasPlussButton = true;
				IsLoading = false;
			}
		}

		/// <summary>
		/// Método del UserControlDataGrid: Obtiene la lista seleccionada.
		/// </summary>
		/// <returns>Retorna la lista seleccionada.</returns>
		internal dynamic GetList()
		{
			var strg = ContentList.ToString();
			if (RowType is UserControlGridRow) return JsonConvert.DeserializeObject<List<object>>(ContentList.ToString());
			else return ContentList.ToObject<List<MixedOperation>>();
		}

		/// <summary>
		/// Método del UserControlDataGrid: Obtiene el UserControl seleccionado.
		/// </summary>
		/// <returns>Retorna el UserControl seleccionado.</returns>
		internal dynamic GetRow()
		{
			if (RowType is UserControlGridRow) return new UserControlGridRow();
			else return new UserControlGridRowOperation();
		}

		/// <summary>
		/// Método del UserControlDataGrid: Agrega un elemento hijo a StackPanelTable.
		/// </summary>
		/// <param name="userControl">Elemento a ser agregado a StackPanelTable.</param>
		internal void AddChild(UserControl userControl)
		{
			StackPanelTable.Children.Add(userControl);
		}

		#endregion
	}
}
