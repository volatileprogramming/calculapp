﻿using System.Windows.Controls;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlOperation.xaml
	/// </summary>
	public partial class UserControlOperation : UserControl
	{
		//atributos
		private string _operation;

		/// <summary>
		/// Constructor vacío de UserControlOperation().
		/// </summary>
		public UserControlOperation()
		{
			InitializeComponent();
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De Operation. Almacena la operacion.
		/// </summary>
		public string Operation
		{
			get => _operation;
			set
			{
				TextBlockOperation.Text = _operation = value;
			}
		}

		#endregion
	}
}
