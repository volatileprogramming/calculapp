﻿using Calculapp.Data;
using Calculapp.Modules;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlButtonNew.xaml.
	/// </summary>
	public partial class UserControlButtonNew : UserControl
	{
		/// <summary>
		/// Constructor vacío de UserControlButtonNew().
		/// </summary>
		public UserControlButtonNew()
		{
			InitializeComponent();
		}

		#region Eventos del Mouse

		/// <summary>
		/// Evento de la clase UserControlButtonNew: Se ejecuta al entrar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void Grid_MouseEnter(object sender, MouseEventArgs e)
		{
			PlaySound("mouse_hover.mp3");

			var image = sender as Grid;
			image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento de la clase UserControlButtonNew: Se ejecuta al sacar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void Grid_MouseLeave(object sender, MouseEventArgs e)
		{
			//PlaySound("mouse_leave.mp3");

			var image = sender as Grid;
			image.OpacityMask = new SolidColorBrush { Opacity = 0.65, Color = Colors.Black };
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
	}
}
