﻿using Calculapp.Data;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculapp.Views.UserControls
{
    /// <summary>
    /// Interaction logic for UserControlCredits.xaml
    /// </summary>
    public partial class UserControlCredits : UserControl
    {
        public UserControlCredits()
        {
            InitializeComponent();
        }

        public void exitCredits()
        {
            DataHandler.IsAnimating = false;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var jsonReader = new JSONHandler();
            var credits = jsonReader.ReadBin("Credits.bin");
            CredtisParser(credits);
        }

        /// <summary>
        /// Da formato al textBlock para realizar la salida en un formato paresido al de los creditos.
        /// </summary>
        /// <param name="j_object">objeto json sin formato de credito</param>
        private void CredtisParser(JObject j_object)
        {
            foreach( var categorias in j_object)
            {
                string categoriaString = categorias.Key;

                var textBlockCategoria = CreditsTextBlock(categoriaString, true);
                creditsPanel.Children.Add(textBlockCategoria);

                foreach (var miembros in j_object[categoriaString])
                {
                    string nombreString  = miembros["Nombre"].ToString();
                    string apellidoString = miembros["Apellido"].ToString();

                    var textBlockMiembro = CreditsTextBlock((string)(nombreString + " " + apellidoString));
                    creditsPanel.Children.Add(textBlockMiembro);
                }
                creditsPanel.Children.Add(CreditsTextBlock("  "));
            }
        }

        /// <summary>
        /// Crea un textBlock que ocupa toda la pantalla horizontalmente y muestra el texto centralizado en la pantalla.
        /// </summary>
        /// <param name="text">Texto a mostrar en pantalla</param>
        /// <param name="isHeader">Si es "true" el texto sera de un tamaño mas grande</param>
        /// <returns></returns>
        private TextBlock CreditsTextBlock(string text, bool isHeader = false)
        {
            TextBlock textBock = new TextBlock();

            textBock.Text = text;
            textBock.Foreground = Brushes.White;
            if (isHeader) textBock.FontSize = DataHandler.GlobalFontSize * 2;
            else textBock.FontSize = DataHandler.GlobalFontSize;
            textBock.HorizontalAlignment = HorizontalAlignment.Center;
            textBock.TextAlignment = TextAlignment.Center;

            return textBock;
        }
    }
}
