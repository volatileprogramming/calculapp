﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.Screens;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for userControlInfoCard.xaml
	/// </summary>
	public partial class UserControlInfoCard : UserControl, INotifyPropertyChanged
    {
		//atributos
        private Image _imageMonument = new Image();
        private string _textName;
        private string _textDescription;

		/// <summary>
		/// Constructor vacío de UserControlInfoCard().
		/// </summary>
		public UserControlInfoCard()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Constructor de UserControlInfoCard(JToken provincia).
		/// </summary>
		/// <param name="province">Datos de la provincia.</param>
        public UserControlInfoCard(Province province)
        {
            InitializeComponent();
            
            ImageMonument.Source = new BitmapImage(new Uri(province.Image, UriKind.Relative));
            SourceTextName = province.Name;
            SourceTextDescription = province.Description;
			Province = province;
		}

        #region SETTERS AND GETTERS

		/// <summary>
        /// SET & GET: De MyProvince. Almacena los datos de la provincia.
        /// </summary>
		public Province MyProvince { set; get; }

        /// <summary>
        /// SET & GET for the province name incide a TextBlock control
        /// </summary>
        public string SourceTextName
        {
            get => _textName;
            set
            {
                _textName = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// SET & GET for the province description incide a TextBlock control
        /// </summary>
        public string SourceTextDescription
        {
            get => _textDescription;
            set
            {
                _textDescription = value;
                OnPropertyChanged();
            }
        }

		public Province Province { get; }

		#endregion

		#region Eventos

		/// <summary>
		/// Evento del UserControl UserControlInfoCard: Se ejecuta al hacer clic en ImagePlay.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImagePlay_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			PlaySound("transition.ogg");
			DataHandler.IsAnimating = true;

			var myWindow = Application.Current.MainWindow as MainWindow;

			await Task.Delay(1000);
			myWindow.FrameScreenNavigator.Content = new GamePlayScreen();
		}

		/// <summary>
		/// Evento del UserControl UserControlInfoCard: Se ejecuta al hacer clic en ImageClose.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			PlaySound("in.ogg");

			DataHandler.SelectedLevel = null;
			((Grid)this.Parent).Children.Remove(this);
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Evento de la clase MainWindow: Provoca el evento PropertyChanged de este objeto.
        /// </summary>
        /// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
        protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }


		#endregion

		private TextSynthesizer VoiceReader = new TextSynthesizer();
		private bool reading = false;

		private async void ImageListen_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (reading) return;
			VoiceReader.ReadAloud($"{Province.Name}. {Province.Description}");
			reading = true;
			await Task.Delay(1000);
			reading = false;
		}
	}
}
