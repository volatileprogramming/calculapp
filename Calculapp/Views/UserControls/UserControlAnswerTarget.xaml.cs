﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlAnswerTarget.xaml
	/// </summary>
	public partial class UserControlAnswerTarget : UserControl
	{
		/// <summary>
        /// Constructor vacío de UserControlAnswerTarget().
        /// </summary>
		public UserControlAnswerTarget(bool isDargTarget = false)
		{
			InitializeComponent();
			IsDargTarget = isDargTarget;
		}

		/// <summary>
		/// Evento de la clase UserControlAnswerTarget: Se ejecuta al cargar el controlador.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			dynamic parent = this.Parent as Grid;
			if (parent == null) parent = this.Parent as DockPanel;
			if (parent == null) parent = this.Parent as WrapPanel;

			await Task.Delay(350);

			Position = this.TransformToAncestor(parent).Transform(new Point(0, 0));
		}

		#region SETTERS AND GETTES

		/// <summary>
		/// SET & GET: De Position. Indica si es un target para dropeos.
		/// </summary>
		public bool IsDargTarget { get; set; }

		/// <summary>
		/// SET & GET: De Position. Almacena la posicion por defecto del controlador.
		/// </summary>
		public Point Position { set; get; }

		#endregion
	}

	#region Extenciones

	/// <summary>
	/// Clase statica PointExtension: Extiende System.Media.Point.
	/// </summary>
	public static class PointExtension
	{
		public static bool IsInsideOf(this Point pointA, List<Point> pointB)
		{
			bool insideOf = false;

			var pint1 = pointB[0];
			var pint2 = pointB[1];

			insideOf = ((pointA.X > pint1.X) && (pointA.Y > pint1.Y) && (pointA.X < pint2.X) && (pointA.Y < pint2.Y));

			System.Console.WriteLine($"P1 [{pint1.X},{pint1.Y}] | OP [{pointA.X},{pointA.Y}] | P2 [{pint2.X},{pint2.Y}]");

			return insideOf;
		}

		/// <summary>
		/// Suma dos puntos.
		/// </summary>
		/// <param name="pointA">Punto base.</param>
		/// <param name="pointB">Punto a sumar.</param>
		/// <returns>Retorna el punto sumado con el otro.</returns>
		public static Point Add(this Point pointA, Point pointB)
		{
			pointA.X += pointB.X;
			pointA.Y += pointB.Y;
			return pointA;
		}

		/// <summary>
		/// Encuentra el punto segun los parametros dados.
		/// </summary>
		/// <param name="pointA">Punto de referencia.</param>
		/// <param name="withd">Ancho del controlador.</param>
		/// <param name="heigth">Alto del controlador.</param>
		/// <returns>Retorna el punto segun los parametros</returns>
		public static Point FindPoint(this Point pointA, double withd, double heigth)
		{
			var tempPoint = new Point(pointA.X, pointA.Y);
			tempPoint.X += withd;
			tempPoint.Y += heigth;
			return tempPoint;
		}
		
		/// <summary>
		/// Encuentra el punto segun los parametros dados.
		/// </summary>
		/// <param name="pointA">Punto de referencia.</param>
		/// <param name="withd">Ancho del controlador.</param>
		/// <param name="heigth">Alto del controlador.</param>
		/// <returns>Retorna el punto segun los parametros</returns>
		public static Point PointFromSize(this Point pointA, double withd, double heigth)
		{
			var tempPoint = new Point(pointA.X, pointA.Y);
			tempPoint.X -= withd / 2;
			tempPoint.Y -= heigth / 2;
			return tempPoint;
		}

		/// <summary>
		/// Encuentra el punto segun los parametros dados.
		/// </summary>
		/// <param name="pointA">Punto de referencia.</param>
		/// <param name="withd">Ancho del controlador.</param>
		/// <param name="heigth">Alto del controlador.</param>
		/// <returns>Retorna el punto segun los parametros</returns>
		public static Point GetDiference(this Point pointA, Point pointB)
		{
			var tempPoint = new Point(pointA.X, pointA.Y);
			tempPoint.X -= pointB.X;
			tempPoint.Y -= pointB.Y;
			return tempPoint;
		}
	}

	#endregion
}
