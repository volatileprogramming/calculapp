﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlMessage.xaml
	/// </summary>
	public partial class UserControlMessage : UserControl
	{
		/// <summary>
		/// Constructor de UserControlMessage(string title, string message, MyMessageBoxOptions messageBoxOptions = null, List<object> controls = null);
		/// </summary>
		/// <param name="title">Titulo del mensage.</param>
		/// <param name="message">Mensaje a ser mostrado.</param>
		/// <param name="messageBoxOptions">Opciones del cuadro de dialogo.</param>
		/// <param name="controls">Lista de controladores aducionales.</param>
		public UserControlMessage(string title, string message, MyMessageBoxOptions messageBoxOptions = null, List<object> controls = null)
		{
			InitializeComponent();
			TextBlockMessageTitle.Text = title;
			TextBlockMessage.Text = message;
			if (messageBoxOptions != null) SetOptions(messageBoxOptions);
		}

		#region Métodos

		/// <summary>
		/// Evento del UserControl UserControlMessage: Se ejecuta al hace clic en ButtonAcept.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ButtonAcept_Click(object sender, RoutedEventArgs e)
		{

		}

		/// <summary>
		/// Evento del UserControl UserControlMessage: Se ejecuta al hace clic en ButtonCancel.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ButtonCancel_Click(object sender, RoutedEventArgs e)
		{

		}

		/// <summary>
		/// Método del UserControl UserControlMessage: Setea las opciones pasadas por parametro.
		/// </summary>
		/// <param name="options">Opciones a ser aplicadas</param>
		private void SetOptions(MyMessageBoxOptions options)
		{
			//General
			if (options.Foreground != null) Foreground = options.Foreground;
			if (options.FontFamily != null) FontFamily = options.FontFamily;
			if (options.MessageBackground != null) DockPanelMessage.Background = options.MessageBackground;
			if (options.LockerFill != null) RectangleLocker.Fill = options.LockerFill;

			//Titulo
			if (options.TitleFontSize != -1d) TextBlockMessageTitle.FontSize = options.TitleFontSize;
			if (options.TitleFontWeight != null) TextBlockMessageTitle.FontWeight = options.TitleFontWeight;

			//Mensaje
			if (options.MessageFontSize != -1d) TextBlockMessage.FontSize = options.MessageFontSize;
			if (options.TitleFontWeight != null) TextBlockMessage.FontWeight = options.TitleFontWeight;

			//Botones
			if (options.ButtonsMarging != null)
			{
				ButtonAcept.FontWeight = options.ButtonsFontWeight;
				ButtonCancel.FontWeight = options.ButtonsFontWeight;
			}
			if (options.ButtonsFontWeight != null)
			{
				ButtonAcept.Margin = options.ButtonsMarging;
				ButtonCancel.Margin = options.ButtonsMarging;
			}
			//aceptar
			if (options.AceptButtonContent != null) ButtonAcept.Content = options.AceptButtonContent;
			if (options.AceptButtonBackground != null) ButtonAcept.Background = options.AceptButtonBackground;
			if (options.AceptButtonBorderBrush != null) ButtonAcept.BorderBrush = options.AceptButtonBorderBrush;
			if (options.AceptButtonBorderThickness != null) ButtonAcept.BorderThickness = options.AceptButtonBorderThickness;
			//cancelar
			if (options.CancelButtonContent != null) ButtonCancel.Content = options.CancelButtonContent;
			if (options.CancelButtonBackground != null) ButtonCancel.Background = options.CancelButtonBackground;
			if (options.CancelButtonBorderBrush != null) ButtonCancel.BorderBrush = options.CancelButtonBorderBrush;
			if (options.CancelButtonBorderThickness != null) ButtonCancel.BorderThickness = options.CancelButtonBorderThickness;
		}

		#endregion
	}

	/// <summary>
	/// Clase MyMessageBoxOptions: Opciones para el controlador UserControlMessage.
	/// </summary>
	public class MyMessageBoxOptions
	{
		//General
		public Brush Foreground { get; set; }
		public FontFamily FontFamily { get; set; }
		public Brush MessageBackground { get; set; }
		public Brush LockerFill { get; set; }

		//Titulo
		public double TitleFontSize { get; set; } = -1d;
		public FontWeight TitleFontWeight { get; set; }

		//Mensaje
		public double MessageFontSize { get; set; }  = -1d;
		public FontWeight MessageFontWeight { get; set; }

		//Botones
		public FontWeight ButtonsFontWeight { get; set; }
		public Thickness ButtonsMarging { get; set; }
		//aceptar
		public string AceptButtonContent { get; set; }
		public Brush AceptButtonBackground { get; set; }
		public Brush AceptButtonBorderBrush { get; set; }
		public Thickness AceptButtonBorderThickness { get; set; }
		//cancelar
		public string CancelButtonContent { get; set; }
		public Brush CancelButtonBackground { get; set; }
		public Brush CancelButtonBorderBrush { get; set; }
		public Thickness CancelButtonBorderThickness { get; set; }
	}
}
