﻿using System.Windows;
using System.Windows.Controls;
using Calculapp.Data;
using System.Threading.Tasks;
using Calculapp.Modules;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlNumber.xaml
	/// </summary>
	public partial class UserControlNumber : UserControl, IAnswers
	{
		private double _number;

		/// <summary>
        /// Constructor vacío de UserControlNumber().
        /// </summary>
		public UserControlNumber()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase UserControlNumber: Se ejecuta al cargar el control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			await Task.Delay(350);

			dynamic parent = this.Parent as Grid;
			if (parent == null) parent = this.Parent as DockPanel;
			if (parent == null) parent = this.Parent as WrapPanel;

			Position = this.TransformToAncestor(parent).Transform(new Point(0, 0));
		}

		#region SETTERS AND GETTERS

		public bool IsLooked { get; set; } = false;

		/// <summary>
		/// SET & GET: De Number. Almacena el Numero del controlador.
		/// </summary>
		public double Number
		{
			get => _number;
			set
			{
				var hasFractions = (bool)DataHandler.JSON[DataHandler.SelectedGrade]["HasFractions"];

				//setea el valor en entero dependiendo si acepta o no fracciones.
				_number = (hasFractions) ? value : (int)value;
				TextBlockNumber.Text = _number.ToString();
			}
		}

		/// <summary>
		/// SET & GET: De Position. Almacena la posicion por defecto del controlador.
		/// </summary>
		public Point Position { set; get; }

		/// <summary>
		/// SET & GET: De FruitName. Almacena el nombre de la fruta.
		/// </summary>
		public string FruitName { set; get; }

		#endregion
	}
}
