﻿using Calculapp.Modules;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlUserControlCreditsButton.xaml
	/// </summary>
	public partial class UserControlUserControlCreditsButton : UserControl
	{
		public UserControlUserControlCreditsButton()
		{
			InitializeComponent();
		}

		private void UserControl_MouseEnter(object sender, MouseEventArgs e)
		{
			var animation = new AnimationHandler();
			animation.Traslation(this, new Point(0, 0), new Duration(TimeSpan.FromSeconds(0.2)));
		}

		private void UserControl_MouseLeave(object sender, MouseEventArgs e)
		{
			var animation = new AnimationHandler();
			animation.Traslation(this, new Point(0, -50), new Duration(TimeSpan.FromSeconds(0.2)));
		}
	}
}
