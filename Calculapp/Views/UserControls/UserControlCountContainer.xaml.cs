﻿using Calculapp.Data;
using Calculapp.Modules;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlCountContainer.xaml
	/// </summary>
	public partial class UserControlCountContainer : UserControl, IGameplayContainer, INotifyPropertyChanged
	{
		//atributos
		private static UserControlCountContainer _instace = null;
		private bool _isAnimating = true;

		/// <summary>
        /// Constructor vacío de UserControlCountContainer().
        /// </summary>
		public UserControlCountContainer()
		{
			InitializeComponent();
			MainGrid.Children.Remove(BorderFruitTable);
		}

		/// <summary>
		/// Evento de la clase UserControlCountContainer: Se ejecuta al cargar el controlador.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//espera a que las animaciones finalizen
			IsAnimating = true;
			await Task.Delay(1000);

            //cambia el color del fondo cada 5 segundos
            //GetRandomBrush(); //pendiente

            //instancia y añade la ventana de ayuda.
            var help = new UserControlHelp("/Resources/Gifs/count_help.gif");
            Grid.SetRow(help, 1);
            Grid.SetRowSpan(help, 3);
            Grid.SetColumn(help, 1);

            await Task.Delay(350);
            MainGrid.Children.Add(help);

            //setea posicion de la respuesta
            //AnswerPosition = BorderAnswer.TransformToAncestor(MainGrid).Transform(new Point(0, 0));
            IsAnimating = false;
		}

		#region SETTERS AND GETTERS

		public Dictionary<string, UserControlBasket> BasketsList { get; set; } = new Dictionary<string, UserControlBasket>();
		public string[] FruitsNames = new string[] { "aguacate", "piña", "naranja", "uvas" };

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsLocked = !(_isAnimating = value);
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De MouseLastPosition. Almacena la posicion anterior del mouse.
		/// </summary>
		private MousePosition MouseLastPosition { set; get; } = new MousePosition();

		/// <summary>
		/// SET & GET: De AnswerCollisionMasks. Almacena la mascara de coliciones de las respuestas.
		/// </summary>
		public List<Point[]> AnswerCollisionMasks { get; private set; }

		/// <summary>
		/// SET & GET: De Instance. Retorna la inatancia del controlador.
		/// </summary>
		public static UserControlCountContainer Instance
		{
			get
			{
				if (_instace == null) return _instace = new UserControlCountContainer();
				else  return _instace;
			}
			set {  _instace = value; }
		}

		/// <summary>
		/// SET & GET: De Answers. Almacena e instancia la lista de respuestas actuales.
		/// </summary>
		public List<UserControlFruits> Answers { get; set; }

		/// <summary>
		/// SET & GET: De gameplay. Almacena la instancia del gameplay.
		/// </summary>
		public IGameplay Gameplay { set; get; }

		/// <summary>
		/// SET & GET: De SelectecControl. Almacena la instancia del gameplay.
		/// </summary>
		private UserControlFruits SelectecControl { set; get; } //cambiar por UserControlFruit

		/// <summary>
		/// SET & GET: De TextReader. Lee los numeros en voz alta.
		/// </summary>
		public Dictionary<string, List<Point>> CollisionMask { get; set; } = new Dictionary<string, List<Point>>();
		private TextSynthesizer TextReader { get; set; } = new TextSynthesizer();
		public int FruitNumber { get; internal set; }
		public bool Order { get; internal set; }
		private InputHandler Handler { get; set; } = new InputHandler();
		public int Interval { get; internal set; }

		/// <summary>
		/// SET & GET: De MyRandom: Generador de numeros aleatorios.
		/// </summary>
		private readonly Random MyRandom = new Random();

		#endregion

		#region Eventos

		#region Frutas

		/// <summary>
		/// Evento de la clase UserControlCountContainer: Se ejecuta al mover el ratón.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Fruit_MouseMove(object sender, MouseEventArgs e)
		{
			await Task.Delay(DataHandler.FrameRate);
			var control = sender as UserControlFruits;

			if (!IsAnimating && SelectecControl == control && e.LeftButton == MouseButtonState.Pressed)
			{
				DragControl(control, e);
			}
		}

		/// <summary>
		/// Evento de la clase UserControlCountContainer: Se ejecuta al hacer clic en UserControlFruits.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void Fruit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var control = sender as UserControlFruits;

			if(!IsAnimating && SelectecControl == null && !control.IsLooked)
			{
				//bloquea el controlador
				control.IsLooked = true;

				//almacena el control seleccionado
				SelectecControl = control;

				//pone el control por ensima de los demas
				GridControlsContainer.Children.Remove(control);
				GridControlsContainer.Children.Add(control);
			}
		}

		/// <summary>
		/// Evento de la clase UserControlCountContainer: Se ejecuta al sortar clic en UserControlFruits.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void Fruit_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if(!IsAnimating && SelectecControl != null)
			{
				var gameplay = Gameplay as GameplayCount;
				var control = sender as UserControlFruits;
				var fruitName = SelectecControl.FruitName;
				var relativePosition = control.TransformToAncestor(GridControlsContainer).Transform(new Point(0, 0));
				//crea otra instancia
				CreateFuit(control);

				//realiza la cuentas
				if (relativePosition.IsInsideOf(CollisionMask[fruitName]))
				{
					Basket_MouseLeftButtonDown(BasketsList[fruitName], null);
				}

				//envia la respuesta al gameplay
				gameplay.SetAnswer(SelectecControl, CollisionMask[SelectecControl.FruitName]);
				await Task.Delay(400);

				//libera el espacio en memoria
				if (GridControlsContainer.Children.Contains(control)) GridControlsContainer.Children.Remove(control);
				SelectecControl = null;
			}
		}

		#endregion

		#region Canastas

		private void Basket_MouseMove(object sender, MouseEventArgs e)
		{
			
		}

		private void Basket_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (SelectecControl != null)
			{
				var basket = sender as UserControlBasket;
				var fruitName = basket.FruitName;
				if(SelectecControl.FruitName == basket.FruitName)
				{
					var n = basket.Number += SelectecControl.Number;
					//Fruit_MouseLeftButtonUp(SelectecControl, null);
					//lee el texto
					if (n > 1) TextReader.ReadAloudAsync($"{n} {fruitName}s");
					else if (fruitName == "aguacate") TextReader.ReadAloudAsync($"Un {fruitName}");
					else TextReader.ReadAloudAsync($"Una {fruitName}");
				}
			}
		}

		#endregion

		#endregion

		#region Métodos

		#region Creacionales

		private async void CreateFuit(UserControlFruits control)
		{
			await Task.Delay(200);

			var row = Grid.GetRow(control);
			var col = Grid.GetColumn(control);
			var controlN = new UserControlFruits(Interval, FruitsNames[MyRandom.Next(FruitNumber)]);

			//set coll and rows
			Grid.SetColumn(controlN, col);
			Grid.SetRow(controlN, row);

			//set atributos
			controlN.MouseLeftButtonDown += Fruit_MouseLeftButtonDown;
			controlN.MouseMove += Fruit_MouseMove;
			controlN.MouseLeftButtonUp += Fruit_MouseLeftButtonUp;

			//instancia el objeto
			GridControlsContainer.Children.Add(controlN);
		}

		private async void CreateFuit(UserControlBasket control)
		{
			await Task.Delay(200);

			var controlN = new UserControlFruits(Interval, FruitsNames[MyRandom.Next(FruitNumber)]);
			
			//GridControlsContainer.Children.Add(controlN);

			//var position = Mouse.GetPosition(GridControlsContainer);
			//var translateTransform = new TranslateTransform();

			//var X = position.X - 50;
			//var Y = position.Y - 50;

			////Setea la posicion de la del usercontrol
			//control.RenderTransform = translateTransform;
			//translateTransform.X = X;
			//translateTransform.Y = Y;

			//set atributos
			controlN.MouseLeftButtonDown += Fruit_MouseLeftButtonDown;
			controlN.MouseMove += Fruit_MouseMove;
			controlN.MouseLeftButtonUp += Fruit_MouseLeftButtonUp;

			//instancia el objeto
			GridControlsContainer.Children.Add(controlN);
		}

		public void SetControls(int startValue, bool order)
		{
			var range = Answers.Count;

			//limpia los contenedores
			Resect();

			//setea las filas y collumnas
			SetGrid(range);

			//setea fondo para las frutas
			placeFruits(BorderFruitTable, range);
			GridControlsContainer.Children.Add(BorderFruitTable);

			//setea las canastas
			for(var i = 0; i < range; i++)
			{
				//crea la instancia de la canasta
				var basket = new UserControlBasket(
					startValue,
					FruitsNames[i],
					order);

				//setea columnas y filas
				placeTarget(basket, range, i);

				//setea atributos
				basket.PreviewMouseDown += Handler.PreviewMouseDown;
				basket.MouseLeftButtonDown += Basket_MouseLeftButtonDown;
				basket.PreviewMouseMove += Handler.PreviewMouseMove;
				basket.MouseMove += Basket_MouseMove;
				
				//agrega al controlador
				BasketsList.Add(FruitsNames[i], basket);
				GridControlsContainer.Children.Add(basket);
			}

			if (order)
			{
				//instancia los controladores
				for(var i = 0; i < 10; i++)
				{
					var type = (range > 1) ? Answers[MyRandom.Next(0, range)] : Answers[0];
					var control = type.Copy();

					//setea los atributos
					control.PreviewMouseDown += Handler.PreviewMouseDown;
					control.MouseLeftButtonDown += Fruit_MouseLeftButtonDown;
					control.MouseMove += Fruit_MouseMove;
					control.PreviewMouseUp += Handler.PreviewMouseUp;
					control.MouseLeftButtonUp += Fruit_MouseLeftButtonUp;

					//setea columnas y filas
					placeFruits(control, range, i);

					//agrega al controlador
					GridControlsContainer.Children.Add(control);
				}
			}
		}

		private void placeTarget(UIElement target, int r, int i)
		{
			//filas
			if (r > 2)
			{
				if (i < 2) Grid.SetRow(target, 1);
				else Grid.SetRow(target, 2);
			}
			else Grid.SetRow(target, 1);

			//columnas
			if (i == 0 | i == 2) Grid.SetColumn(target, 1);
			else Grid.SetColumn(target, 6);
			if (r != 1) Grid.SetColumnSpan(target, 5);
			else Grid.SetColumnSpan(target, 10);
		}

		private void placeFruits(UIElement target, int r, int i = -1)
		{
			//filas
			if (r > 2) Grid.SetRow(target, 4);
			else Grid.SetRow(target, 3);

			//columnas
			if (i != -1) Grid.SetColumn(target, i+1); //frutas
			else //fondo
			{
				//filas
				Grid.SetRowSpan(target, 2);
				Grid.SetColumn(target, 1);
				Grid.SetColumnSpan(target, 11);
			}
		}

		public void SetGrid(int count)
		{
			var rows = 0;
			var cols = 0;

			//margenes
			GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition { MinHeight=20, MaxHeight=40 });
			GridControlsContainer.ColumnDefinitions.Insert(cols++, new ColumnDefinition {  MinWidth=20, MaxWidth=40 });

			//canastas
			for (var a = 0; a < 2; a++)
			{
				if(count > 2) GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition());
				else
				{
					GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition { Height=new GridLength(230, GridUnitType.Star) });
					break;
				}
			}
			//separador
			GridControlsContainer.RowDefinitions.Insert(rows++, 
				new RowDefinition { MinHeight=20, MaxHeight=40 });
			//frutas
			if(count > 2) GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition());
			else
			{
				GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition { Height=new GridLength(80, GridUnitType.Star) });
			}
			for (var b = 0; b < 10; b++)
				GridControlsContainer.ColumnDefinitions.Insert(cols++, new ColumnDefinition());

			//margenes
			GridControlsContainer.RowDefinitions.Insert(rows++, new RowDefinition { MinHeight=20, MaxHeight=40 });
			GridControlsContainer.ColumnDefinitions.Insert(cols++, new ColumnDefinition {  MinWidth=20, MaxWidth=40 });
		}

		public void Resect()
		{
			//elimina todos los hijos
			SelectecControl = null;
			CollisionMask.Clear();
			BasketsList.Clear();
			GridControlsContainer.Children.Clear();

			//elimina las filas y columnas
			GridControlsContainer.RowDefinitions.Clear();
			GridControlsContainer.ColumnDefinitions.Clear();
		}

		#endregion

		#region Auxiliares

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método de la clase UserControlCountContainer: Realiza el drag de los controles.
		/// </summary>
		/// <param name="control">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void DragControl(UserControlFruits control, MouseEventArgs e)
		{
			//propiedades
			var position = e.GetPosition(MainGrid);

			if (MouseLastPosition.X != position.X || MouseLastPosition.Y != position.Y)
			{
				var translateTransform = new TranslateTransform();

				var X = GetDiferences(position.X, control.Position.X, control.ActualWidth);
				var Y = GetDiferences(position.Y, control.Position.Y, control.ActualHeight);

				//Setea la posicion de la del usercontrol
				control.RenderTransform = translateTransform;
				translateTransform.X = X;
				translateTransform.Y = Y;

				//recuerda la posisicion
				//control.Position = new Point(X, Y);
				MouseLastPosition.Y = position.Y;
				MouseLastPosition.X = position.X;
			}
		}

		/// <summary>
		/// Método de la clase UserControlCountContainer: Optiene la posicion relativa del ratón.
		/// </summary>
		/// <param name="pointA">Indica la posicion actual del objeto.</param>
		/// <param name="pointB">Indica la posicion de referencia.</param>
		/// <param name="size">Indica las dimenciones del control.</param>
		/// <returns>Retorna un double con la posicion relativa del ratón.</returns>
		private double GetDiferences(double pointA, double pointB, double size = 0)
		{
			if (size != 0)
				return pointA - (pointB + (size / 2));
			else
				return pointA - pointB - 10;
		}

		/// <summary>
		/// Método de la clase UserControlCountContainer: Optiene el contenedor padre.
		/// </summary>
		/// <returns>Retorna un Grid con el contenedor padre.</returns>
		public Grid GetParent() => ((Grid)this.Parent);

		#endregion

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}

		#endregion
	}
}
