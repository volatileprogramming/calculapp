﻿using Calculapp.Data;
using Calculapp.Modules;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interación lógica de UserControlGridRowOperation.xaml.
	/// </summary>
	public partial class UserControlGridRowOperation : UserControl, INotifyPropertyChanged
	{
		//properties
		private string _textBoxOperationText;
		private bool _hasWrongOperation = false;

		/// <summary>
		/// Constructor vacío de UserControlGridRowOperation().
		/// </summary>
		public UserControlGridRowOperation()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase UserControlGridRowOperation: Se ejecuta al cargar el UserControl.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//setea propiedades
			var text = TextBoxOperation.Text;

			//setea el focus al TextBoxOperation
			if (string.IsNullOrEmpty(text))
			{
				while (!TextBoxOperation.IsFocused)
				{
					await Task.Delay(100);
					Keyboard.Focus(TextBoxOperation);
				}
			}
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De Index: Indica si el index del mensaje.
		/// </summary>
		public int Index { set; get; }

		/// <summary>
		/// SET & GET: De ParentControl: El UserControl padre.
		/// </summary>
		public UserControlDataGrid ParentControl { get; internal set; }

		/// <summary>
		/// SET & GET: De TextBoxOperationText: Almacena la operacion.
		/// </summary>
		public string TextBoxOperationText
		{
			get => _textBoxOperationText;
			set
			{
				_textBoxOperationText = value;
				TextBoxOperation.PreviewTextInput += TextBoxOperation_PreviewTextInput;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De SelectedSectionName: Indica la sección seleccionada.
		/// </summary>
		public string SelectedSectionName { get; internal set; }

		/// <summary>
		/// SET & GET: De HasEdited: Indica si ha editado o no.
		/// </summary>
		public bool HasEdited { get; private set; } = true;

		/// <summary>
		/// SET & GET: De HasWrongOperation: Indica si tiene una operación incorrecta o no.
		/// </summary>
		public bool HasWrongOperation
		{
			get => _hasWrongOperation;
			set
			{
				_hasWrongOperation = value;
				if (value) ParentControl.EditRow(this);
				else ParentControl.FinishEdition();
			}
		}

		#endregion

		#region Eventos del UserControlGridRowOperation 

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al precionar una tecla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void TextBoxOperation_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.Text))
			{
				//setea las propiedades.
				var regexChar = new Regex(@"[-+*\/]{1}"); //@"[ ]{0,1}[0-9]{1,2}[ ]{0,1}[-+*\/]{1}"
				var regexSpace = new Regex(@"[ ]{1}");
				var text = TextBoxOperation.Text + e.Text;
				var d = e.Text[0];
				var digit = GetDigit(text);
				var isChar = (text.Length > 1) ? regexChar.IsMatch(text[text.Length - 2].ToString()) : regexChar.IsMatch(d.ToString());
				var isDigit = (text.Length > 1) ? char.IsDigit(text[text.Length - 2]) : false;
				var isSpace = (text.Length > 1) ? regexSpace.IsMatch(text[text.Length - 2].ToString()) : regexSpace.IsMatch(d.ToString());
				var repited = (isChar | (isSpace && d == ' '));

				//setea espacios
				//if (isDigit & regexChar.IsMatch(d.ToString()))
				//{
				//	TextBoxOperation.Text += $" {d}";
				//	e.Handled = false;
				//}
				//if (isChar & char.IsDigit(d))
				//{
				//	TextBoxOperation.Text += $" {d}";
				//	e.Handled = false;
				//}

				//verifica los datos
				if (SelectedSectionName == "DockPanelFirst")
				{
					//e.Handled = !regexItem.IsMatch(e.Text);
					//e.Handled = !e.Text.Any(x => regexItem.IsMatch(x.ToString()) || ' '.Equals(x));
					e.Handled = !(digit < 100 && !repited && (char.IsDigit(d) | d == '+' |  d == '-' |  d == ' '));
				}
				else if (SelectedSectionName == "DockPanelSecond")
				{
					e.Handled = !(digit < 1000 && !repited && (char.IsDigit(d) | d == '+' |  d == '-' |  d == '*' |  d == '/' |  d == ' '));
				}
				else if (SelectedSectionName == "DockPanelThird")
				{
					e.Handled = !(digit < 1000000 && !repited && (char.IsDigit(d) | d == '+' |  d == '-' |  d == '*' |  d == '/' |  d == ' '));
				}

				HasEdited = true;
			}
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta antes de perder el foco del control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void TextBoxOperation_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			//setea propiedades
			var operation = TextBoxOperation.Text;
			var IsNullOrEntyOrWhiteSpace = (string.IsNullOrEmpty(operation) | string.IsNullOrWhiteSpace(operation));

			//verifica los datos
			if(HasEdited && !IsNullOrEntyOrWhiteSpace)
			{
				var expression = new EspressionDisposable(operation);
				var myWindow = Application.Current.MainWindow as MainWindow;
				var canCalculate = expression.checkSyntax();
				var resoult = (canCalculate) ? expression.calculate() : 0;

				if (!canCalculate) //comprueba que se pueda calcular
				{
					e.Handled = await myWindow.ShowMessage("Error", $"Esta operación: \"{operation}\" es incorrecta.", MessageDialogStyle.Affirmative);
					Keyboard.Focus(TextBoxOperation);
					HasWrongOperation = !(HasEdited = false);
					return;
				}
				else if (resoult < 0) //comprueba que el resultado sea R >= 0
				{
					e.Handled = await myWindow.ShowMessage("Error", $"Esta operación: \"{operation} = {resoult}\" no puede ser negativa.", MessageDialogStyle.Affirmative);
					Keyboard.Focus(TextBoxOperation);
					HasWrongOperation = !(HasEdited = false);
					return;
				}
				else if (resoult % 1 != 0) //comprueba que el resultado no sea una fracción
				{
					e.Handled = await myWindow.ShowMessage("Error", $"Esta operación: \"{operation} = {resoult}\" no puede ser una fracción.", MessageDialogStyle.Affirmative);
					Keyboard.Focus(TextBoxOperation);
					HasWrongOperation = !(HasEdited = false);
					return;
				}

				HasWrongOperation = false;
				return;
			}

			//setea el enfoque
			if (!HasEdited && !IsNullOrEntyOrWhiteSpace)
			{
				Keyboard.Focus(TextBoxOperation);
				e.Handled = true;
			}
			else if (IsNullOrEntyOrWhiteSpace)
			{
				HasWrongOperation = false;
			}
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al terminar la edición del TextBoxMessage.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void TextBoxOperation_LostFocus(object sender, RoutedEventArgs e)
		{
			if (HasWrongOperation) e.Handled = true;
			else
			{
				var value = TextBoxOperation.Text;
				var operations = ParentControl.ContentList.ToObject<List<MixedOperation>>();

				PlaySound("out.ogg");

				//Actualiza el dataHandler y el config con los datos del TexBox.
				if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
				{
					if (Index < operations.Count) operations.RemoveAt(Index);
					if ((StackPanel)this.Parent != null) ((StackPanel)this.Parent).Children.Remove(this);
				}
				else
				{
					if (Index < operations.Count) operations[Index].Operation = value;
					else operations.Add(new MixedOperation { Operation = value, Active = true });
				}

				ParentControl.ContentList = JArray.FromObject(operations);
				if (!ParentControl.HasPlussButton) ParentControl.HasPlussButton = true;
			}
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al hacer clic en ImageBasket.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void ImageBasket_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (HasWrongOperation) e.Handled = true;
			else
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				var operations = ParentControl.ContentList.ToObject<List<MixedOperation>>();

				PlaySound("in.ogg");

				bool resoult = await myWindow.ShowMessage("Pregunta", "¿Desea eliminar esta operación?", MessageDialogStyle.AffirmativeAndNegative);

				//Remueve el controlador del padre y el mensaje de DataHandler y el config.
				if (resoult)
				{
					if (!(Index >= operations.Count))
					{
						operations.RemoveAt(Index);
						ParentControl.ContentList = JArray.FromObject(operations);
					}
					if ((StackPanel)this.Parent != null) ((StackPanel)this.Parent).Children.Remove(this);
				}
			}
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al hacer clic en CheckBoxPlayList.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void CheckBoxPlayList_Click(object sender, RoutedEventArgs e)
		{
			if (HasWrongOperation) e.Handled = true;
			else
			{
				var value = TextBoxOperation.Text;
				var operations = ParentControl.ContentList.ToObject<List<MixedOperation>>();
				var count = operations.Count;

				if (!(string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)) && Index < count) ParentControl.ContentList[Index]["Active"] = CheckBoxPlayList.IsChecked;
			}
		}

		#endregion

		#region Eventos del Ratón

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al entrar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageBasket_MouseEnter(object sender, MouseEventArgs e)
		{
			PlaySound("mouse_hover.mp3");

			var image = sender as Image;
			var svgReader = new SVGReader();

			if (image.Name == ImageBasket.Name) image.Source = svgReader.Read(@"Icons\Garbage-Closed.svg");
			image.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };
		}

		/// <summary>
		/// Evento del UserControl UserControlGridRowOperation: Se ejecuta al sacar el mouse.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void ImageBasket_MouseLeave(object sender, MouseEventArgs e)
		{
			//PlaySound("mouse_leave.mp3");

			var image = sender as Image;
			var svgReader = new SVGReader();

			if (image.Name == ImageBasket.Name) image.Source = svgReader.Read(@"Icons\Garbage-Closed-01.svg");
			image.OpacityMask = new SolidColorBrush { Opacity = 0.65, Color = Colors.Black };
		}

		#endregion

		#region Métodos

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		/// <summary>
		/// Método GetDigit. Optiene la ultima sifra de un string.
		/// </summary>
		/// <param name="text">Texto a extraer el digito.</param>
		/// <returns></returns>
		private int GetDigit(string text)
		{
			//setea atributos
			int n;
			var imputs = text.Split();

			return (int.TryParse(imputs[imputs.Length -1], out n)) ? n : 0;
		}

		#endregion
		
		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase Options: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion

		
	}
}
