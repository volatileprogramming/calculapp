﻿using Calculapp.Data;
using Calculapp.Modules;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlBasket.xaml
	/// </summary>
	public partial class UserControlBasket : UserControl
	{
		private double _number;
		private string _fruitName;

		public UserControlBasket(int number, string fruitName, bool order)
		{
			InitializeComponent();
			Number = number;
			FruitName = fruitName;
			Order = order;

            var svgReader = new SVGReader();
            ImageBasket.Source = svgReader.Read(@"Images\basket.svg");
        }

		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{

			FontSize = DataHandler.GlobalFontSize * 2;

			dynamic parent = this.Parent as Grid;
			if (parent == null) parent = this.Parent as DockPanel;
			if (parent == null) parent = this.Parent as WrapPanel;

			await Task.Delay(350);

			//setea los datos
			var intance = UserControlCountContainer.Instance;

			//carga los puntos
			Point pointA = this.TransformToAncestor(parent).Transform(new Point(0, 0));
			var pointB = pointA.FindPoint(ActualWidth, ActualHeight);

			//setea la posicion de la respuesta
			intance.CollisionMask.Add(FruitName, new List<Point> { pointA, pointB });
		}

		#region SETTERS AND GETTERS


		public bool IsLooked { get; set; } = false;

		/// <summary>
		/// SET & GET: De Number. Almacena el Numero del controlador.
		/// </summary>
		public double Number
		{
			get => _number;
			set
			{
				var hasFractions = (bool)DataHandler.JSON[DataHandler.SelectedGrade]["HasFractions"];

				//setea el valor en entero dependiendo si acepta o no fracciones.
				_number = (hasFractions) ? value : (int)value;
				TextBlockNumber.Text = _number.ToString();
			}
		}

		/// <summary>
		/// SET & GET: De FruitName. Almacena el nombre de la fruta.
		/// </summary>
		public string FruitName
		{
			get => _fruitName;
			set
			{
				_fruitName = value;

				if (_fruitName == "aguacate")
				{
					MainGrid.Background = Brushes.Green;
				}
				else if (_fruitName == "piña")
				{
					MainGrid.Background = Brushes.Gold;
				}
				else if (_fruitName == "naranja")
				{
					MainGrid.Background = Brushes.Orange;
				}
				else
				{
					MainGrid.Background = Brushes.DarkViolet;
				}
			}
		}
		public bool Order { get; }

		/// <summary>
		/// SET & GET: De Position. Almacena la posicion por defecto del controlador.
		/// </summary>
		public Point Position { set; get; }
		public Dictionary<string, List<Point>> CollisionMask { get; private set; }

		#endregion
	}
}
