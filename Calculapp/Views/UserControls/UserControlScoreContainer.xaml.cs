﻿using Calculapp.Data;
using Calculapp.Views.Screens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlScoreContainer.xaml
	/// </summary>
	public partial class UserControlScoreContainer : UserControl, INotifyPropertyChanged
	{
		private List<ScoreData> _scores;

		/// <summary>
		/// Constructor vacío de UserControlScoreContainer().
		/// </summary>
		public UserControlScoreContainer(ScoreData data)
		{
			InitializeComponent();
			Data = data;
			Scores = ReadCollectionData();
			FontSize = DataHandler.GlobalFontSize;

			//verifica la puntuación
			if (IsNewScroe()) ShowMessage(true);
		}

		/// <summary>
		/// Evento de la clase UserControlOrderingContainer: Se ejecuta al cargar el controlador.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			//..
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De Data. Indica la puntuación acumulada.
		/// </summary>
		public ScoreData Data { get; set; }

		/// <summary>
		/// SET & GET: De Data. Indica la puntuación acumulada.
		/// </summary>
		private List<ScoreData> Scores
		{
			get => _scores;
			set
			{
				_scores = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De Index. Indica el indice a ser modificado.
		/// </summary>
		private int Index { get; set; } = 0;

		#endregion

		#region Eventos



		#endregion

		#region Métodos

		#region Creacionales

		/// <summary>
		/// Método de la clase UserControlScoreContainer. Verifica si es una nueva puntuación o no.
		/// </summary>
		/// <returns>Retorna true o false dependiendo si es una nueva puntuación o no.</returns>
		private bool IsNewScroe()
		{
			var count = 0;
			if (Scores.Count > 0) //verifica las puntuaciones
			{
				foreach (var data in Scores)
				{
					if (Data.Puntuación > data.Puntuación)
					{
						Index = count;
						return true;
					}
					count++;
				}
				return false;
			}
			else //si no existen crea una lista nueva
			{
				for (var i = 1; i <= 10; i++)
				{
					Scores.Add(new ScoreData { Posición = i });
				}
				WriteCollectionData(); //Guarda los datos
				return true;
			}
		}

		/// <summary>
		/// Método de la clase UserControlScoreContainer. Carga la lista de puntuaciones.
		/// </summary>
		/// <returns>Retorna la lista de puntuaciones.</returns>
		private List<ScoreData> ReadCollectionData()
		{
			//setea los atriburos
			var dataReader = new JSONHandler();
			var grade = DataHandler.SelectedGrade;
			var module = DataHandler.SelectedModule;
			var level = DataHandler.SelectedLevel;
			var json = dataReader.ReadBin("Scores.bin");
			var dataModule = json[grade][module];

			//verifica si tiene datos
			if (level != null)
			{
				var listScrores = (JArray)json[grade][module];
				var myDictionary = listScrores.GetValue(level.Number - 1, new Dictionary<string, List<ScoreData>>());
				return (myDictionary.Count > 0) ? myDictionary[level.Number.ToString()] : new List<ScoreData>();
			}
			else
			{
				return json[grade].GetValue(module, new List<ScoreData>());
			}
		}

		/// <summary>
		/// Método de la clase UserControlScoreContainer. Escribe la lista de puntuaciones.
		/// </summary>
		private void WriteCollectionData()
		{
			//setea los atriburos
			var dataReader = new JSONHandler();
			var grade = DataHandler.SelectedGrade;
			var module = DataHandler.SelectedModule;
			var level = DataHandler.SelectedLevel;
			var json = dataReader.ReadBin("Scores.bin");
			var dataModule = json[grade][module];

			//escribe el JSON
			if (level != null)
			{
				var listScrores = (JArray)json[grade][module];
				var myDictionary = new Dictionary<string, List<ScoreData>>();

				myDictionary.Add(level.Number.ToString(), Scores);

				if (level.Number <= listScrores.Count)
					listScrores[level.Number - 1] = JToken.FromObject(myDictionary);
				else listScrores.Add(JToken.FromObject(myDictionary));

				json[grade][module] = listScrores;
			}
			else json[grade][module] = (JArray)JToken.FromObject(Scores);

			//escribe el JSON
			dataReader.WriteBin("Scores.bin");
		}

		/// <summary>
		/// Método de la clase UserControlScoreContainer. Muestra u oculta el mensaje.
		/// </summary>
		/// <param name="value">Muestra u oculta el mensaje.</param>
		private async void ShowMessage(bool value)
		{
			//muestra y oculta los controladores
			if (value)
			{
				BorderContainer.Visibility = Visibility.Collapsed;
				BorderMessage.Visibility = Visibility.Visible;
				while (!TextBoxName.IsFocused) //enfoca el texbox
				{
					await Task.Delay(100);
					Keyboard.Focus(TextBoxName);
				}
			}
			else
			{
				BorderContainer.Visibility = Visibility.Visible;
				BorderMessage.Visibility = Visibility.Collapsed;
			}
		}

		#endregion

		#region Imagenes

		private void Acept_Click(object sender, RoutedEventArgs e)
		{
			//almacena los datos
			var text = TextBoxName.Text;
			var isNullOrEmtyOrWhiteSpace = (string.IsNullOrEmpty(text) | string.IsNullOrWhiteSpace(text));

			Data.Posición = Index+1;
			Data.Nombre = (!isNullOrEmtyOrWhiteSpace) ? TextBoxName.Text : "AAA";

			Scores[Index] = Data;

			//refresca la tabla
			DataGridScores.ItemsSource = Scores;

			//guarda los datos
			WriteCollectionData();

			//Oculta el mensaje
			ShowMessage(false);
		}

		private void GoBack_Click(object sender, RoutedEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;

			if (DataHandler.SelectedLevel != null)
			{
				DataHandler.SelectedLevel = null;
				myWindow.FrameScreenNavigator.Content = new MappScreen();
			}
			else myWindow.FrameScreenNavigator.Content = new ModuleSelectionScreen();

			//elimina el controlador
			((Grid)this.Parent).Children.Remove(this);
		}

		private void Repit_Click(object sender, RoutedEventArgs e)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			myWindow.FrameScreenNavigator.Content = new GamePlayScreen();

			//elimina el controlador
			((Grid)this.Parent).Children.Remove(this);
		}

		private void TextBoxName_LostFocus(object sender, RoutedEventArgs e)
		{
			Acept_Click(null, null);
		}

		#endregion
		
		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase ModuleSelectionScreen: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}

		#endregion
	}

	#region Extenciones

	/// <summary>
	/// Clase statica JTokenExtensions: Extiende Newtonsoft.Json.Linq.JToken.
	/// </summary>
	public static class JTokenExtensions
	{
		/// <summary>
		/// Método de la clase JTokenExtensions: Obtiene el valor expesificado si existe.
		/// </summary>
		/// <typeparam name="T">Tipo de los datos a ser convertidos.</typeparam>
		/// <param name="jToken">JToken a ser convertido.</param>
		/// <param name="key">Nombre del atributo.</param>
		/// <param name="defaultValue">Valor por defeto del objeto.</param>
		/// <returns>Retorna el valor expesificado si existe.</returns>
		public static T GetValue<T>(this JToken jToken, string key, T defaultValue = default(T))
		{
			dynamic ret = jToken[key];
			if (ret == null) return defaultValue;
			if (ret is JToken) return JsonConvert.DeserializeObject<T>(ret.ToString());
			return ret.ToObject<T>();
		}

		/// <summary>
		/// Método de la clase JTokenExtensions: Obtiene el valor expesificado si existe.
		/// </summary>
		/// <typeparam name="T">Tipo de los datos a ser convertidos.</typeparam>
		/// <param name="jToken">JToken a ser convertido.</param>
		/// <param name="index">Indice del atributo.</param>
		/// <param name="defaultValue">Valor por defeto del objeto.</param>
		/// <returns>Retorna el valor expesificado si existe.</returns>
		public static T GetValue<T>(this JArray jToken, int index, T defaultValue = default(T))
		{
			dynamic ret = (jToken.Count > index) ? jToken[index] : null;
			if (ret == null) return defaultValue;
			if (ret is JArray) return JsonConvert.DeserializeObject<T>(ret.ToString());
			return ret.ToObject<T>();
		}
	}

	#endregion
}
