﻿using System.Windows.Controls;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlLevelIndicator.xaml
	/// </summary>
	public partial class UserControlLevelIndicator : UserControl
	{
		public UserControlLevelIndicator()
		{
			InitializeComponent();
		}
	}
}
