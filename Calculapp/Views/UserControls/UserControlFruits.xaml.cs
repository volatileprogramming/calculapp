﻿using Calculapp.Data;
using Calculapp.Modules;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Calculapp.Views.UserControls
{
	/// <summary>
	/// Interaction logic for UserControlFruits.xaml
	/// </summary>
	public partial class UserControlFruits : UserControl, IAnswers
	{
		private double _number;
		private string _fruitName;

		/// <summary>
        /// Constructor vacío de UserControlFruits().
        /// </summary>
		public UserControlFruits(double number, string fruitName)
		{
			InitializeComponent();
			Number = number;
			FruitName = fruitName;

            var svgReader = new SVGReader();
            ImageFruit.Source = svgReader.Read(@"Images\Fruits\"+fruitName+".svg");

            var translateTransform = new TranslateTransform();
			this.RenderTransform = translateTransform;

			if (Number > 1) TextBlockNumber.Visibility = Visibility.Visible;
		}

		/// <summary>
		/// Evento de la clase UserControl_Loaded: Se ejecuta al cargar el control.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void UserControl_Loaded(object sender, RoutedEventArgs e)
		{
			IsLooked = true;
			FontSize = DataHandler.GlobalFontSize;
			await Task.Delay(650);

			dynamic parent = this.Parent as Grid;
			if (parent == null) parent = this.Parent as DockPanel;
			if (parent == null) parent = this.Parent as WrapPanel;

			Position = this.TransformToAncestor(parent).Transform(new Point(0, 0));
			IsLooked = false;
		}

		#region SETTERS AND GETTERS

		public bool IsLooked { get; set; } = false;

		/// <summary>
		/// SET & GET: De Number. Almacena el Numero del controlador.
		/// </summary>
		public double Number
		{
			get => _number;
			set
			{
				var hasFractions = (bool)DataHandler.JSON[DataHandler.SelectedGrade]["HasFractions"];

				//setea el valor en entero dependiendo si acepta o no fracciones.
				_number = (hasFractions) ? value : (int)value;
				TextBlockNumber.Text = _number.ToString();
			}
		}

		/// <summary>
		/// SET & GET: De FruitName. Almacena el nombre de la fruta.
		/// </summary>
		public string FruitName
		{
			get => _fruitName;
			set
			{
				_fruitName = value;

				if (_fruitName == "aguacate")
				{
					MainGrid.Background = Brushes.Green;
				}
				else if (_fruitName == "piña")
				{
					MainGrid.Background = Brushes.Gold;
				}
				else if (_fruitName == "naranja")
				{
					MainGrid.Background = Brushes.Orange;
				}
				else
				{
					MainGrid.Background = Brushes.DarkViolet;
				}
			}
		}

		/// <summary>
		/// SET & GET: De Position. Almacena la posicion por defecto del controlador.
		/// </summary>
		public Point Position { get; set; } = new Point();

		#endregion
		
	}

	public static class UserControlFruitsExtension
	{
		public static UserControlFruits Copy(this UserControlFruits type)
		{
			return new UserControlFruits(type.Number, type.FruitName);
		}
	}
}
