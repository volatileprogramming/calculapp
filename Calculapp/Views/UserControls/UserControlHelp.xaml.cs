﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace Calculapp.Views.UserControls
{
    /// <summary>
    /// Interaction logic for UserControlHelp.xaml
    /// </summary>
    public partial class UserControlHelp : UserControl
    {
        /// <summary>
        /// 
        /// </summary>
        public UserControlHelp()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        public UserControlHelp(string fileName)
        {
            InitializeComponent();

            FileName = fileName;

            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri(fileName, UriKind.Relative);
            image.EndInit();
            ImageBehavior.SetAnimatedSource(img, image);
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        private void MainGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
			var parent = this.Parent as Grid;
			if (parent.Children.Contains(this)) parent.Children.Remove(this);
        }
    }
}
