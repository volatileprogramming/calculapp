﻿using Calculapp.Data;
using Calculapp.Modules;
using Calculapp.Views.Screens;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;

namespace Calculapp.Views
{
	/// <summary>
	/// Interación lógica de MainWindow.xaml
	/// </summary>
	public partial class MainWindow : MetroWindow, INotifyPropertyChanged
	{
		#region Propiedades

		private static double _fontSize = 14;
		private bool _closeMe;
		private bool _isAnimating = false;
		private Point _mousePosition;
		private bool _isLocked = true;

		#endregion

		/// <summary>
		/// Constructor vacío de MainWindow().
		/// </summary>
		public MainWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Evento de la clase MainWindow: Se ejecuta al cargar la ventana.
		/// </summary>
		/// <param name="sender">Objeto que disparo el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
		{
			FrameScreenNavigator.Content = new LoadingScreen();
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De InverseAnimationFlag. Indica si la aminación es invertida o no.
		/// </summary>
		public bool InverseAnimationFlag { set; get; } = false;

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public double GlobalFontSize
		{
			get => _fontSize;
			set
			{
				FontSize = _fontSize = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De Music. Reproductor de musica.
		/// </summary>
		public MediaPlayer Music { set; get; } = new MediaPlayer();

		/// <summary>
		/// SET & GET: De MouseLastPoint. Almacena el ultimo punto del ratón.
		/// </summary>
		public Point MousePosition
		{
			get => _mousePosition;
			set
			{
				_mousePosition = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				_isAnimating = !value;
				OnPropertyChanged();
				var contains = MainGrid.Children.Contains(RectangleLocker);

				if (_isAnimating && !contains) MainGrid.Children.Add(RectangleLocker);
				else if (contains) MainGrid.Children.Remove(RectangleLocker);
			}
		}

		/// <summary>
		/// Método de la clase MainWindow: Bloquea o desbloquea el contenedor el contenedor.
		/// </summary>
		public bool IsLocked
		{
			get => _isLocked;
			set
			{
				_isLocked = value;
				OnPropertyChanged();
			}
		}

		public LoadingScreen LoadingScreen
		{
			get => default(LoadingScreen);
			set
			{
			}
		}

		public MainMenuSreen MainMenuSreen
		{
			get => default(MainMenuSreen);
			set
			{
			}
		}

		public ModuleSelectionScreen ModuleSelectionScreen
		{
			get => default(ModuleSelectionScreen);
			set
			{
			}
		}

		public GamePlayScreen GamePlayScreen
		{
			get => default(GamePlayScreen);
			set
			{
			}
		}

		public MappScreen MappScreen
		{
			get => default(MappScreen);
			set
			{
			}
		}

		public OptionsScreen OptionsScreen
		{
			get => default(OptionsScreen);
			set
			{
			}
		}

		public UserControls.MyMessageBoxOptions MyMessageBoxOptions
		{
			get => default(UserControls.MyMessageBoxOptions);
			set
			{
			}
		}

		public SVGReader SVGReader
		{
			get => default(SVGReader);
			set
			{
			}
		}

		public UserControls.UserControlMessage UserControlMessage
		{
			get => default(UserControls.UserControlMessage);
			set
			{
			}
		}

		public SoundPlayer SoundPlayer
		{
			get => default(SoundPlayer);
			set
			{
			}
		}

		#endregion

		#region Eventos del MainWindow

		/// <summary>
		/// Evento de la clase MainWindow: Se ejecuta al cambiar de página.
		/// </summary>
		/// <param name="sender">Objeto que disparo el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void FrameScreenNavigator_Navigating(object sender, NavigatingCancelEventArgs e)
		{
			var frame = sender as Frame;
			if (frame.Content != null)
			{
				var ta = new ThicknessAnimation
				{
					Duration = TimeSpan.FromSeconds(0.3),
					DecelerationRatio = 0.7,
					To = new Thickness(0, 0, 0, 0)
				};

				if (!InverseAnimationFlag)//(e.NavigationMode == NavigationMode.New)
				{
					PlaySound("go-back.wav");
					ta.From = new Thickness(500, 0, 0, 0);
				}
				else //if (e.NavigationMode == NavigationMode.Back)
				{
					PlaySound("go-back.wav");
					ta.From = new Thickness(0, 0, 500, 0);
					InverseAnimationFlag = false;
				}

				(e.Content as Page).BeginAnimation(MarginProperty, ta);
			}
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al interntar cerrar la vantana.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void MainWindow_Closing(object sender, CancelEventArgs e)
		{
			if (e.Cancel) return;

			// we want manage the closing itself!
			e.Cancel = !this._closeMe;
			// yes we want now really close the window
			if (this._closeMe) return;

			var mySettings = new MetroDialogSettings()
			{
				AffirmativeButtonText = "Salir",
				NegativeButtonText = "Cancelar",
				DialogMessageFontSize = GlobalFontSize,
				DialogTitleFontSize = GlobalFontSize * 2,
				AnimateShow = true,
				AnimateHide = false
			};

			var result = await this.ShowMessageAsync("Pregunta", "¿Desea cerrar la aplicación?", MessageDialogStyle.AffirmativeAndNegative, mySettings);

			this._closeMe = result == MessageDialogResult.Affirmative;

			if (this._closeMe) Application.Current.Shutdown();
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al precionar una tecla.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void MainWindow_KeyDown(object sender, KeyEventArgs e)
		{
			await Task.Delay(100);
			if (e.Key == Key.D && Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftShift))
			{
				FlyoutDebuger.IsOpen = true;
			}
		}

		#endregion

		#region Métodos del MainWindow

		/// <summary>
		/// Método de la clase MainWindow: Muestra un cuadro de dialogo de confirmación.
		/// </summary>
		/// <param name="title">Titulo del cuadro de dialogo.</param>
		/// <param name="message">Mensaje a mostrar.</param>
		/// <param name="dialogStyle">Tipo de respuestas del cuadro de dialogo</param>
		/// <returns>Retorna true o false dependiendo de la respuesta.</returns>
		public async Task<bool> ShowMessage(string title, string message, MessageDialogStyle dialogStyle = MessageDialogStyle.AffirmativeAndNegative)
		{
			var mySettings = new MetroDialogSettings()
			{
				AffirmativeButtonText = "Si",
				NegativeButtonText = "No",
				DialogMessageFontSize = GlobalFontSize,
				DialogTitleFontSize = GlobalFontSize * 2,
				AnimateShow = true,
				AnimateHide = false
			};

			var result = await this.ShowMessageAsync(title, message, dialogStyle, mySettings);
			if (result == MessageDialogResult.Affirmative) return true;
			else return false;
		}

		/// <summary>
		/// Método de la clase MainWindow: Optiene la página actual.
		/// </summary>
		/// <returns>Retorna un object con el contenido del FrameScreenNavigator.</returns>
		public object GetCurrentPage()
		{
			return FrameScreenNavigator.Content;
		}

		/// <summary>
		/// Método de la clase MainWindow: Cambia el cursor actual.
		/// </summary>
		public void ChangeCursor(Cursor cursor)
		{
			Mouse.OverrideCursor = cursor;
		}

		#endregion

		#region Eventos del Ratón

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al poner el ratón encima del MainWindow.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void MetroWindow_MouseEnter(object sender, MouseEventArgs e)
		{
			Mouse.OverrideCursor = DataHandler.CurrentCursor;
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al sacar el ratón de encima del MainWindow.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private void MetroWindow_MouseLeave(object sender, MouseEventArgs e)
		{
			Mouse.OverrideCursor = Cursors.Arrow;
		}

		/// <summary>
		/// Evento de la clase MainMenuSreen: Se ejecuta al mover el ratón.
		/// </summary>
		/// <param name="sender">Objeto que dispara el evento.</param>
		/// <param name="e">Manejador del evento.</param>
		private async void MainWindow_MouseMove(object sender, MouseEventArgs e)
		{
			await Task.Delay(DataHandler.FrameRate);
			var position = e.GetPosition(this);
			var translateTransform = new TranslateTransform();

			//setea el texto de los labels
			LavelMousePosition.Content = $"[{position.X},{position.Y}]";

			//setea la ultima posicion
			MousePosition = position;
		}

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Propiedad de evento cambiado.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Evento de la clase MainWindow: Provoca el evento PropertyChanged de este objeto.
		/// </summary>
		/// <param name="propertyName">La propiedad que tiene un nuevo valor.</param>
		protected void OnPropertyChanged([CallerMemberName]string propertyName = null)
		{
			PropertyChangedEventHandler handler = this.PropertyChanged;
			if (handler != null)
			{
				var e = new PropertyChangedEventArgs(propertyName);
				handler(this, e);
			}
		}


		#endregion
	}
}
