﻿namespace Calculapp.Data
{
	/// <summary>
	/// Clase ScoreData: Modelo de datos para las puntuaciones.
	/// </summary>
	public class ScoreData
	{
		/// <summary>
		/// Constructor vacío de ScoreData().
		/// </summary>
		public ScoreData() { }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De Posición. Indica la posición de la puntuación.
		/// </summary>
		public int Posición { get; set; }

		/// <summary>
		/// SET: y GET: De Nombre. Almecena el nobre del estudiante.
		/// </summary>
		public string Nombre { get; set; } = "AAA";

		/// <summary>
		/// SET: y GET: De Tiempo. Almecena el tiempo del estudiante.
		/// </summary>
		public string Tiempo { get; set; } = "00:00.00";

		/// <summary>
		/// SET: y GET: De Puntuación. Almecena la puntuación del estudiante.
		/// </summary>
		public int Puntuación { get; set; } = 0;

		#endregion
	}
}
