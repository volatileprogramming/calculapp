﻿namespace Calculapp.Data
{
	/// <summary>
	/// Clase Level. Modelo de datos para los niveles.
	/// </summary>
	public class Level
	{
		/// <summary>
		/// Constructor vacío de Level().
		/// </summary>
		public Level() { }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De Name. Almecena el nobre del controlador.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// SET: y GET: De Min. Almecena el numero del nivel.
		/// </summary>
		public int Number { set; get; }

		/// <summary>
		/// SET: y GET: De Min. Almecena el valor minimo de la operación.
		/// </summary>
		public double Min { set; get; }

		/// <summary>
		/// SET: y GET: De Max. Almecena el valor maximo de la operación.
		/// </summary>
		public double Max { set; get; }

		#endregion

    }
}
