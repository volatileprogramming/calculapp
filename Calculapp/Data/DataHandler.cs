﻿using Calculapp.Modules;
using Calculapp.Views;
using Calculapp.Views.Screens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase DataHandler: Maneja los datos globales.
	/// </summary>
	public static class DataHandler
	{
		#region Propiedades

		private static bool _soundMute;
        private static bool _musicMute;
        private static double _musicVolume;
        private static double _soundVolume;
		private static bool _isAnimating;
		private static string _selectedGrade;
		private static string _selectedModule;
		private static double _globalFontSize;
        private static bool _fullscreen;
		private static Level _selectedLevel;
		private static Point _mouseRelativePosition;
		private static Cursor _currentCursor;
		private static StringCollection _messages = new StringCollection();
		private static bool _voiceMute;
		private static double _voiceVolume;

		#endregion

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De CurrentCursor. Indica el cursor actual.
		/// </summary>
		public static Cursor CurrentCursor
		{
			get => _currentCursor;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.ChangeCursor(_currentCursor = value);
			}
		}

		/// <summary>
		/// SET & GET: De FrameRate. Indica la velocidad de refresco de la aplicacion.
		/// </summary>
		public static TimeSpan FrameRate { get; set; } = TimeSpan.FromMilliseconds(17);

		/// <summary>
		/// SET & GET: De FullScreen. Indica el modo de pantalla.
		/// </summary>
		public static bool FullScreen
		{
            get => _fullscreen;
            set
            {
                ConfingHandler.Settings.FullScreen = _fullscreen = value;
                ConfingHandler.Save();
            }
        }

		/// <summary>
		/// SET & GET: De SoundMute. Indica si el sonido esta en mute.
		/// </summary>
        public static bool SoundMute
		{
            get =>  _soundMute;
            set
            {
                ConfingHandler.Settings.SoundMute = _soundMute = value;
                ConfingHandler.Save();
            }
        }

		/// <summary>
		/// SET & GET: De MusicMute. Indica si la musica esta en mute.
		/// </summary>
        public static bool MusicMute
        {
            get => _musicMute;
            set
            {
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.Music.IsMuted = ConfingHandler.Settings.MusicMute = _musicMute = value;
                ConfingHandler.Save();
            }
        }

		/// <summary>
		/// SET & GET: De MousePosition. Indica la posicion del ratón segun el control seleccionado.
		/// </summary>
		public static Point MouseRelativePosition
		{
			get => _mouseRelativePosition;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				_mouseRelativePosition = value;
			}
		}

		/// <summary>
		/// SET & GET: De MusicVolume. Indica el vloumen de la musica.
		/// </summary>
		public static double MusicVolume
        {
            get => _musicVolume;
            set
            {
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.Music.Volume = ConfingHandler.Settings.MusicVolume = _musicVolume = value;
                ConfingHandler.Save();
            }
        }

		/// <summary>
		/// SET & GET: De SoundVolume. Indica el volumen del sonido.
		/// </summary>
        public static double SoundVolume
        {
            get => _soundVolume;
            set
            {
                ConfingHandler.Settings.SoundVolume = _soundVolume = value;
                ConfingHandler.Save();
            }
        }

		public static double VoiceVolume
		{
			get => _voiceVolume;
			set
			{
				ConfingHandler.Settings.VoiceVolume = _voiceVolume = value;
                ConfingHandler.Save();
			}
		}
		public static bool VoiceMute
		{
			get => _voiceMute;
			set
			{
				ConfingHandler.Settings.VoiceMute = _voiceMute = value;
                ConfingHandler.Save();
			}
		}

		/// <summary>
		/// SET & GET: De SelectedGrade. Indica el grado seleccionado.
		/// </summary>
        public static string SelectedGrade { set; get; }

		/// <summary>
		/// SET & GET: De SelectedModule. Indica el módulo seleccionado.
		/// </summary>
        public static string SelectedModule { set; get; }

		/// <summary>
		/// SET & GET: De JSON. Almacena los datos de un json en tipo object.
		/// </summary>
		public static JObject JSON { set; get; }

		/// <summary>
		/// SET & GET: De GlobalFontSize. Indica el tamaño de las fuentes.
		/// </summary>
		public static double GlobalFontSize
		{
			get
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				return myWindow.GlobalFontSize;
			}
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.GlobalFontSize = value;
			}
		}

		/// <summary>
		/// SET & GET: De SelectedLevel. Indica el nivel seleccionado.
		/// </summary>
		public static Level SelectedLevel { get; internal set; } = null;

		/// <summary>
		/// SET & GET: De IsAnimating. Indica si se esta animando o no.
		/// </summary>
		public static bool IsAnimating
		{
			get => _isAnimating;
			set
			{
				var myWindow = Application.Current.MainWindow as MainWindow;
				myWindow.IsAnimating = _isAnimating = !value;
			}
		}

		public static JSONHandler JSONHandler
		{
			get => default(JSONHandler);
			set
			{
			}
		}

		

		#endregion

		#region Metodos de la clase

		/// <summary>
		/// Método de la clase DataHandler: Carga los datos y crea e inisiliza las variable globales.
		/// </summary>
		/// <param name="main">Instancia de la ventana principal.</param>
		public static void Load(object main)
		{
			//seteo de variables
			var myWindow = main as MainWindow;
			var loadingScreen = myWindow.GetCurrentPage() as LoadingScreen;

			//Aplicar configuración
			ConfingHandler.Apply(myWindow);
			loadingScreen.SetProgress(0.7d);

			//Carga de JSON
			var json = new JSONHandler();
			JSON = json.ReadBin("Config.bin");

			//Animación del texto
			AnimateMessage(loadingScreen);

			//Inisialización de variable globales
			SoundMute = ConfingHandler.Settings.SoundMute;
			MusicMute = ConfingHandler.Settings.MusicMute;
			MusicVolume = ConfingHandler.Settings.MusicVolume;
			SoundVolume = ConfingHandler.Settings.SoundVolume;
			FullScreen = ConfingHandler.Settings.FullScreen;

			//Carga de archivos
			MusicPlayer.Open("Cancion_Juego.mp3");
        }

        /// <summary>
        /// Método de la clase DataHandler: Cambia los mensajes motivacionales.
        /// </summary>
        private static async void AnimateMessage(LoadingScreen loadingScreen)
		{
			var messages = JSON["Messages"].ToObject<List<string>>();
			int index = 0;
			while (loadingScreen.IsLoading)
			{
				index = GetNextRandom(index, messages.Count - 1);
				await Task.Delay(2000);
				loadingScreen.MetroHeaderPhrases.Content = messages[index];
            }
        }

		/// <summary>
        /// Método de la clase DataHandler: Obtiene un entero aleatorio dentro de un rango dato.
        /// </summary>
		/// <param name="value">Valor anterior del numero.</param>
		/// <param name="range">Rango a generar numero aleatorio.</param>
		/// <returns>Retorna un entero aleatorio diferente del anterior.</returns>
		private static int GetNextRandom(int value, int range)
		{
			int index = 0;

			if(range > 0)
			{
				Random random = new Random();
				do index = random.Next(range);
				while (index == value);
			}

			return index;
		}

		/// <summary>
		/// Método de la clase DataHandler: Lee un archivo JSON y lo retorna como un JObject.
		/// </summary>
		/// <param name="fileName">Nombre del archivo .bin.</param>
		/// <returns>Retorna un json en tipo object.</returns>
		public static JObject Read(string fileName)
		{
			//Documentacion de newtonsoft.json
			//https://www.newtonsoft.com/json/help/html/Introduction.htm
			var json = new JSONHandler();

			return JSON = json.ReadBin(fileName);
		}

		/// <summary>
		/// Método de la clase DataHandler: Guarda los datos del JSON.
		/// </summary>
		public static void Write(string fileName)
		{
			var json = new JSONHandler();

			json.WriteBin(fileName, JSON.ToString());
		}

		/// <summary>
        /// Método de la clase DataHandler: Aplica la configuración de la aplicación a la ventan principal.
        /// </summary>
		public static void Apply()
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			ConfingHandler.Apply(myWindow);
		}

		/// <summary>
		/// Método de la clase ChangeCursor: Cambia el cursor actual.
		/// </summary>
		public static void ChangeCursor(string fileName)
		{
			 CurrentCursor = new Cursor(GetFullPath(fileName));
		}

		/// <summary>
		/// Método de la clase DataHandler: Retorna el path completo del archivo.
		/// </summary>
		private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Resources\Icons\Cursors\" + fileName;
		}

		#endregion
	}
}
