﻿namespace Calculapp.Data
{
	/// <summary>
	/// Clase Province: Modelo de datos para las provincias.
	/// </summary>
	public class Province
	{
		/// <summary>
		/// Constructor vacío de Province().
		/// </summary>
		public Province() { }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De Name. Almecena el nobre de la provincia.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// SET: y GET: De Image. Almecena la ruta de la imagen.
		/// </summary>
		public string Image { get; set; }
		
		/// <summary>
		/// SET: y GET: De Description. Almecena la descripción de la provincia.
		/// </summary>
		public string Description { get; set; }

		#endregion
	}
}
