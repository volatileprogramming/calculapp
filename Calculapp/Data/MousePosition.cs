﻿namespace Calculapp.Data
{
	/// <summary>
	/// Clase MousePosition: Reprecenta las cordenadas del mouse.
	/// </summary>
	public class MousePosition
	{
		/// <summary>
        /// Constructor vacío de MousePosition().
        /// </summary>
		public MousePosition() { }

		/// <summary>
        /// Constructor de MousePosition(int x, int y).
        /// </summary>
		public MousePosition(int x, int y) { X = x; Y = y; }

		#region SETTERS AND GETTES

		/// <summary>
		/// SET & GET: De X. Almacena un punto en el eje X.
		/// </summary>
		public double X { get; set; }

		/// <summary>
		/// SET & GET: De X. Almacena un punto en el eje Y.
		/// </summary>
		public double Y { get; set; }

		#endregion
	}
}
