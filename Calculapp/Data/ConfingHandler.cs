﻿using Calculapp.Properties;
using Calculapp.Views;
using System.Windows;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase ConfingHandler: Maneja los datos globales.
	/// </summary>
	public static class ConfingHandler
    {
		#region Métodos

		/// <summary>
		/// SET: y GET: De Settings. Las opciones del sistema.
		/// </summary>
		internal static Settings Settings { get; set; } = Settings.Default;

        /// <summary>
        /// Metodo de la clase ConfingHandler: 
        /// </summary>
        /// <returns>Retorna la configuraciones de usuario.</returns>
        internal static Settings GetSettings() { return Settings; }

        /// <summary>
        /// Método de la clase ConfingHandler: Aplica la configuración de la aplicación a la ventan principal.
        /// </summary>
		/// <param name="main">Instancia de la ventana principal.</param>
        public static void Apply(object main)
        {
			var myWindow = main as MainWindow;
			var previousHeight = myWindow.Height;
			var FontSize = DataHandler.GlobalFontSize;

			// Modo de pantalla
            if (Settings.FullScreen) 
			{
				myWindow.WindowState = WindowState.Maximized;
				myWindow.UseNoneWindowStyle = true;
				var newHeight = myWindow.Height;
				var r = (14 * (previousHeight / newHeight));
				DataHandler.GlobalFontSize += r;
			}
		}

		/// <summary>
		/// Método de la clase ConfigHandle. Guarda la configuracion en Config.xml
		/// </summary>
		public static void Save() { Settings.Save(); }

		/// <summary>
		/// Método de la clase ConfigHandle. Daja la configuracion por
		/// defecto del Config.xml
		/// </summary>
		public static void Reset() { Settings.Reset(); }

		#endregion
	}
}
