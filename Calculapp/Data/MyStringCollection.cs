﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase MyStringCollection: Modelo de datos para DataGrib.
	/// </summary>
	class MyStringCollection
	{
		/// <summary>
		/// GET & SET: De Message. Almacena un mensaje.
		/// </summary>
		public string Message { set; get; } = string.Empty;
	}
}
