﻿using System.Collections.Generic;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase ListScores: Modelo de datos para las puntuaciones.
	/// </summary>
	class ListScores
	{
		/// <summary>
		/// Constructor vacío de ListScores().
		/// </summary>
		public ListScores() { }

		#region RegionName

		/// <summary>
		/// SET: y GET: De Posición. Indica la posición de la puntuación.
		/// </summary>
		public List<ScoreData> Scores { get; set; } = new List<ScoreData>();

		#endregion
	}
}
