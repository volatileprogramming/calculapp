﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase MixedOperation. Modelo de datos para operaciones mixtas.
	/// </summary>
	public class MixedOperation
	{
		/// <summary>
		/// Constructor vacío de MixedOperation().
		/// </summary>
		public MixedOperation() { }

		/// <summary>
		/// SET: y GET: De Operation. Almecena un string con la operación.
		/// </summary>
		public string Operation { get; set; }

		/// <summary>
		/// SET: y GET: De Active. Indica si la operación esta activa o no.
		/// </summary>
		public bool Active { get; set; }
	}
}
