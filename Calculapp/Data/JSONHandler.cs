﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Calculapp.Data
{
	/// <summary>
	/// Clase JSONHandler. Maneja los archivos de configuración y guardado del usuario.
	/// </summary>
	public class JSONHandler
    {
		/// <summary>
		/// Constructor vacío de JSONHandler().
		/// </summary>
		public JSONHandler() { }

		/// <summary>
		/// Constructor de JSONHandler(string path).
		/// </summary>
		/// <param name="fileName">Nombre del archivo .json</param>
		public JSONHandler(string fileName, bool isEncrypted=false)
		{
			FileName = fileName;
			using(Stream fileStream = File.Open(GetFullPath(FileName), FileMode.Open))
			{
                StreamReader reader = new StreamReader(fileStream, Encoding.UTF8);

                if (!isEncrypted)
                {
                    Json = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }
                else
                {
                    string encryptedText = reader.ReadToEnd();
                    Json = (JObject)JToken.Parse(SecurityProtocol.DecryptString(encryptedText));
                }
			}
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De la lista de errores encontrados en procesos.
		/// </summary>
		public static List<string> Errors { set; get; } = new List<string>();

		/// <summary>
		/// SET: y GET: De Json. Contiene los datos del archivo .json.
		/// </summary>
		public JObject Json { get; set; }

		/// <summary>
		/// SET: y GET: De FileName. Indica el nombre del archivo .json.
		/// </summary>
		public string FileName { get; set; }

		#endregion

		#region Métodos

		/// <summary>
		/// Método de la clase JSONHandler: Retorna el json.
		/// </summary>
		public JObject Read() { return Json; }

		/// <summary>
		/// Método de la clase JSONHandler: Retorna el json.
		/// </summary>
		/// <param name="fileName">Nombre del archivo .json</param>
		/// <returns>Retorna los datos del archivo en un JObject.</returns>
		public JObject Read(string fileName, bool isEncrypted = false)
        {
            FileName = fileName;
            using (Stream fileStream = File.Open(GetFullPath(FileName), FileMode.Open))
            {
                StreamReader reader = new StreamReader(fileStream, Encoding.UTF8);

                if (!isEncrypted)
                {
                    return Json = (JObject)JToken.ReadFrom(new JsonTextReader(reader));
                }
                else
                {
                    string encryptedText = reader.ReadToEnd();
                    return Json = (JObject)JToken.Parse(SecurityProtocol.DecryptString(encryptedText));
                }
            }
        }

        /// <summary>
		/// Método de la clase JSONHandler: Retorna el json.
		/// </summary>
		/// <param name="fileName">Nombre del archivo .json</param>
		/// <returns>Retorna los datos del archivo en un JObject.</returns>
		public JObject ReadBin(string fileName, bool isEncrypted = false)
        {
            FileName = fileName;
            using (Stream fileStream = File.Open(GetFullPath(FileName), FileMode.Open))
            {
                BinaryReader reader = new BinaryReader(fileStream, Encoding.UTF8);

                byte[] encryptedText = reader.ReadBytes((int)reader.BaseStream.Length);
                byte[] deEncryptText = SecurityProtocol.DecryptTripleDES(encryptedText);
                string desEncryptedText = Encoding.UTF8.GetString(deEncryptText);
                return Json = (JObject)JToken.Parse(desEncryptedText);
            }
        }

        /// <summary>
        /// Método de la clase JSONHandler: Escribe el archivo .json.
        /// </summary>
        public void Write()
		{
			using (StreamWriter file = File.CreateText(GetFullPath(FileName)))
			{
				JsonSerializer serializer = new JsonSerializer();
				serializer.Serialize(file, Json);
			}
		}

		/// <summary>
		/// Método de la clase JSONHandler: Escribe el archivo .json.
		/// </summary>
		/// <param name="fileName">Nombre del archivo .json</param>
		public void Write(string fileName, string text=null)
		{
			FileName = fileName;
            using (Stream fileStream = File.Open(GetFullPath(FileName), FileMode.Create))
            {
                StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8);

                if (text == null)
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(writer, Json);
                }
                else
                {
                    writer.Write(SecurityProtocol.EncryptString(text));
                }
            }
		}

        /// <summary>
		/// Método de la clase JSONHandler: Escribe el archivo .json.
		/// </summary>
		/// <param name="fileName">Nombre del archivo .json</param>
		public void WriteBin(string fileName, string text = null )
        {
			
            FileName = fileName;

            using (Stream fileStream = File.Open(GetFullPath(FileName), FileMode.Create))
            {
                BinaryWriter writer = new BinaryWriter(fileStream, Encoding.UTF8);

				if (text == null)
				{
					string toEncrypt = Json.ToString();
					byte[] stringInBytes = Encoding.UTF8.GetBytes(toEncrypt);
					writer.Write(SecurityProtocol.EncryptTripleDES(stringInBytes));
				}
				else
				{
					byte[] stringInBytes = Encoding.UTF8.GetBytes(text);
					writer.Write(SecurityProtocol.EncryptTripleDES(stringInBytes));
				}
            }
        }

        /// <summary>
        /// Método de la clase JSONHandler: Retorna el path completo del archivo.
        /// </summary>
        private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Data\JSON\" + fileName;
		}

		#endregion

		#region Ejemplos
		
		/* Implementación 01
		 * ---- Leer ----
		 * var JSONHandlerr = new JSONHandler("Ejemplo.json");
		 * var json = JSONHandlerr.Read();
		 * 
		 * ---- Escribir ----
		 * var JSONHandlerr = new JSONHandler("Ejemplo.json");
		 * var json = JSONHandlerr.Write();
		 * 
		 * Implementación 02
		 * ---- Leer ----
		 * var JSONHandlerr = new JSONHandler();
		 * var json = JSONHandlerr.Read("Ejemplo.json");
		 * 
		 * ---- Escribir ----
		 * var JSONHandlerr = new JSONHandler();
		 * var json = JSONHandlerr.write("Ejemplo.json");
		 */

		#endregion
    }
}
