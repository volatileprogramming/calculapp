﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Calculapp.Modules
{
    /// <summary>
    /// Clase AnimationHandler: Gestiona las animaciones de los controladores.
    /// </summary>
    class AnimationHandler
    {
		//atributos
        private bool _isAnimating;
        private bool _multiAnimations;

        /// <summary>
        /// Constructor vacío de AnimationHandler().
        /// </summary>
        public AnimationHandler(bool multiAnimations = false)
        {
            _isAnimating = false;
            _multiAnimations = multiAnimations;
        }

        #region Métodos

        /// <summary>
        /// Funcion para la animacion de desplazamiento de un controlador desde su posicion a la final en un tiempo dado.
        /// </summary>
        /// <param name="control">Control a resivir la animacion</param>
        /// <param name="fin">Punto X e Y final de la animacion</param>
        /// <param name="duration">Tiempo que durara la animmacion</param>
        public async void Traslation(Control control, Point fin, Duration duration)
        {
            if (!_isAnimating || _multiAnimations)
            {
                _isAnimating = true;


                //Crea los timeLine basado en la distancia entre el control y el fin tomando en cuenta el tiempo dado.

                var animateX = new DoubleAnimation(fin.X, duration);
                var animateY = new DoubleAnimation(fin.Y, duration);

                //Inicio de la animacion de desplazamiento.
                control.RenderTransform.BeginAnimation(TranslateTransform.XProperty, animateX);
                control.RenderTransform.BeginAnimation(TranslateTransform.YProperty, animateY);

                //Espera hasta que el tiempo dado pase.
                await Task.Delay(duration.TimeSpan.Milliseconds);
                _isAnimating = false;
            }
        }

        /// <summary>
        /// Funcion para la animacion del cambio de tamaño del ancho y alto de un controlador en un tiempo dado.
        /// </summary>
        /// <param name="control">Control a resivir la animacion</param>
        /// <param name="escalaX">Escala del control en el eje X donde quedara</param>
        /// <param name="escalaY">Escala del control en el eje X donde quedara</param>
        /// <param name="duration">Tiempo que durara la animmacion</param>
        public async void Transform(Control control, double escalaX, double escalaY, Duration duration)
        {
            if (!_isAnimating || _multiAnimations)
            {
                _isAnimating = true;

				//dynamic parent = control.Parent;
				//var RP = control.TransformToAncestor(parent).Transform(new Point(0, 0));
				//var translateTransform = new TranslateTransform(RP.X, RP.Y);
				//control.RenderTransform = translateTransform;

                //Crea el metodo de transformacion de escala del contro.
                ScaleTransform myScaleTransform = new ScaleTransform
				{
					CenterX = (control.ActualWidth / 2),
					CenterY = (control.ActualHeight / 2)
				};

                //Crea los timeLine basado en la escala actual del contro y la final en el tiempo dado.
                var animateX = new DoubleAnimation(escalaX, duration);
                var animateY = new DoubleAnimation(escalaY, duration);

				//Inicio de la animacion de transformacion.
				control.RenderTransform = myScaleTransform;

                myScaleTransform.BeginAnimation(ScaleTransform.ScaleXProperty, animateX);
                myScaleTransform.BeginAnimation(ScaleTransform.ScaleYProperty, animateY);

                //Espera hasta que el tiempo dado pase.
                await Task.Delay(duration.TimeSpan.Milliseconds);
                _isAnimating = false;
            }
        }

        /// <summary>
        /// Método de la clase AnimationHandler. Realiza una animación de transición.
        /// </summary>
        /// <param name="control">Control a resivir la animacion</param>
        /// <param name="alphaFinal">Alpha al que el control finalizara despues de la animacion</param>
        /// <param name="duration">Tiempo que durara la animmacion</param>
        public async void Transition(object control, int alphaFinal, Duration duration)
        {
            if (!_isAnimating || _multiAnimations)
            {
                var _control = control as UserControl;
                _isAnimating = true;

                await Task.Delay(duration.TimeSpan.Milliseconds);
                _isAnimating = false;
            }
        }

        /// <summary>
        /// Método de la clase AnimationHandler. Realiza una animación de rotación.
        /// </summary>
        /// <param name="control">Control a resivir la animacion</param>
        /// <param name="angluloFinal">Angulo en el que el control finalizara despues de la animacion</param>
        /// <param name="duration">Tiempo que durara la animmacion</param>
        public async void Rotation(object control, int angluloFinal, Duration duration)
        {
            if (!_isAnimating || _multiAnimations)
            {
                var _control = control as UserControl;
                _isAnimating = true;

                await Task.Delay(duration.TimeSpan.Milliseconds);
                _isAnimating = false;
            }
        }

        #endregion
    }
}
