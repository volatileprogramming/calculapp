﻿using Calculapp.Data;
using Calculapp.Views.UserControls;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase Buider: Instancia los controladores de la pantalla seleccionada.
	/// </summary>
	public static class Builder
	{
		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de la pantalla seleccionada.
		/// </summary>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		public static void Buil(object container)
		{
			var module = DataHandler.SelectedModule;

			//Instancia los objetos del gameplay de operaciones basicas [ + - x / ].
			if(DataHandler.SelectedLevel != null)
				BuilGamePlayOPerations(DataHandler.Read("Levels.bin"), container);
			else
			{
				//Instancia los objetos del mapa.
				if (module != "Ordering" && module != "Count" && module != "MixedOperations")
					BuilMapp(DataHandler.Read("Levels.bin"), container);
				else 
				{
					//Instancia los objetos del gameplay de ordenamiento.
					if (module == "Ordering") BuilGamePlayOrdering(DataHandler.Read("Config.bin"), container);
					//Instancia los objetos del gameplay de conteo.
					if (module == "Count") BuilGamePlayCount(DataHandler.Read("Config.bin"), container);
					//Instancia los objetos del gameplay de operaciones mixtas.
					if (module == "MixedOperations") BuilGamePlayMixedOperations(DataHandler.Read("Config.bin"), container);
				}
			}
		}

		#region SETTERS AND GETTERS

		/// <summary>
		/// MyRandom: Generador de numeros aleatorios.
		/// </summary>
		private static readonly Random MyRandom = new Random();

		#endregion

		#region Métodos de la clase Builder

		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de Mapp.
		/// </summary>
		/// <param name="jObject">Objeto JSON que contiene los datos de los controladores a ser creados.</param>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		private static void BuilMapp(JObject jObject, object container)
		{
			//carga de datos
			var svgReader = new SVGReader();
			var grado = DataHandler.SelectedGrade;
			var module = DataHandler.SelectedModule;
			var levels = jObject[grado][module]["Levels"].ToObject<List<Level>>();
			var lastLevelPassed = (int)jObject[grado][module]["LastLevelPassed"];
			var canvas = container as Canvas;
			var currentLevel = 1;
			//datos de Provincias
            var JSONReader = new JSONHandler();
            var provinces = JSONReader.ReadBin("Province.bin");

			//instancia y agrega controladores
			foreach (var level in levels)
			{
				//instancia los niveles
                var levelControl = canvas.FindName(level.Name) as UserControlLevel;
                levelControl.Visibility = Visibility.Visible;

                //almacena las propiedades de los niveles
                if (lastLevelPassed == 0 && currentLevel == 1) levelControl.Passed = false;
                else if (lastLevelPassed >= currentLevel) levelControl.Passed = true;
				else if (lastLevelPassed+1 == currentLevel) levelControl.Passed = false;
                levelControl.Level = level;
                levelControl.MyProvince = provinces[level.Name].ToObject<Province>();

                //incrementa el contador para verificar el ultimo nivel pasado
                currentLevel++;
			}
		}

		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de GamePlayOPerations.
		/// </summary>
		/// <param name="jObject">Objeto JSON que contiene los datos de los controladores a ser creados.</param>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		private static void BuilGamePlayOPerations(JObject roomData, object container)
		{
			//carga de datos
			var grado = DataHandler.SelectedGrade;
			var level = DataHandler.SelectedLevel;
			var hasFractions = (bool)roomData[grado]["HasFractions"];
			var hasNegative = (bool)roomData[grado]["HasNegative"];
			var operationContainer = container as UserControlOperationsContainer;
			var min = level.Min;
			var max = level.Max;

			//setea el gamePlay
			operationContainer.Gameplay = new OperationsGameplay();
			var gamePlay = operationContainer.Gameplay as OperationsGameplay;

			//genera las operaciones
			if (DataHandler.SelectedModule != "Multiplication") gamePlay.Operations = GenerateOperations(min, max);
			else
			{
				var min2 = 1;
				var max2 = 12;
				gamePlay.Operations = GenerateOperations(min, max, min2, max2);
			}

			//seterar la primera operacion
			operationContainer.OperationControl.Operation = gamePlay.Operations[0];

			//genera las respuestas
			gamePlay.Answers = GenerateAnswers(gamePlay.Operations, hasFractions, hasNegative);

			//seteara la primera respuesta
			operationContainer.Answers = gamePlay.Answers[0];
		}

		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de GamePlayMixedOperations.
		/// </summary>
		/// <param name="jObject">Objeto JSON que contiene los datos de los controladores a ser creados.</param>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		private static void BuilGamePlayMixedOperations(JObject roomData, object container)
		{
			//carga de datos
			var grado = DataHandler.SelectedGrade;
			var mixedOperations = roomData[grado]["MixedOperations"];
			var hasFractions = (bool)roomData[grado]["HasFractions"];
			var hasNegative = (bool)roomData[grado]["HasNegative"];
			var operationContainer = container as UserControlOperationsContainer;

			//setea el gamePlay
			operationContainer.Gameplay = new OperationsGameplay();
			var gamePlay = operationContainer.Gameplay as OperationsGameplay;

			//genera las operaciones
			gamePlay.Operations = GetMixedOperations(mixedOperations);

			//seterar la primera operacion
			operationContainer.OperationControl.Operation = gamePlay.Operations[0];

			//genera las respuestas
			gamePlay.Answers = GenerateAnswers(gamePlay.Operations, hasFractions, hasNegative);

			//seterar la primera respuesta
			operationContainer.Answers = gamePlay.Answers[0];
		}

		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de GamePlayOrdering.
		/// </summary>
		/// <param name="jObject">Objeto JSON que contiene los datos de los controladores a ser creados.</param>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		private static void BuilGamePlayOrdering(JObject roomData, object container)
		{
			//carga de datos
			var grado = DataHandler.SelectedGrade;
			var range = (int)roomData[grado]["OrderCount"];
			var startValue = (int)roomData[grado]["OrderStartValue"];
			var order = (bool)roomData[grado]["OrderOrder"];
			var operationContainer = container as UserControlOrderingContainer;

			//setea el gamePlay
			operationContainer.Gameplay = new GameplayOrdering();
			var gamePlay = operationContainer.Gameplay as GameplayOrdering;

			//indica el valor inicial
			gamePlay.Order = order;
			gamePlay.Range = range;
			gamePlay.StarValue = startValue;

			//seterar la primera respuesta
			operationContainer.Order = order;
			operationContainer.Range = range;
			operationContainer.StartValue = startValue;
			operationContainer.Answers = GenerateOrderingList(startValue, range);
		}

		/// <summary>
		/// Método de la clase Builder: Instancia todos los controladores de GamePlayCount.
		/// </summary>
		/// <param name="jObject">Objeto JSON que contiene los datos de los controladores a ser creados.</param>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		private static void BuilGamePlayCount(JObject roomData, object container)
		{
			//carga de datos
			var grado = DataHandler.SelectedGrade;
			var countTime = (int)roomData[grado]["CountTime"];
			var fruitNumber = (int)roomData[grado]["CountFuitsNumber"];
			var interval = (int)roomData[grado]["CountInterval"];
			var startValue = (int)roomData[grado]["CountStartValue"];
			var order = (bool)roomData[grado]["CountOrder"];
			var operationContainer = container as UserControlCountContainer;

			//setea el gamePlay
			operationContainer.Gameplay = new GameplayCount();
			var gamePlay = operationContainer.Gameplay as GameplayCount;

			//indica el valor inicial
			gamePlay.LimitTime = TimeSpan.FromMinutes(countTime);
			gamePlay.Order = order;
			gamePlay.StarValue = startValue;
			gamePlay.Fruits = new List<string> { "avocado", "uvas", "naranja", "piña" };

			//seterar la primera respuesta
			operationContainer.Interval = interval;
			operationContainer.Order = order;
			operationContainer.FruitNumber = fruitNumber;
			operationContainer.Answers = GetFruits(interval, fruitNumber);
			operationContainer.SetControls(startValue, order);
		}

		#endregion

		#region Métodos auxiliares

		#region Operations

		/// <summary>
		/// Método de la clase Builder: Obtiene una lista de operaciones.
		/// </summary>
		/// <param name="min">Valor minimo.</param>
		/// <param name="max">Valor maximo.</param>
		/// <returns>Retorna un List<string> con la lista de operaciones.</returns>
		private static List<string> GenerateOperations(double min, double max, double minO = 0, double maxO = 0)
		{
			var hasFractions = (bool)DataHandler.JSON[DataHandler.SelectedGrade]["HasFractions"];
			var operations = new List<string>();
			double n1, n2;

			if (DataHandler.SelectedModule == "Addition")
			{
				for (int i = 0; i < 10; i++)
				{
					n1 = (!hasFractions) ? (int)MyRandom.NextDouble(min, max) : MyRandom.NextDouble(min, max);
					n2 = (!hasFractions) ? (int)MyRandom.NextDouble(min, max) : MyRandom.NextDouble(min, max);
					operations.Add($"{n1} {GetOperator()} {n2}");
				}
			}
			else if (DataHandler.SelectedModule == "Subtraction")
			{
				for (int i = 0; i < 10; i++)
				{
					n1 = (!hasFractions) ? (int)MyRandom.NextDouble(min+1, max) : MyRandom.NextDouble(min+1, max);
					n2 = (!hasFractions) ? (int)MyRandom.NextDouble(min, n1) : MyRandom.NextDouble(min, n1);
					operations.Add($"{n1} {GetOperator()} {n2}");
				}
			}
			else if (DataHandler.SelectedModule == "Multiplication")
			{
				for (int i = 0; i < 10; i++)
				{
					n1 = (!hasFractions) ? (int)MyRandom.NextDouble(minO, maxO) : MyRandom.NextDouble(minO, maxO);
					n2 = (!hasFractions) ? (int)MyRandom.NextDouble(min, max) : MyRandom.NextDouble(min, max);
					operations.Add($"{n1} {GetOperator()} {n2}");
				}
			}
			else
			{
				for (int i = 0; i < 10; i++)
				{
					if (!hasFractions)
					{
						n1 = (int)MyRandom.NextDouble(min, max);

						var divisors = GetPrimeFactors((int)n1);

						if (divisors.Count > 1) n2 = divisors[MyRandom.Next(0, divisors.Count - 1)];
						else if (divisors.Count > 0) n2 = divisors[0];
						else n2 = n1;
					}
					else
					{
						n1 = (int)MyRandom.NextDouble(min, max);
						n2 = (int)MyRandom.NextDouble(min, max);
					}

					operations.Add($"{n1} {GetOperator()} {n2}");
				}
			}

			return operations;
		}
		
		/// <summary>
		/// Método de la clase Builder: Obtiene una lista de operaciones.
		/// </summary>
		/// <param name="number"></param>
		/// <returns>Retorna un List<string> con la lista de operaciones.</returns>
		private static List<int> GetPrimeFactors(int number)
		{
			var primeFactors = new List<int>();
			for (int p = 2; p < number; ++p)
			{
				while (number % p == 0)
				{
					primeFactors.Add(p);
					number = number / p;
				}
			}
			return primeFactors;
		}

		/// <summary>
		/// Método de la clase Builder: Obtiene un array de 2 dimenciones con las respuestas.
		/// </summary>
		/// <param name="operations">Listas de operaciones.</param>
		/// <param name="HasFractions">Indica si tiene o no numeros fraccionarios.</param>
		/// <param name="HasNegative">Indica si tiene o no numeros negativos.</param>
		/// <returns>Retorna un double[][] con respuestas diferentes y aleatorias.</returns>
		private static double[][] GenerateAnswers(List<string> operations, bool HasFractions, bool HasNegative)
		{
			//setea atributos
			var answers = new List<double[]>();
			var count = 0;
			double resoult, min, max;

			foreach (var operation in operations)
			{
				using(var epression = new EspressionDisposable(operation))
				{
					
					//No fracciones para grados menores que 4to.
					resoult = (HasFractions) ? epression.calculate() : (int)epression.calculate();

					//Minimo mayor que 0 para grados 1ro, 2do y 3ro.
					min = (HasNegative) ? resoult - 5d : (resoult - 5d < 0) ? 0 : resoult - 5d;
					max = resoult + 5d;

					double[] resoults = GetNextRandom(min, max);
					answers.Add(resoults);
					//for (int i = 0; i < 10; i++) answers.Add(GetNextRandom(min, max));

					if (answers[count].Contains<double>(resoult))
					{
						count++;
						continue;
					}
					else answers[count][MyRandom.Next(0, 4)] = resoult;
				}
				count++;
			}

			return answers.ToArray();
		}

		/// <summary>
		/// Método de la clase Builder: Optiene el operador del nivel.
		/// </summary>
		/// <returns>Retorna un char con el operador de la operacón.</returns>
		private static char GetOperator()
		{
			if (DataHandler.SelectedModule == "Addition") return '+';
			else if (DataHandler.SelectedModule == "Subtraction") return '-';
			else if (DataHandler.SelectedModule == "Multiplication") return '*';
			else return '/';
		}

		#endregion

		#region Mixed Operations

		/// <summary>
		/// Método de la clase Builder: Obtiene una lista de operaciones.
		/// </summary>
		/// <param name="mixedOperations">Lista de operaciones del grado seleccionado.</param>
		/// <returns>Retorna un List<string> con la lista de operaciones activas.</returns>
		private static List<string> GetMixedOperations(JToken mixedOperations)
		{
			var operations = new List<string>();

			foreach (var operation in mixedOperations) if((bool)operation["Active"]) operations.Add(operation["Operation"].ToString());

			return operations;
		}

		/// <summary>
		/// Método de la clase Builder: Obtiene un int[] de muneros diferentes y aleatorios.
		/// </summary>
		/// <param name="min">Valor minimo.</param>
		/// <param name="max">Valor maximo.</param>
		/// <returns>Retorna un int[] de muneros diferentes y aleatorios.</returns>
		private static double[] GetNextRandom(double min, double max)
		{
			var list = new List<double>();
			var value = 0d;

			for (int i = 0; i < 4; i++)
			{
				do
				{
					var hasFractions = (bool)DataHandler.JSON[DataHandler.SelectedGrade]["HasFractions"];

					value = MyRandom.NextDouble() * (max - min) + min;

					//setea el valor en entero dependiendo si acepta o no fracciones.
					if (!hasFractions) value = (int)value;
				}
				while (list.Contains(value));

				list.Add(value);
			}

			return list.ToArray();
		}

		#endregion

		#region Ordenamiento

		/// <summary>
		/// Método de la clase Builder: Obtiene un array de 2 dimenciones con las respuestas.
		/// </summary>
		/// <param name="startValue">Valor iniciar de la lista.</param>
		/// <param name="range">Distancia desde el valor iniciar.</param>
		/// <returns>Retorna un double[] con respuestas diferentes y aleatorias.</returns>
		private static double[] GenerateOrderingList(int startValue, int range)
		{
			var list = new List<double>();

			//genera la lista ordenada
			for (var i = startValue; i < (startValue + range); i++) list.Add(i);

			//baraja la lista
			list.Shuffle();

			return list.ToArray();
		}

		#endregion

		#region Conteo

		private static List<UserControlFruits> GetFruits(int interval, int fruitNumber)
		{
			var list = new List<UserControlFruits>();
			var fruitNames = new string[] { "aguacate", "piña", "naranja", "uvas" };

			for(var i = 0; i < fruitNumber; i++)
			{
				list.Add(new UserControlFruits(interval, fruitNames[i]));
			}

			return list;
		}

		#endregion

		#endregion
	}

	#region Clases Auxiliares

	/// <summary>
	/// Clase statica RandomExtensions: Extiende System.Random.
	/// </summary>
	public static class RandomExtensions
	{
		/// <summary>
		/// Método de la clase RandomExtensions: Optiene un double generado aleatoriamente.
		/// </summary>
		/// <param name="random">Clase System.Ramdom.</param>
		/// <param name="maxValue">Valor maximo a ser generado.</param>
		/// <returns>Retorna un double generado aleatoriamente.</returns>
		public static double NextDouble(this Random random, double maxValue)
		{
			return random.NextDouble() * maxValue;
		}

		/// <summary>
		/// Método de la clase RandomExtensions: Optiene un double generado aleatoriamente.
		/// </summary>
		/// <param name="random">Clase System.Ramdom.</param>
		/// <param name="minValue">Valor minimo a ser generado.</param>
		/// <param name="maxValue">Valor maximo a ser generado.</param>
		/// <returns>Retorna un double generado aleatoriamente.</returns>
		public static double NextDouble(this Random random, double minValue, double maxValue)
		{
			return random.NextDouble() * (maxValue - minValue) + minValue;
		}
	}

	/// <summary>
	/// Clase statica IListExtensions: Extiende System.Collections.Generic.IList.
	/// </summary>
	public static class IListExtensions
	{
		/// <summary>
		/// Método de la clase RandomExtensions: Baraja la lista.
		/// </summary>
		/// <typeparam name="T">Tipo de la lista.</typeparam>
		/// <param name="ts">Lista a ser barajada.</param>
		public static void Shuffle<T>(this IList<T> ts) {
			var count = ts.Count;
			var last = count - 1;
			for (var i = 0; i < last; ++i) {
				var r = MyRandom.Next(i, count);
				var tmp = ts[i];
				ts[i] = ts[r];
				ts[r] = tmp;
			}
		}

		/// <summary>
		/// MyRandom: Generador de numeros aleatorios.
		/// </summary>
		private static readonly Random MyRandom = new Random();
	}

	#endregion
}
