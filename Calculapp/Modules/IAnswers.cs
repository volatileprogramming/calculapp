﻿using System.Windows;

namespace Calculapp.Modules
{
	public interface IAnswers
	{
		double Number { get; set; }
		string FruitName { get; set; }
		Point Position { get; set; }
		bool IsLooked { get; set; }
	}
}
