﻿using Calculapp.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculapp.Modules
{
	public class InputHandler
	{

		public static bool IsLocked { get; private set; }
		private static TimeSpan FrameRate { get; set; } = DataHandler.FrameRate;
		private static List<object> KeyDownList { get; set; } = new List<object>(); 
		private static List<object> KeyUpList { get; set; } = new List<object>(); 
		private static List<object> MouseDownList { get; set; } = new List<object>(); 
		private static List<object> MouseMoveList { get; set; } = new List<object>(); 
		private static List<object> MouseUpList { get; set; } = new List<object>(); 
		private static List<object> TouchDownList { get; set; } = new List<object>(); 
		private static List<object> TouchMoveList { get; set; } = new List<object>(); 
		private static List<object> TouchUpList { get; set; } = new List<object>(); 

		public void PreviewKeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = (IsLocked | KeyDownList.Contains(sender));
			Lock();
			//Save(TouchMoveList, sender);
		}

		public void PreviewKeyUp(object sender, KeyEventArgs e)
		{
			e.Handled = (IsLocked | KeyUpList.Contains(sender));
			Lock();
			//Save(TouchMoveList, sender);
		}

		public void PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = (IsLocked | MouseDownList.Contains(sender));
			Lock();
			Save(TouchMoveList, sender);
		}

		public void PreviewMouseMove(object sender, MouseEventArgs e)
		{
			e.Handled = (IsLocked | MouseMoveList.Contains(sender));
			Lock();
			Save(TouchMoveList, sender);
		}

		public void PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			e.Handled = (IsLocked | MouseUpList.Contains(sender));
			Lock();
			Save(TouchMoveList, sender);
			var answer = sender as IAnswers;
			if (answer != null) answer.IsLooked = true;
		}

		public void PreviewTouchDown(object sender, TouchEventArgs e)
		{
			e.Handled = (IsLocked | TouchDownList.Contains(sender));
			Lock();
			Save(TouchMoveList, sender);
		}

		public void PreviewTouchMove(object sender, TouchEventArgs e)
		{
			e.Handled = (IsLocked | TouchMoveList.Contains(sender));
			Lock();
			Save(TouchMoveList, sender);
		}

		public void PreviewTouchUp(object sender, TouchEventArgs e)
		{
			e.Handled = (IsLocked | TouchUpList.Contains(sender));
			Lock();
			Save(TouchUpList, sender);
			var answer = sender as IAnswers;
			if (answer != null) answer.IsLooked = true;
		}

		private async void Lock()
		{
			IsLocked = true;
			await Task.Delay(FrameRate);
			IsLocked = false;
		}

		private async void Save(List<object> list, object sender)
		{
			
			list.Add(sender);
			await Task.Delay(1000);
			list.Remove(sender);
		}
	}
}
