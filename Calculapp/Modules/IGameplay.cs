﻿using Calculapp.Data;
using Calculapp.Views.Screens;
using System.Diagnostics;
using System.Windows;

namespace Calculapp.Modules
{
	public interface IGameplay
	{
		//metodos
		void Start();
		void End();
		//setters y getters
		IAnswers SelectedAnswer { set; get; }
		bool IsPlaying { get; set; }
		bool Stoped { get; set; }
		int Score { get; set; }
		ScoreData MyScore { get; set; }
		Stopwatch Timer { get; set; }
		GamePlayScreen Screen { get; set; }
	}
}
