﻿using Calculapp.Data;
using Stannieman.AudioPlayer;
using System.Reflection;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase SoundPlayer: Reproduce los efectos de sonido.
	/// </summary>
	public class SoundPlayer : AudioPlayer
	{
        private float Volume;
        private bool IsMute;
        /// <summary>
        /// Constructor de SoundPlayer(string fileName).
        /// </summary>
        /// <param name="fileName">Nombre del archivo de sonido.</param>
        public SoundPlayer(string fileName)
		{
			SetFileAsync(GetFullPath(fileName), "0");
			FinishedPlaying += delegate { Dispose(); };
			Volume = (float)DataHandler.SoundVolume;
			IsMute = DataHandler.SoundMute;
		}

		#region Métodos

		/// <summary>
		/// Método de la clase SoundPlayer: Reproduce el sonido.
		/// </summary>
		public void Play()
		{
			PlayAsync();
		}

		/// <summary>
		/// Método de la clase SoundPlayer: Retorna el path completo del archivo.
		/// </summary>
		private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Resources\Sounds\" + fileName;
		}

		#endregion
	}
}
