﻿namespace Calculapp.Modules
{
	/// <summary>
	/// Clase MultiBinding: Premite hacer multiples Bindings en una sola linea.
	/// </summary>
	public class MultiBinding : System.Windows.Data.MultiBinding
	{
		/// <summary>
		/// Constructor de MultiBinding(BindingBase b1, BindingBase b2).
		/// </summary>
		/// <param name="b1">Primer binding de la lista.</param>
		/// <param name="b2">Segundo binding de la lista.</param>
		/// <param name="converter">Convertor de datos.</param>
		public MultiBinding(System.Windows.Data.BindingBase b1, System.Windows.Data.BindingBase b2, object converter)
		{
			Bindings.Add(b1);
			Bindings.Add(b2);
			Converter = converter as System.Windows.Data.IMultiValueConverter;
		}
	}
}
