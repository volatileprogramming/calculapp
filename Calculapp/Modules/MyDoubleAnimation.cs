﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase MyDoubleAnimation: Anima un double.
	/// </summary>
	public static class MyDoubleAnimation
	{
		/// <summary>
		/// Constructor de MyDoubleAnimation(double from, double to, TimeSpan duration);
		/// </summary>
		/// <param name="from">Indica el valor inicial.</param>
		/// <param name="to">Indica el valor final.</param>
		/// <param name="speed">Velocidad de la animación.</param>
		public static async void Animate(double from, double to, TimeSpan speed)
		{
			while (from != to)
			{
				if(from < to) from += 0.01;
				else from -= 0.01;
				await Task.Delay(speed);
			}
		}
	}
}
