﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase abstracta BaseConverter: Sirver para la implementacion de MarkupExtension.
	/// </summary>
	public abstract class BaseConverter : MarkupExtension
	{
		/// <summary>
		/// Método de la clase BaseConverter: Provee el valor a convertir.
		/// </summary>
		/// <param name="serviceProvider">Servicio de provee los valores.</param>
		/// <returns>Retonra el valor a convertir.</returns>
		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}

	/// <summary>
	/// Clase DoubleToStringConverter: Convierte los valores doubles a strings y viceversa.
	/// </summary>
	[ValueConversion(typeof(double), typeof(string))]
	public class DoubleToStringConverter : BaseConverter, IValueConverter
	{
		/// <summary>
		/// Método de la clase DoubleToStringConverter:	Convierte los dobles a strings.
		/// </summary>
		/// <param name="value">Valor a ser convertido.</param>
		/// <param name="targetType">Tipo del valor a ser convertido.</param>
		/// <param name="parameter">Parametros del objeto.</param>
		/// <param name="culture">Datos culturales del objeto.</param>
		/// <returns>Retorno el valor convertido a string.</returns>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			double doubleType = (double)value;
			int intType = (int)(doubleType);
			return intType.ToString();
		}
		
		/// <summary>
		/// Método de la clase DoubleToStringConverter: Convierte los strings a dobles.
		/// </summary>
		/// <param name="value">Valor a ser convertido.</param>
		/// <param name="targetType">Tipo del valor a ser convertido.</param>
		/// <param name="parameter">Parametros del objeto.</param>
		/// <param name="culture">Datos culturales del objeto.</param>
		/// <returns>Retorno el valor convertido a double.</returns>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string strValue = value as string;
			if (double.TryParse(strValue, out double resultDouble))
			{
				return resultDouble;
			}
			return DependencyProperty.UnsetValue;
		}
	}
}
