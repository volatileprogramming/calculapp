﻿using Calculapp.Data;
using Calculapp.Views;
using Calculapp.Views.Screens;
using Calculapp.Views.UserControls;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase OperationsGameplay: Maneja todo el gameplay de operaciones.
	/// </summary>
	public class OperationsGameplay : IGameplay
	{
		//atributos
		private int _wrongAnswers = 0;
		private List<string> _operations;

		/// <summary>
		/// Constructor vacío de OperationsGameplay().
		/// </summary>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		public OperationsGameplay() { }

		#region SETTERS AND GETTERS

		public bool Stoped { get; set; } = false;

		/// <summary>
		/// SET: y GET: De Screen. Almecena la pantalla actual.
		/// </summary>
		public GamePlayScreen Screen { get; set; }

		/// <summary>
		/// SET: y GET: De CurrentOperation. Almecena el indice de la operación actual.
		/// </summary>
		public int CurrentOperation { set; get; } = 0;

		/// <summary>
		/// SET: y GET: De Operations. Almecena la lista de operaciones.
		/// </summary>
		public List<string> Operations
		{
			get => _operations;
			set
			{
				var parent = UserControlOperationsContainer.Instance;
				parent.SetIndicator(_operations = value);
			}
		}

		/// <summary>
		/// SET: y GET: De Aswers. Almecena la lista de respuestas según el numero de operaciones.
		/// </summary>
		public double[][] Answers { set; get; }

		/// <summary>
		/// SET: y GET: De Score. Almecena la puntuación acumulada.
		/// </summary>
		public int Score
		{
			get => MyScore.Puntuación;
			set
			{
				Screen.Score = MyScore.Puntuación = value;
			}
		}

		/// <summary>
		/// SET: y GET: De Score. Almecena los datos de la puntuación acumulada.
		/// </summary>
		public ScoreData MyScore { get; set; } = new ScoreData();

		/// <summary>
		/// SET: y GET: De Timer. Indica el tiempo transcurrido en el game play.
		/// </summary>
		public Stopwatch Timer { get; set; } = new Stopwatch();

		/// <summary>
		/// SET: y GET: De WrongAswers. Almecena la cantidad de respuestas equivocadas.
		/// </summary>
		public int WrongAnswers
		{
			get => _wrongAnswers;
			set
			{
				Screen.WrongAnswers = _wrongAnswers = value;
			}
		}

		/// <summary>
		/// SET: y GET: De SelectedAnswer. Almacena la respuesta seleccionada.
		/// </summary>
		public IAnswers SelectedAnswer { set; get; } = null;

		/// <summary>
		/// SET: y GET: De IsAnimating. Indica si se esta reproduciendo una animación o no.
		/// </summary>
		public bool IsAnimating { get; set; } = false;

		/// <summary>
		/// SET: y GET: De IsPlaying. Indica si se esta jugando o no.
		/// </summary>
		public bool IsPlaying { get; set; } = true;

		#endregion

		#region Dibujado

		/// <summary>
		/// Método de la clase OperationsGameplay: Ejetula el NextOperationAnimation.
		/// </summary>
		private void NextOperationAnimation()
		{
			//Reproduce Sonido
			//PlaySound("");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Ejetula el WinAnimation.
		/// </summary>
		private void WinAnimation()
		{
			//Reproduce Sonido
			PlaySound("level-complete.wav");
			PlaySound("yes.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Ejetula el WrongAnimation.
		/// </summary>
		private void LostAnimation()
		{
			//Reproduce Sonido
			PlaySound("game-over.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Ejetula el WrongAnimation.
		/// </summary>
		private void CorrectAnimation()
		{
			//Reproduce Sonido
			PlaySound("good.mp3");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Ejetula el WrongAnimation.
		/// </summary>
		private void WrongAnimation()
		{
			//Reproduce Sonido
			PlaySound("wrong.wav");

			//Realiza animación
		}

		#endregion

		#region Interacciones

		/// <summary>
		/// Método de la clase OperationsGameplay: Inisializa la ejecución del gameplay.
		/// </summary>
		public void Start()
		{
			//inicia el juego
			Stoped = !(IsPlaying = true);

			//inicia el tiempo
			Timer.Start();

			//inicia el gameplay
			WhilePlaying();
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Cambia la operacion actial.
		/// </summary>
		/// <param name="resoult">Indica si la respuesta es correcta o incorrecta.</param>
		private void NextOperation(bool resoult)
		{
			var current = UserControlOperationsContainer.Instance.DockPanelLevelIndicator.Children[CurrentOperation++] as UserControlLevelIndicator;

			current.BorderIndicator.Margin = new Thickness {  Left = 0, Bottom = 5, Right = 0, Top = 5 };
			current.BorderIndicator.Background = (resoult) ? Brushes.Green : Brushes.Red;

			//cambia la operacion
			if (CurrentOperation < Operations.Count)
			{
				var next = UserControlOperationsContainer.Instance.DockPanelLevelIndicator.Children[CurrentOperation] as UserControlLevelIndicator;

				next.BorderIndicator.Margin = new Thickness {  Left = 0, Bottom = 3, Right = 0, Top = 3 };
				next.BorderIndicator.Background = Brushes.White;
				next.BorderIndicator.OpacityMask = new SolidColorBrush { Opacity = 1, Color = Colors.Black };

				UserControlOperationsContainer.Instance.Answers = Answers[CurrentOperation];
				UserControlOperationsContainer.Instance.Operation = Operations[CurrentOperation];
			}

			//animacion para la siguiente operacion
			NextOperationAnimation();
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Agrega una respuesta incorrecta.
		/// </summary>
		private void AddWrongAnswer()
		{
			//suma respuesta incorrecta
			WrongAnswers++;

			//Muestra animacion
			WrongAnimation();

			//cambia la operacion
			NextOperation(false);
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Agrega una respuesta correcta.
		/// </summary>
		private void AddCorrectAnswer()
		{
			//suma 10 puntos a la puntuacion
			Score += 10;

			//Muestra animacion
			CorrectAnimation();

			//cambia la operacion
			NextOperation(true);
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Verifica si la respuesta es correcta o no.
		/// </summary>
		/// <param name="answer">Respuesta a ser verificada.</param>
		/// <returns>Retorna true o false dependiendo de la condicion.</returns>
		private bool CheckAswer(IAnswers answer)
		{
			//seta propiedades
			var expression = new EspressionDisposable(Operations[CurrentOperation]);

			//comprueva si respuesta es correcta o no
			if (answer.Number == expression.calculate()) return true;
			else return false;
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Muestra los resultados de la partida.
		/// </summary>
		private void ShowResoults()
		{
			//setea las propiedades
			var myWindow = Application.Current.MainWindow as MainWindow;
			var instance = UserControlOperationsContainer.Instance;
			var parent = instance.Parent as Grid;

			//limpia el contenedor padre
			if (parent != null && parent.Children.Contains(instance)) parent.Children.Remove(instance);

			//verifica si ganaste o perdiste
			if (WrongAnswers < 3)
			{
				WinAnimation();
				
				//guarda el ultimo nivel pasado
				var level = DataHandler.SelectedLevel;
				if (level != null)
				{

					var grade = DataHandler.SelectedGrade;
					var module = DataHandler.SelectedModule;
					var lastLevelPased = (int)DataHandler.JSON[grade][module]["LastLevelPassed"];

					if (lastLevelPased < level.Number)
					{
						DataHandler.JSON[grade][module]["LastLevelPassed"] = level.Number;
						DataHandler.Write("Levels.bin");
					}
				}
			}
			else LostAnimation();

			//muestra puntuaciones
			MyScore.Tiempo = Timer.Elapsed.ToString(@"mm\:ss\.ff");;
			Timer.Stop();

			myWindow.MainGrid.Children.Add(new UserControlScoreContainer(MyScore));
		}

		/// <summary>
		/// Método de la clase OperationsGameplay: Finaliza la ejecución del gameplay.
		/// </summary>
		public void End()
		{
			Stoped = !(IsPlaying = false);
		}

		#endregion

		#region Calculos

		/// <summary>
		/// Método de la clase OperationsGameplay: Raliza los calculos de los inputs del usuario.
		/// </summary>
		private async void WhilePlaying()
		{
			do
			{
				//Sincroniza el bucle con los frame rates
				await Task.Delay(DataHandler.FrameRate);

				//salta el bucle si no se ha seleccionado ninguna respuesta.
				if (SelectedAnswer == null) continue;

				//berifica que la respuesta sea correcta
				await Task.Run(() => CheckAswer(SelectedAnswer)
				).ContinueWith(r => 
				{
					if (r.Result) AddCorrectAnswer();
					else AddWrongAnswer();
				}, TaskScheduler.FromCurrentSynchronizationContext());

				//recetea la respuesta.
				SelectedAnswer = null;

			} while (IsPlaying && WrongAnswers < 3 && CurrentOperation < Operations.Count);

			//Muestra resultados una vez finalizado el gameplay
			if (!Stoped) ShowResoults();
		}

		#endregion

		#region Sonido

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
	}
}
