﻿namespace Calculapp.Modules
{
	/// <summary>
	/// Clase PointConverter: Convierte el dos double en un punto y biseversa.
	/// </summary>
	public class PointConverter : BaseConverter, System.Windows.Data.IMultiValueConverter
	{
		/// <summary>
		/// Método de la clase PointConverter: Convierte dos double en un punto.
		/// </summary>
		/// <param name="values">Valores a ser convertidos.</param>
		/// <param name="targetType">Tipo del target.</param>
		/// <param name="parameter">Parametros.</param>
		/// <param name="culture">Información de cultura.</param>
		/// <returns>Retorna un object con los datos convertidos.</returns>
		public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			double xValue = (double)values[0];
			double yValue = (double)values[1];
			return new System.Windows.Point(xValue, yValue);
		}

		/// <summary>
		/// Método de la clase PointConverter: Convierte un punto en dos double.
		/// </summary>
		/// <param name="value">Valor a ser convertido.</param>
		/// <param name="targetType">Tipo del target.</param>
		/// <param name="parameter">Parametros.</param>
		/// <param name="culture">Información de cultura.</param>
		/// <returns>Retorna un object[] con los datos convertidos.</returns>
		public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			System.Windows.Point point = (System.Windows.Point)value;
			return new object[] { point.X, point.Y };
		}
	}
}
