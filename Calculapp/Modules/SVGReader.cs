﻿using SharpVectors.Converters;
using SharpVectors.Renderers.Wpf;
using System;
using System.Reflection;
using System.Windows.Media;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase SVGReader: Lee archivos svg y los convierte en DrawingImage.
	/// </summary>
	public class SVGReader
	{
		/// <summary>
        /// Constructor vacío de SVGReader().
        /// </summary>
		public SVGReader() { }

		/// <summary>
        /// Constructor de LoadingScreen(string fileName).
        /// </summary>
		/// <param name="fileName">Archivo a ser tranformado a DrawingImage.</param>
		public SVGReader(string fileName)
		{
			// 1. Create conversion options
            Settings.IncludeRuntime = true;
			Settings.TextAsGeometry = false;

			// 2. Select a file to be converted
			SVGFile = GetFullPath(fileName);

			// 3. Create a file reader
            Converter = new FileSvgReader(Settings);

            // 4. Read the SVG file
            Drawing = Converter.Read(SVGFile);
		}

		#region SETTERS AND GETTES

		/// <summary>
		/// SET & GET: De Settings. Configuración del WpfDrawing.
		/// </summary>
		private WpfDrawingSettings Settings { get; set; } = new WpfDrawingSettings();

		/// <summary>
		/// SET & GET: De Drawing. Grupo de dibujo.
		/// </summary>
		private DrawingGroup Drawing { get; set; }

		/// <summary>
		/// SET & GET: De Converter. Lector de archivos SVG.
		/// </summary>
		private FileSvgReader Converter { get; set; }

		/// <summary>
		/// SET & GET: De SVGFile. String con el full path del archivo SVG.
		/// </summary>
		private string SVGFile { get; set; }

		#endregion

		#region Métodos de la clase SVGReader

		/// <summary>
		/// Método de la clase SVGReader: Lee el archivo svg seleccionado.
		/// </summary>
		/// <returns>Retorna un DrawingImage del archivo svg leido.</returns>
		public DrawingImage Read()
		{
			try
			{
				return new DrawingImage(Drawing);
			}
			catch (Exception) { throw; }
		}
		
		/// <summary>
		/// Método de la clase SVGReader: Lee el archivo svg seleccionado.
		/// </summary>
		/// <param name="fileName">Archivo a ser tranformado a DrawingImage.</param>
		/// <returns>Retorna un DrawingImage del archivo svg leido.</returns>
		public DrawingImage Read(string fileName)
		{
			// 1. Create conversion options
            Settings.IncludeRuntime = true;
			Settings.TextAsGeometry = false;

			// 2. Select a file to be converted
			SVGFile = GetFullPath(fileName);

			// 3. Create a file reader
            Converter = new FileSvgReader(Settings);

            // 4. Read the SVG file
            Drawing = Converter.Read(SVGFile);

			// 5. Returns the Drawing Image
			return new DrawingImage(Drawing);
		}

		/// <summary>
		/// Método de la clase SVGReader: Retorna el path completo del archivo.
		/// </summary>
		private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Resources\" + fileName;
		}

		#endregion

		#region Ejemplos
		
		/* Implementación 01
		 * var svgReader = new SVGReader("Ejemplo.svg");
		 * Imagen.Sourse = svgReader.Read();
		 * 
		 * Implementación 02
		 * var svgReader = new SVGReader();
		 * Imagen.Sourse = svgReader.Read("Ejemplo.svg");
		 */

		#endregion
	}
}
