﻿using Calculapp.Data;
using Calculapp.Views;
using Calculapp.Views.Screens;
using Calculapp.Views.UserControls;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase GameplayOrdering: Maneja todo el gameplay de ordenamiento.
	/// </summary>
	class GameplayOrdering : IGameplay
	{
		//atributos
		private int _wrongAnswers = 0;

		/// <summary>
		/// Constructor vacío de GameplayOrdering().
		/// </summary>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		public GameplayOrdering() { }

		#region SETTERS AND GETTERS

		public bool Stoped { get; set; }

		/// <summary>
		/// SET: y GET: De StarValue. Almecena el valor inicial.
		/// </summary>
		public int StarValue { get; set; } = 0;

		/// <summary>
		/// SET: y GET: De OrderedList. Almecena la lista ordenada.
		/// </summary>
		public int Range { get; set; } = 0;

		/// <summary>
		/// SET: y GET: De Screen. Almecena la pantalla actual actual.
		/// </summary>
		public GamePlayScreen Screen { get; set; }

		/// <summary>
		/// SET: y GET: De CurrentOperation. Almecena el indice de la operación actual.
		/// </summary>
		public int CurrentOperation { set; get; } = 0;

		/// <summary>
		/// SET: y GET: De Score. Almecena la puntuación acumulada.
		/// </summary>
		public int Score
		{
			get => MyScore.Puntuación;
			set
			{
				Screen.Score = MyScore.Puntuación = value;
			}
		}

		/// <summary>
		/// SET: y GET: De Score. Almecena los datos de la puntuación acumulada.
		/// </summary>
		public ScoreData MyScore { get; set; } = new ScoreData();

		/// <summary>
		/// SET: y GET: De Timer. Indica el tiempo transcurrido en el game play.
		/// </summary>
		public Stopwatch Timer { get; set; } = new Stopwatch();

		/// <summary>
		/// SET: y GET: De WrongAswers. Almecena la cantidad de respuestas equivocadas.
		/// </summary>
		public int WrongAnswers
		{
			get => _wrongAnswers;
			set
			{
				Screen.WrongAnswers = _wrongAnswers = value;
			}
		}

		/// <summary>
		/// SET: y GET: De SelectedAnswer. Almacena la respuesta seleccionada.
		/// </summary>
		public IAnswers SelectedAnswer { set; get; } = null;
		public Point TargetPoint { get; private set; }

		/// <summary>
		/// SET: y GET: De IsAnimating. Indica si se esta reproduciendo una animación o no.
		/// </summary>
		public bool IsAnimating { get; set; } = false;

		/// <summary>
		/// SET: y GET: De IsPlaying. Indica si se esta jugando o no.
		/// </summary>
		public bool IsPlaying { get; set; } = true;
		public bool Order { get; internal set; }

		#endregion

		#region Dibujado

		/// <summary>
		/// Método de la clase GameplayOrdering: Ejetula el NextOperationAnimation.
		/// </summary>
		private void NextOperationAnimation()
		{
			//Reproduce Sonido
			//PlaySound("");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Ejetula el WinAnimation.
		/// </summary>
		private void WinAnimation()
		{
			//Reproduce Sonido
			PlaySound("level-complete.wav");
			PlaySound("yes.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Ejetula el WrongAnimation.
		/// </summary>
		private void LostAnimation()
		{
			//Reproduce Sonido
			PlaySound("game-over.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Ejetula el WrongAnimation.
		/// </summary>
		private void CorrectAnimation()
		{
			//Reproduce Sonido
			PlaySound("good.mp3");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Ejetula el WrongAnimation.
		/// </summary>
		private void WrongAnimation()
		{
			//Reproduce Sonido
			PlaySound("wrong.wav");

			//Realiza animación
		}

		#endregion

		#region Interacciones

		/// <summary>
		/// Método de la clase GameplayOrdering: Inisializa la ejecución del gameplay.
		/// </summary>
		public void Start()
		{
			//inicia el juego
			Stoped = !(IsPlaying = true);

			//inicia el tiempo
			Timer.Start();

			//inicia el gameplay
			WhilePlaying();
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Agrega una respuesta incorrecta.
		/// </summary>
		private void AddWrongAnswer()
		{
			//suma respuesta incorrecta
			WrongAnswers++;

			//reta 10 puntos a la puntuacion
			if (Score > 0) Score -= 10;

			//Muestra animacion
			WrongAnimation();
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Agrega una respuesta correcta.
		/// </summary>
		private void AddCorrectAnswer()
		{
			//suma 10 puntos a la puntuacion
			Score += 10;

			//Muestra animacion
			CorrectAnimation();
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Verifica si la respuesta es correcta o no.
		/// </summary>
		/// <param name="answer">Respuesta a ser verificada.</param>
		/// <returns>Retorna true o false dependiendo de la condicion.</returns>
		private bool CheckAswer(IAnswers answer)
		{
			var number = (int)answer.Number;

			if ((Order & number == (CurrentOperation + StarValue)) || 
				(!Order & number == ((StarValue + Range) - (CurrentOperation + 1))))
			{
				//seta propiedades
				var animation = new AnimationHandler();
				var control = answer as UserControlSquare;
				var objectPoint = control.Position;
				var finalPoint = TargetPoint.GetDiference(objectPoint);

				control.IsLooked = true;

				//inicia la animacion
				animation.Traslation(control, finalPoint, TimeSpan.FromMilliseconds(400));

				//cambia de elemento
				CurrentOperation++;
				return true;
			}
			else return false;
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Muestra los resultados de la partida.
		/// </summary>
		private async void ShowResoults()
		{
			await Task.Delay(1000);
			//setea las propiedades
			var myWindow = Application.Current.MainWindow as MainWindow;
			var instance = UserControlOrderingContainer.Instance;
			var parent = instance.Parent as Grid;

			//limpia el contenedor padre
			if (parent != null && parent.Children.Contains(instance)) parent.Children.Remove(instance);

			//verifica si ganaste o perdiste
			if (WrongAnswers < 3) WinAnimation();
			else LostAnimation();

			//muestra puntuaciones
			MyScore.Tiempo = Timer.Elapsed.ToString(@"mm\:ss\.ff");;
			Timer.Stop();

			myWindow.MainGrid.Children.Add(new UserControlScoreContainer(MyScore));
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Finaliza la ejecución del gameplay.
		/// </summary>
		public void End()
		{
			Stoped = !(IsPlaying = false);
			Timer.Stop();
		}

		#endregion

		#region Calculos

		public void SetAnswer(IAnswers answers, Point answerPoint)
		{
			TargetPoint = answerPoint;
			SelectedAnswer = answers;
		}

		/// <summary>
		/// Método de la clase GameplayOrdering: Raliza los calculos de los inputs del usuario.
		/// </summary>
		private async void WhilePlaying()
		{
			do
			{
				//Sincroniza el bucle con los frame rates
				await Task.Delay(DataHandler.FrameRate);

				//salta el bucle si no se ha seleccionado ninguna respuesta.
				if (SelectedAnswer == null) continue;

				//berifica que la respuesta sea correcta
				await Task.Run(() =>
				{
					return Screen.Dispatcher.Invoke(() =>
					{
						return CheckAswer(SelectedAnswer);
					});
				}
				).ContinueWith(r => 
				{
					if (r.Result) AddCorrectAnswer();
					else AddWrongAnswer();
				}, TaskScheduler.FromCurrentSynchronizationContext());

				//recetea la respuesta.
				SelectedAnswer = null;

			} while (IsPlaying && WrongAnswers < 3 && CurrentOperation < Range);

			//Muestra resultados una vez finalizado el gameplay
			if (!Stoped) ShowResoults();
		}

		#endregion

		#region Sonido

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
	}
}
