﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase TaskHandler. Maneja lista de tareas en un nucleo secundario.
	/// </summary>
	public static class TaskHandler
    {
		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De UnicTask. Tarea a ser realizada en un nucleo secudario.
		/// </summary>
		public static Task UnicTask { get; set; }

		/// <summary>
		/// SET: y GET: De TaskList. Lista de tareas a ser realizadas en un nucleo secudario.
		/// </summary>
		public static List<Task> TaskList { get; set; } = new List<Task>();

		/// <summary>
		/// SET: y GET: De Errors. Lista de errores encontrados en procesos.
		/// </summary>
		public static List<string> Errors { set; get; } = new List<string>();

		#endregion

		#region Métodos

		/// <summary>
		/// Método de la clase TaskHandler. Añade una lista de tareas a ser ejecutadas.
		/// </summary>
		/// <param name="actions">Array de acciones a ser ejecutadas.</param>
		public static void AddTasks(Action[] actions)
        {
            try
            {
                foreach (var action in actions)
                {
                    var LastTask = new Task(action);
                    LastTask.Start();
                    TaskList.Add(LastTask);
                }

            }
            catch (Exception ex)
			{
				Errors.Add($"{ex.Message}.\n");
			}
        }

		/// <summary>
		/// Método de la clase TaskHandler. Ejecuta la tarea espesificada en un nucleo secundario.
		/// </summary>
		/// <param name="action">Acción a se ejecutada en un nucleo secundario.</param>
		/// <param name="parameters">Lista de parametros de la función.</param>
		public static void Run(Action action, List<object> parameters = null)
        {
            try
            {
                UnicTask = new Task(action);
                UnicTask.Start();
                TaskList.Add(UnicTask);
            }
            catch (Exception ex)
			{
				Errors.Add($"{ex.Message}.\n");
			}
		}

		internal static void RunTask(Task task)
		{
			TaskList.Add(task);
			task.Start();
		}

		public static async void TaskCompleted()
		{
			await Task.WhenAll(TaskList.ToArray());
			TaskList.Clear();
		}

		/// <summary>
		/// Método de la clase TaskHandler. Ejecuta un tarea al terminar todas las tareas en segundo plano.
		/// </summary>
		/// <param name="action">Acción a ejecutar al terminar la lista de tareas.</param>
		/// <param name="parameters">Lista de parametros de la función.</param>
		public static void TaskCompleted(Action action, List<object> parameters = null)
        {
            try
            {
                Task.WaitAll(TaskList.ToArray());
                action.Invoke();
            }
            catch (Exception ex)
			{
				Errors.Add($"{ex.Message}.\n");
			}
		}

		#endregion
    }
}
