﻿using Calculapp.Data;
using Calculapp.Views;
using System;
using System.Reflection;
using System.Windows;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase MusicPlayer: Reproductor de musica y efectos sonoros.
	/// </summary>
	public static class MusicPlayer
	{
		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET: De MusicIsPlaying. Indica si la musica se esta reproduciendo o no.
		/// </summary>
		public static bool IsPlaying { get; private set; } = false;

		/// <summary>
		/// SET & GET: De CurrentMusic: Indica la canción actual.
		/// </summary>
		public static string CurrentMusic { private set; get; } = "Cancion_Juego.mp3";

		/// <summary>
		/// SET & GET: De MusicVolume: Indica el volumen de la musica.
		/// </summary>
		public static double MusicVolume { get => DataHandler.MusicVolume; set => DataHandler.MusicVolume = value; }

		/// <summary>
		/// SET & GET: De SoundVolume: Indica si la musica esta o no en mute.
		/// </summary>
		public static bool MusicMute { get => DataHandler.MusicMute; set => DataHandler.MusicMute = value; }

		#endregion

		#region Métodos

		/// <summary>
		/// Método de la clase MusicPlayer: Reproduce la musica o el sonido espesificados.
		/// </summary>
		/// <param name="fileName">Archivo de audio a ser reproducido.</param>
		/// <param name="isSound">Indica si es un sonido o una musica.</param>
		public static void Play(string fileName = "")
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			if (string.IsNullOrEmpty(fileName))
			{
				try
				{
					myWindow.Music.Play();
					IsPlaying = true;
				}
				catch (Exception) { throw; }
			}
			else Change(fileName);
		}

		/// <summary>
		/// Método de la clase MusicPlayer: Puasa la musica, para su posterior reanudación.
		/// </summary>
		public static void Pause()
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			myWindow.Music.Pause();
		}

		/// <summary>
		/// Método de la clase MusicPlayer: Detiene la musica.
		/// </summary>
		public static void Stop()
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			myWindow.Music.Stop();
		}

		/// <summary>
		/// Método de la clase MusicPlayer: Repite la musica.
		/// </summary>
		public static void Repeat()
		{
			Stop();
			Play();
		}

		/// <summary>
		/// Método de la clase MusicPlayer: Retorna el path completo del archivo.
		/// </summary>
		private static string GetFullPath(string fileName)
		{
			var path = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
			return path + @"\Resources\Songs\" + fileName;
		}

		#endregion

		#region Eventos

		/// <summary>
		/// Método de la clase MusicPlayer: Abre los archivos de audio.
		/// </summary>
		public static void Open(string fileName)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			try
			{
				myWindow.Music.Open(new Uri(GetFullPath(fileName)));
				myWindow.Music.Volume = MusicVolume;
				myWindow.Music.MediaEnded += delegate {
					Repeat();
				};
			}
			catch (Exception) { throw; }
		}

		/// <summary>
		/// Método de la clase MusicPlayer: Cambia la musica actual.
		/// </summary>
		public static void Change(string fileName)
		{
			var myWindow = Application.Current.MainWindow as MainWindow;
			Stop();
			CurrentMusic = fileName;

			try
			{
				myWindow.Music.Open(new Uri(GetFullPath(fileName)));
				myWindow.Music.Volume = MusicVolume;
				myWindow.Music.MediaEnded += delegate {
					Repeat();
				};
			}
			catch (Exception) { throw; }
		}

		#endregion
	}
}
