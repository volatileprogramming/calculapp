﻿namespace Calculapp.Modules
{
	interface IGameplayContainer
	{
		IGameplay Gameplay { set; get; }
	}
}
