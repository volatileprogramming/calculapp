﻿using Calculapp.Data;
using Calculapp.Views;
using Calculapp.Views.Screens;
using Calculapp.Views.UserControls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase GameplayCount: Maneja todo el gameplay de operaciones.
	/// </summary>
	class GameplayCount : IGameplay
    {
		/// <summary>
		/// Constructor vacío de GameplayCount().
		/// </summary>
		/// <param name="container">Contenedor de los controalores a ser instanciados.</param>
		public GameplayCount() { }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET: y GET: De Screen. Almecena el indice de la operación actual.
		/// </summary>
		public GamePlayScreen Screen { get; set; }

		/// <summary>
		/// SET: y GET: De CurrentOperation. Almecena el indice de la operación actual.
		/// </summary>
		public int CurrentOperation { set; get; } = 0;

		/// <summary>
		/// SET: y GET: De Score. Almecena la puntuación acumulada.
		/// </summary>
		public int Score
		{
			get => MyScore.Puntuación;
			set
			{
				Screen.Score = MyScore.Puntuación = value;
			}
		}

		/// <summary>
		/// SET: y GET: De Score. Almecena los datos de la puntuación acumulada.
		/// </summary>
		public ScoreData MyScore { get; set; } = new ScoreData();

		/// <summary>
		/// SET: y GET: De Timer. Indica el tiempo transcurrido en el game play.
		/// </summary>
		public Stopwatch Timer { get; set; } = new Stopwatch();

		/// <summary>
		/// SET: y GET: De LimitTime. Indica el tiempo limite del game play.
		/// </summary>
		public TimeSpan LimitTime { get; set; }
		public List<Point> CollitionMask { get; private set; }

		/// <summary>
		/// SET: y GET: De SelectedAnswer. Almacena la respuesta seleccionada.
		/// </summary>
		public IAnswers SelectedAnswer { set; get; } = null;

		/// <summary>
		/// SET: y GET: De IsAnimating. Indica si se esta reproduciendo una animación o no.
		/// </summary>
		public bool IsAnimating { get; set; } = false;

		/// <summary>
		/// SET: y GET: De IsPlaying. Indica si se esta jugando o no.
		/// </summary>
		public bool IsPlaying { get; set; } = true;
		public bool Order { get; internal set; }
		public int StarValue { get; internal set; }
		public List<string> Fruits { get; internal set; }
		public bool Stoped { get; set; }

		#endregion

		#region Dibujado

		/// <summary>
		/// Método de la clase GameplayCount: Ejetula el NextOperationAnimation.
		/// </summary>
		private void NextOperationAnimation()
		{
			//Reproduce Sonido
			//PlaySound("");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase GameplayCount: Ejetula el WinAnimation.
		/// </summary>
		private void WinAnimation()
		{
			//Reproduce Sonido
			PlaySound("level-complete.wav");
			PlaySound("yes.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase GameplayCount: Ejetula el WrongAnimation.
		/// </summary>
		private void LostAnimation()
		{
			//Reproduce Sonido
			PlaySound("game-over.wav");

			//Realiza animación
			DataHandler.IsAnimating = true;
		}

		/// <summary>
		/// Método de la clase GameplayCount: Ejetula el WrongAnimation.
		/// </summary>
		private void CorrectAnimation()
		{
			//Reproduce Sonido
			PlaySound("good.mp3");

			//Realiza animación
		}

		/// <summary>
		/// Método de la clase GameplayCount: Ejetula el WrongAnimation.
		/// </summary>
		private void WrongAnimation()
		{
			//Reproduce Sonido
			PlaySound("wrong.wav");

			//Realiza animación
		}

		#endregion

		#region Interacciones

		/// <summary>
		/// Método de la clase GameplayCount: Inisializa la ejecución del gameplay.
		/// </summary>
		public void Start()
		{
			//inicia el juego
			Stoped = !(IsPlaying = true);

			//inicia la cuenta
			CountDown();

			//inicia el gameplay
			WhilePlaying();
		}

		/// <summary>
		/// Método de la clase GameplayCount: Agrega una respuesta incorrecta.
		/// </summary>
		private void AddWrongAnswer()
		{
			//Muestra animacion
			WrongAnimation();
		}

		/// <summary>
		/// Método de la clase GameplayCount: Agrega una respuesta correcta.
		/// </summary>
		private void AddCorrectAnswer()
		{
			//suma 10 puntos a la puntuacion
			Score += 10;

			//Muestra animacion
			CorrectAnimation();
		}

		/// <summary>
		/// Método de la clase GameplayCount: Verifica si la respuesta es correcta o no.
		/// </summary>
		/// <param name="answer">Respuesta a ser verificada.</param>
		/// <returns>Retorna true o false dependiendo de la condicion.</returns>
		private bool CheckAswer(IAnswers answer)
		{
			//seta propiedades
			//var animation = new AnimationHandler();
			var control = answer as UserControlFruits;
			var parent = (Grid)control.Parent;
			if (parent == null) return false;
			var relativePosition = control.TransformToAncestor(parent).Transform(new Point(0, 0));

			////anima el control
			//animation.Transform(control, 0, 0, new Duration(TimeSpan.FromMilliseconds(400)));

			//comprueva si respuesta es correcta o no
			return (relativePosition.IsInsideOf(CollitionMask));
		}

		/// <summary>
		/// Método de la clase GameplayCount: Muestra los resultados de la partida.
		/// </summary>
		private void ShowResoults()
		{
			//setea las propiedades
			var myWindow = Application.Current.MainWindow as MainWindow;
			var instance = UserControlCountContainer.Instance;
			var parent = instance.Parent as Grid;

			//limpia el contenedor padre
			if (parent != null && parent.Children.Contains(instance)) parent.Children.Remove(instance);

			//animación de victoria
			WinAnimation();

			//muestra puntuaciones
			MyScore.Tiempo = Timer.Elapsed.ToString(@"mm\:ss\.ff");
			Timer.Stop();

			myWindow.MainGrid.Children.Add(new UserControlScoreContainer(MyScore));
		}

		internal void SetAnswer(IAnswers answers, List<Point> list)
		{
			CollitionMask = list;
			SelectedAnswer = answers;
		}

		/// <summary>
		/// Método de la clase GameplayCount: Finaliza la ejecución del gameplay.
		/// </summary>
		public void End()
		{
			Stoped = !(IsPlaying = false);
			Timer.Stop();
		}

		#endregion

		#region Calculos

		private async void CountDown()
		{
			//inicia la cuenta
			Timer.Start();

			//cuenta a la inversa por segundos
			do
			{
				await Task.Delay(1000);
				var currentTime = LimitTime - Timer.Elapsed;

				//muestra el tiempo en pantalla
				Screen.CurrentTime = currentTime;

				if (currentTime < TimeSpan.FromSeconds(0)) IsPlaying = false;

			} while (IsPlaying);
		}

		/// <summary>
		/// Método de la clase GameplayCount: Raliza los calculos de los inputs del usuario.
		/// </summary>
		private async void WhilePlaying()
		{
			do
			{
				//Sincroniza el bucle con los frame rates
				await Task.Delay(DataHandler.FrameRate);

				//salta el bucle si no se ha seleccionado ninguna respuesta.
				if (SelectedAnswer == null) continue;

				//berifica que la respuesta sea correcta
				await Task.Run(() => 
				{
					return Screen.Dispatcher.Invoke(() =>
					{
						return CheckAswer(SelectedAnswer);
					});
				}
				).ContinueWith(r =>
				{
					if (r.Result) AddCorrectAnswer();
					//else AddWrongAnswer();
				}, TaskScheduler.FromCurrentSynchronizationContext());

				//recetea la respuesta.
				SelectedAnswer = null;

			} while (IsPlaying && Timer.IsRunning);

			//Muestra resultados una vez finalizado el gameplay
			if (!Stoped) ShowResoults();
		}

		#endregion

		#region Sonido

		/// <summary>
		/// Método PlaySound. Reproduce un efecto de sonido de forma asincrona.
		/// </summary>
		/// <param name="fileName">Nombre del archivo de audio.</param>
		private void PlaySound(string fileName)
		{
			//reproduce el sonido
			if (!DataHandler.SoundMute)
			{
				var media = new SoundPlayer(fileName);
				media.Play();
			}
		}

		#endregion
    }
}
