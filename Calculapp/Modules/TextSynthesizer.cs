﻿using Calculapp.Data;
using System;
using System.Globalization;
using System.Speech.Synthesis;
using System.Threading.Tasks;

namespace Calculapp.Modules
{
	/// <summary>
	/// Class to Synthesize computer text aloud.
	/// </summary>
	public class TextSynthesizer
    {
        //atributos 
        private double _volume;

        /// <summary>
        /// Default class constructor.
        /// </summary>
        public TextSynthesizer()
        {
            InitializeComponents();
        }

        /// <summary>
        /// Class constructor that initialize TextToSpeak attribute.
        /// </summary>
        /// <param name="textToSpeak"></param>
        public TextSynthesizer(string textToSpeak)
        {
            InitializeComponents();
            TextToSpeak = textToSpeak;
        }

		#region SETTERS AND GETTERS

		/// <summary>
		/// SET & GET of TextToSpeak: that store text to speak aloud. 
		/// </summary>
		public string TextToSpeak { get; set; } = string.Empty;

        /// <summary>
        /// SET & GET of Volume: that store the volume of the synthesizer.
        /// </summary>
        public double Volume 
        { 
            get => _volume;
            set
            {
                _volume = value;
                Synthesizer.Volume = Convert.ToInt32(value*100);
            } 
        }

        /// <summary>
        /// SET & GET of Synthesizer: that speak aloud any given text.
        /// </summary>
        public SpeechSynthesizer Synthesizer { get; set; } = new SpeechSynthesizer();
        #endregion

        #region METODOS DE LA CLASE TextSynthesizer

        /// <summary>
        /// Config and start the properties needed to use the class TextSynthesizer.
        /// </summary>
        private void InitializeComponents()
        {
            Synthesizer.SetOutputToDefaultAudioDevice();
            Synthesizer.SelectVoiceByHints(VoiceGender.Female, VoiceAge.Adult, 3, CultureInfo.GetCultureInfo("es-MX"));
			Volume = DataHandler.SoundVolume;
            //Synthesizer.Rate = 5;
        }

        /// <summary>
        /// Methor that reads text aloud using a synthesizer to read the local TextToSpeak attribute.
        /// </summary>
        public void ReadAloud()
        {
            Synthesizer.Speak(TextToSpeak);
        }

        /// <summary>
        /// Methor that reads text aloud asynchronously using a synthesizer to read the local TextToSpeak attribute.
        /// </summary>
        public async void ReadAloudAsync()
        {
            await Task.Run(() => Synthesizer.SpeakAsync(TextToSpeak));
        }

        /// <summary>
        /// Methor that reads text aloud using a synthesizer and gets the text to read as a parameter.
        /// </summary>
        /// <param name="textToSpeak"></param>
        public void ReadAloud(string textToSpeak)
        {
            Synthesizer.Speak(textToSpeak);
        }

        /// <summary>
        /// Methor that reads text aloud asynchronously using a synthesizer and gets the text to read as a parameter.
        /// </summary>
        /// <param name="textToSpeak"></param>
        public async void ReadAloudAsync(string textToSpeak)
        {
            await Task.Run(() => Synthesizer.SpeakAsync(textToSpeak));
        }

        #endregion
    }
}
