﻿using System;
using System.ComponentModel;
using org.mariuszgromada.math.mxparser;

namespace Calculapp.Modules
{
	/// <summary>
	/// Clase EspressionDisposable. Realiza operaciones matematicas
	/// </summary>
	public class EspressionDisposable : Expression, IDisposable
	{
		// Pointer to an external unmanaged resource.
        private IntPtr handle;
        // Other managed resource this class uses.
        private Component component = new Component();
        // Track whether Dispose has been called.
        private bool disposed = false;

		/// <summary>
		/// Constructor de EspressionDisposable(string expressionString, params PrimitiveElement[] elements);
		/// </summary>
		/// <param name="expressionString">Texto a ser calculado.</param>
		/// <param name="elements">Parametros de la operación.</param>
		public EspressionDisposable(string expressionString, params PrimitiveElement[] elements) : base(expressionString, elements)
		{
		}

		/// <summary>
		/// IDisposable de la calse EspressionDisposable. Vacia el espacio en memoria.
		/// </summary>
		void IDisposable.Dispose()
		{
			Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Método de la clase EspressionDisposable. Vacía el espacio en memoria.
		/// </summary>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if(!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing)
                {
                    // Dispose managed resources.
                    component.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;

                // Note disposing has been done.
                disposed = true;

            }
        }

		/// <summary>
		/// CloseHandle de EspressionDisposable. Manejador de cerrado.
		/// </summary>
		/// <param name="handle">Manejador.</param>
		/// <returns>Retorna true o false segun la condición.</returns>
		[System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

		/// <summary>
		/// Destructor de la clase EspressionDisposable().
		/// </summary>
		~EspressionDisposable() { Dispose(false); }
	}
}
